/**
 * @file
 * Javascript for the DonorPerfect name form element.
 */

(function ($, drupalSettings) {

  Drupal.behaviors.donorperfectFormElementName = {
    attach: function (context, settings) {

      /**
       * Update placeholder for last_name field on donor_type change.
       */
      $('div.donorperfect-name-wrapper div.donor-type-wrapper select:not(.donorperfect-form-element-name-processed)').bind('change', function () {
        var donor_type = $(this).val();
        var placeholder = 'Last Name';
        if (donor_type !== 'IN') {
          placeholder = $(this).find('option[value="' + donor_type + '"]').html() + ' Name';
        }
        $(this).closest('div.donorperfect-name-wrapper').find('div.last-name-wrapper input[type="text"]').attr('placeholder', placeholder);
      }).addClass('donorperfect-form-element-name-processed');

    }
  };

})(jQuery, window.drupalSettings);
