/**
 * @file
 * Javascript for search functionality for the DonorPerfect name form element.
 */

(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.donorperfectFormElementNameSearch = {
    attach: function (context, settings) {

      /**
       * Add action to initiate search after last name has been filled in.
       */
      $('div.donorperfect-name-wrapper > div.last-name-wrapper > input:not(.donorperfect-form-element-name-search-processed)').bind('blur', function () {
        $.fn.DonorPerfectFormElementNamePerformSearch(this);
      }).addClass('donorperfect-form-element-name-search-processed');

      /**
       * Add action to initiate search when search button clicked.
       */
      $('div.donorperfect-name-search-button-wrapper > img:not(.donorperfect-form-element-name-search-processed)').bind('click', function (e) {
        $.fn.DonorPerfectFormElementNamePerformSearch(this);
      }).addClass('donorperfect-form-element-name-search-processed');

      /**
       * Add action to search results match link click.
       */
      $('td.donorperfect-name-match > a:not(.donorperfect-form-element-name-search-processed)').bind('click', function (e) {
        e.preventDefault();
        var data = jQuery.parseJSON($(this).attr('result-data'));
        var element_token = $(this).closest('div.donorperfect-name-search-wrapper').attr('element-token');
        var $form = $(this).closest('form');
        var nbsp_regex = new RegExp(String.fromCharCode(160), "g");
        // First, reset all DPFields to their default values.
        $.fn.DonorPerfectFormElementNameSearchResetFields(JSON.stringify({element_token}));
        // Check for phone type.
        ['mobile', 'home', 'business', 'fax'].forEach(phone_type => {
          if (!data.hasOwnProperty('phone_type')) {
            if (data.hasOwnProperty(phone_type + '_phone') && data[phone_type + '_phone']) {
              data.phone_type = phone_type;
            }
          }
        });
        // Now udpate DPFields with values from selected match.
        jQuery.each(data, function (key, value) {
          if (typeof value !== 'object') {
            // All DPfields in the form have already been set to the defaults set by the form creator.
            // Therefore, if the value from DP for this field is empty, then leave the form default.
            if (value.length) {
              // Replace &nbsp; with spaces.
              value = value.replace(nbsp_regex, " ");
              $form.find('input[dpfield="' + key + '"]').each(function (index) {
                switch($(this).attr('type')) {
                  case 'radio':
                    if ($(this).attr('value') === value) {
                      this.checked = true;
                    } else {
                      this.checked = false;
                    }
                    $(this).trigger('change');
                    break;

                  case 'checkbox':
                    if ((value === 1) || (value === '1') || (value === 'Y')) {
                      this.checked = true;
                    } else {
                      this.checked = false;
                    }
                    $(this).trigger('change');
                    break;

                  default:
                    $(this).val(value);
                    break;
                }
              });
              $form.find('select[dpfield="' + key + '"]').each(function (index) {
                $(this).val(value);
                $(this).trigger('change');
              });
              $form.find('textarea[dpfield="' + key + '"]').each(function (index) {
                $(this).val(value);
              });
            }
          }
        });
        // Hide the search results table.
        $.fn.DonorPerfectFormElementNameSearchSwitchLoadingResults(JSON.stringify({'show': 'none', element_token}));
      }).addClass('donorperfect-form-element-name-search-processed');

    }
  };

  /**
   * Perform DonorPerfect search and display results.
   */
  $.fn.DonorPerfectFormElementNamePerformSearch = function (element) {
    var $name_wrapper = $(element).closest('div.donorperfect-name-wrapper');
    var element_token = $name_wrapper.attr('element-token');
    if (drupalSettings.hasOwnProperty('donorPerfectNameSearch')) {
      if (drupalSettings.donorPerfectNameSearch.hasOwnProperty(element_token)) {
        if (drupalSettings.donorPerfectNameSearch[element_token].searching) {
          return;
        }
      } else {
        drupalSettings.donorPerfectNameSearch[element_token] = {searching: true};
      }
    } else {
      drupalSettings.donorPerfectNameSearch = {};
      drupalSettings.donorPerfectNameSearch[element_token] = {searching: true};
    }
    var donor_type = $name_wrapper.find('div.donor-type-wrapper > select').val();
    var last_name = $name_wrapper.find('div.last-name-wrapper > input').val();
    var first_name = $name_wrapper.find('div.first-name-wrapper > input').val();
    if (last_name) {
      var ajaxSettings = {
        url: '/donorperfect/form_element/name/search',
        submit: {element_token, donor_type, last_name, first_name}
      };
      Drupal.ajax(ajaxSettings).execute();
      $.fn.DonorPerfectFormElementNameSearchSwitchLoadingResults(JSON.stringify({show: 'searching', element_token}));
    }
  };

  /**
   * Switch between loading graphic and search results.
   */
  $.fn.DonorPerfectFormElementNameSearchSwitchLoadingResults = function (settings) {
    var data = jQuery.parseJSON(settings);
    var element_token = '';
    if (data.hasOwnProperty('element_token') && data.element_token.length) {
      element_token = data.element_token;
    } //end if
    if (element_token.length) {
      var search_wrapper = 'div#donorperfect-name-search-wrapper-' + element_token;
      $(search_wrapper).children().hide();
      if (data.show === 'results') {
        $(search_wrapper + ' > .donorperfect-name-search-results').show();
        drupalSettings.donorPerfectNameSearch[element_token].searching = false;
      } else if (data.show === 'searching') {
        $(search_wrapper + ' > .donorperfect-name-search-searching').show();
      }
    }
  };

  /**
   * Reset all DonorPerfect fields to default values.
   */
  $.fn.DonorPerfectFormElementNameSearchResetFields = function (settings) {
    var data = jQuery.parseJSON(settings);
    if (data.element_token) {
      var $form = $('div#donorperfect-element-wrapper-' + data.element_token).closest('form');
    } else {
      return;
    }
    $form.find('input.dpfield').each(function (index, item) {
      var element_type = $(this).attr('type');
      switch (element_type) {
        case "checkbox":
        case "radio":
          this.checked = item.defaultChecked;
          $(this).trigger('change');
          break;

        default:
          $(this).val(function () {
            return this.defaultValue;
          });
          break;

      }
    });
    $form.find('textarea.dpfield').each(function (index) {
      $(this).val(function () {
        return this.defaultValue;
      });
    });
    $form.find('select.dpfield').each(function (index) {
      var orig_option = '#' + $(this).attr('id') + ' option[selected]';
      $(this).val(function () {
        return $(orig_option).val();
      });
      $(this).trigger('change');
    });
    // Hidden Donor ID field has to be cleared manually.
    $form.find('input[dpfield="donor_id"]').val('');
  };

})(jQuery, window.Drupal, window.drupalSettings);
