<?php

namespace Drupal\donorperfect_contact\Entity;

use Drupal\donorperfect\Entity\ControllerBase;

/**
 * Controller class for DonorPerfect Contact entities.
 */
class ContactController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  const ENTITY_TYPE_ID = 'donorperfect_contact';

  /**
   * {@inheritdoc}
   */
  public function getSettingsFormClass() {
    return '\Drupal\donorperfect_contact\Form\SettingsForm';
  }

}
