<?php

namespace Drupal\donorperfect_contact\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\donorperfect\Entity\EntityBase;

/**
 * Defines the entity.
 *
 * @ingroup donorperfect_contact
 *
 * @ContentEntityType(
 *   id = "donorperfect_contact",
 *   label = @Translation("DonorPerfect Contact"),
 *   label_collection = @Translation("DonorPerfect Contacts"),
 *   label_singular = @Translation("contact"),
 *   label_plural = @Translation("contacts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count contact",
 *     plural = "@count contacts",
 *   ),
 *   base_table = "dpcontact",
 *   udf_table = "dpcontactudf",
 *   entity_keys = {
 *     "id" = "contact_id",
 *     "label" = "contact_id",
 *   },
 *   handlers = {
 *     "storage" = "\Drupal\donorperfect\Entity\Storage",
 *     "storage_schema" = "\Drupal\donorperfect\Entity\StorageSchema",
 *     "access" = "\Drupal\donorperfect\Entity\AccessControlHandler",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "\Drupal\donorperfect_contact\Entity\ContactViewBuilder",
 *     "views_data" = "\Drupal\donorperfect\Entity\ViewsData",
 *     "form" = {
 *       "add" = "\Drupal\donorperfect_contact\Form\ContactForm",
 *       "edit" = "\Drupal\donorperfect_contact\Form\ContactForm",
 *     },
 *   },
 *   admin_permission = "donorperfect admin",
 *   links = {
 *     "canonical" = "/admin/donorperfect/contact/{donorperfect_contact}",
 *     "add-form" = "/admin/donorperfect/contact/add",
 *     "edit-form" = "/admin/donorperfect/contact/{donorperfect_contact}/edit",
 *     "collection" = "/admin/donorperfect/contact/list"
 *   },
 * )
 */
class Contact extends EntityBase implements ContactInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    if (\Drupal::moduleHandler()->moduleExists('donorperfect_donor')) {
      $fields['donor_id'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Donor'))
        ->setDescription(t('The donor this contact belongs to.'))
        ->setSetting('target_type', 'donorperfect_donor');
    }

    return $fields;
  }

}
