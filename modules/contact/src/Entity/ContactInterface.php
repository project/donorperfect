<?php

namespace Drupal\donorperfect_contact\Entity;

/**
 * Provides an interface to be implemented by contact entities.
 */
interface ContactInterface {}
