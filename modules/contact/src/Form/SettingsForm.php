<?php

namespace Drupal\donorperfect_contact\Form;

use Drupal\donorperfect\Form\EntitySettingsFormBase;

/**
 * Form controller for DonorPerfect Contact settings.
 *
 * @ingroup donorperfect_contact
 */
class SettingsForm extends EntitySettingsFormBase {

  /**
   * {@inheritdoc}
   */
  const ENTITY_TYPE_ID = 'donorperfect_contact';

}
