<?php

namespace Drupal\donorperfect_other\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\donorperfect\Entity\EntityBase;

/**
 * Defines the entity.
 *
 * @ingroup donorperfect_other
 *
 * @ContentEntityType(
 *   id = "donorperfect_other",
 *   label = @Translation("DonorPerfect Other Info"),
 *   label_collection = @Translation("DonorPerfect Other Infos"),
 *   label_singular = @Translation("other info"),
 *   label_plural = @Translation("other infos"),
 *   label_count = @PluralTranslation(
 *     singular = "@count other info",
 *     plural = "@count other infos",
 *   ),
 *   base_table = "dpotherinfo",
 *   udf_table = "dpotherinfoudf",
 *   entity_keys = {
 *     "id" = "other_id",
 *     "label" = "other_id",
 *   },
 *   handlers = {
 *     "storage" = "\Drupal\donorperfect\Entity\Storage",
 *     "storage_schema" = "\Drupal\donorperfect\Entity\StorageSchema",
 *     "access" = "\Drupal\donorperfect\Entity\AccessControlHandler",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "\Drupal\donorperfect_other\Entity\OtherViewBuilder",
 *     "views_data" = "\Drupal\donorperfect\Entity\ViewsData",
 *     "form" = {
 *       "add" = "\Drupal\donorperfect_other\Form\OtherForm",
 *       "edit" = "\Drupal\donorperfect_other\Form\OtherForm",
 *     },
 *   },
 *   admin_permission = "donorperfect admin",
 *   links = {
 *     "canonical" = "/admin/donorperfect/other/{donorperfect_other}",
 *     "add-form" = "/admin/donorperfect/other/add",
 *     "edit-form" = "/admin/donorperfect/other/{donorperfect_other}/edit",
 *     "collection" = "/admin/donorperfect/other/list"
 *   },
 * )
 */
class Other extends EntityBase implements OtherInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    if (\Drupal::moduleHandler()->moduleExists('donorperfect_donor')) {
      $fields['donor_id'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Donor'))
        ->setDescription(t('The donor this other belongs to.'))
        ->setSetting('target_type', 'donorperfect_donor');
    }

    return $fields;
  }

}
