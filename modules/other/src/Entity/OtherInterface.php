<?php

namespace Drupal\donorperfect_other\Entity;

/**
 * Provides an interface to be implemented by other info entities.
 */
interface OtherInterface {}
