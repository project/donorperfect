<?php

namespace Drupal\donorperfect_other\Entity;

use Drupal\donorperfect\Entity\ControllerBase;

/**
 * Controller class for DonorPerfect Other entities.
 */
class OtherController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  const ENTITY_TYPE_ID = 'donorperfect_other';

  /**
   * {@inheritdoc}
   */
  public function getSettingsFormClass() {
    return '\Drupal\donorperfect_other\Form\SettingsForm';
  }

}
