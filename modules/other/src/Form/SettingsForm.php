<?php

namespace Drupal\donorperfect_other\Form;

use Drupal\donorperfect\Form\EntitySettingsFormBase;

/**
 * Form controller for DonorPerfect Other settings.
 *
 * @ingroup donorperfect_other
 */
class SettingsForm extends EntitySettingsFormBase {

  /**
   * {@inheritdoc}
   */
  const ENTITY_TYPE_ID = 'donorperfect_other';

}
