<?php

namespace Drupal\donorperfect_donor;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed field item list for the 'name_full' donorperfect_donor field.
 */
class DonorNameFullFieldItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\donorperfect_donor\Entity\DonorInterface $donor */
    $donor = $this->getEntity();
    $first_name = '';
    if ($donor->hasField('first_name')) {
      $first_name = $donor->get('first_name')->first();
      $first_name = !empty($first_name) ? trim($first_name->getString()) : '';
    }
    $last_name = $donor->hasField('last_name') ? trim($donor->get('last_name')->first()->getString()) : '';
    $full_name = trim($first_name . ' ' . $last_name);
    $this->list[0] = $this->createItem(0, $full_name);
  }

}
