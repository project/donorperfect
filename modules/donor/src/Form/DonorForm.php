<?php

namespace Drupal\donorperfect_donor\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the entity edit forms.
 *
 * @ingroup donorperfect_donor
 */
class DonorForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#after_build'] = ['::afterBuildForm'];
    return $form;
  }

  /**
   * Perform necessary functions after the form is built.
   *
   * @param array $form
   *   The complete form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object for the form.
   *
   * @return array
   *   Return the updated $form array.
   */
  public function afterBuildForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The donor %feed has been updated.', ['%feed' => $entity->toLink()->toString()]);
    }
    else {
      $message = $this->t('The donor %feed has been added.', ['%feed' => $entity->toLink()->toString()]);
    }
    $this->messenger->addMessage($message);
    $form_state->setRedirectUrl($this->entity->toUrl('canonical'));
    return $status;
  }

}
