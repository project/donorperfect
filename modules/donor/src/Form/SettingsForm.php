<?php

namespace Drupal\donorperfect_donor\Form;

use Drupal\donorperfect\Form\EntitySettingsFormBase;

/**
 * Form controller for DonorPerfect Donor settings.
 *
 * @ingroup donorperfect_donor
 */
class SettingsForm extends EntitySettingsFormBase {

  /**
   * {@inheritdoc}
   */
  const ENTITY_TYPE_ID = 'donorperfect_donor';

}
