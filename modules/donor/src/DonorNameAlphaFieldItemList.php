<?php

namespace Drupal\donorperfect_donor;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed field item list for the 'name_alpha' donorperfect_donor field.
 *
 * This field is for a donor name formatted to be used in alphabetized lists.
 */
class DonorNameAlphaFieldItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\donorperfect_donor\Entity\DonorInterface $donor */
    $donor = $this->getEntity();
    $first_name = $donor->hasField('first_name') ? trim($donor->get('first_name')->first()->getString()) : '';
    $last_name = $donor->hasField('last_name') ? trim($donor->get('last_name')->first()->getString()) : '';
    $alpha_name = '';
    if (!empty($last_name) && !empty($first_name)) {
      $alpha_name = trim($last_name . ', ' . $first_name);
    }
    elseif (!empty($last_name)) {
      $alpha_name = $last_name;
    }
    elseif (!empty($first_name)) {
      $alpha_name = $first_name;
    }
    $this->list[0] = $this->createItem(0, $alpha_name);
  }

}
