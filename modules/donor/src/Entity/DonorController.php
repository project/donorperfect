<?php

namespace Drupal\donorperfect_donor\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\donorperfect\DPQuery;
use Drupal\donorperfect\DPUtility;
use Drupal\donorperfect\Entity\ControllerBase;
use Drupal\donorperfect\FormValidator;

/**
 * Controller class for DonorPerfect Donor entities.
 */
class DonorController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  const ENTITY_TYPE_ID = 'donorperfect_donor';

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The DPQuery service.
   *
   * @var \Drupal\donorperfect\DPQuery
   */
  protected $dpQuery;

  /**
   * The DPUtility service.
   *
   * @var \Drupal\donorperfect\DPUtility
   */
  protected $dpUtility;

  /**
   * Constructs a new \Drupal\donorperfect_donor\Entity\DonorController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config.factory service.
   * @param \Drupal\donorperfect\DPQuery $dpquery
   *   The DPQuery service.
   * @param \Drupal\donorperfect\DPUtility $dputility
   *   The DPUtility service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, DPQuery $dpquery, DPUtility $dputility) {
    $this->configFactory = $config_factory;
    $this->dpQuery = $dpquery;
    $this->dpUtility = $dputility;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsFormClass() {
    return '\Drupal\donorperfect_donor\Form\SettingsForm';
  }

  /**
   * Search for DonorPerfect donors that match given conditions.
   *
   * @param array $conditions
   *   Array of conditions to search for.
   * @param string $join_type
   *   Type of table join to be used in the query.
   *
   * @return array
   *   Array of donor_ids that match given conditions.
   */
  public function search(array $conditions = [], string $join_type = 'left') {
    $results = [];
    if (!empty($conditions)) {
      $dptables = $this->dpUtility->getCache('tables');
      $query = $this->dpQuery->create('select', 'dp.donor_id');
      $query->setJoinType($join_type);
      foreach ($conditions as $key => $info) {
        // Check if condition is in the format table.field => value.
        if ($dot = strpos($key, '.')) {
          $info = [
            'table' => substr($key, 0, $dot),
            'field' => substr($key, $dot + 1),
            'value' => $info,
          ];
        }
        // Check if the condition is already compiled.
        elseif (is_string($info)) {
          if (preg_match('/(?:=|>|<|LIKE|NULL)/', $info)) {
            $query->addCondition($info);
          }
          continue;
        }
        // Compile condition based on which field is being queried.
        if (($info['field'] === 'donor_id') && !empty($info['value'])) {
          $donor_id_conditions = [];
          if (is_array($info['value'])) {
            foreach ($info['value'] as $donor_id) {
              if (is_numeric($donor_id)) {
                $donor_id_conditions[] = '(dp.donor_id=' . (int) $donor_id . ')';
              }
            }
          }
          elseif (is_numeric($info['value'])) {
            $donor_id_conditions[] = '(dp.donor_id=' . (int) $donor_id . ')';
          }
          if (!empty($donor_id_conditions)) {
            $donor_id_condition = '(' . implode(' OR ', $donor_id_conditions) . ')';
            $query->addCondition($donor_id_condition);
          }
        }
        elseif (($info['field'] === 'first_name') && !empty($info['value'])) {
          // Test for different variations on the first name.
          $variations = $this->configFactory->get('donorperfect.settings')->get('first_name_variations') ?? [];
          $variations_added = [];
          $name_conditions = [];
          $table = $info['table'] ?? 'dp';
          $first_names = $this->explodeFirstNames(strtolower($info['value']));
          foreach ($first_names as $first_name) {
            $first_name_added = FALSE;
            foreach ($variations as $group) {
              if (in_array($first_name, $group)) {
                $first_name_added = TRUE;
                foreach ($group as $variation) {
                  if (!in_array($variation, $variations_added)) {
                    $name_conditions[] = "({$table}.first_name LIKE '%" . str_replace("'", "''", $variation) . "%')";
                    $variations_added[] = $variation;
                  }
                }
              }
            }
            if (!$first_name_added) {
              // Do alpha_dash validation on the value to protect against
              // SQL injection.
              if (FormValidator::validateAlphaDash($first_name)) {
                $name_conditions[] = "({$table}.first_name LIKE '%" . str_replace("'", "''", $first_name) . "%')";
              }
            }
          }
          if (!empty($name_conditions)) {
            $condition = '(' . implode(' OR ', $name_conditions) . ')';
            $query->addCondition($condition);
          }
        }
        elseif (($info['field'] === 'email') && !empty($info['value'])) {
          $email_conditions = [];
          if (is_array($info['value'])) {
            $emails_added = [];
            foreach ($info['value'] as $email) {
              if (is_string($email) && FormValidator::validateEmail($email) && !in_array($email, $emails_added)) {
                $email_conditions[] = "(dp.email LIKE '{$email}') OR (dpaddress.email LIKE '{$email}')";
                $emails_added[] = $email;
              }
            }
          }
          elseif (is_string($info['value']) && FormValidator::validateEmail($info['value'])) {
            $email_conditions[] = "(dp.email LIKE '{$info['value']}') OR (dpaddress.email LIKE '{$info['value']}')";
          }
          if (!empty($email_conditions)) {
            $condition = '(' . implode(' OR ', $email_conditions) . ')';
            $query->addCondition($condition);
            $query->addField('dpaddress.donor_id');
          }
        }
        elseif ((strpos($info['field'], 'phone') !== FALSE) && !empty($info['value'])) {
          $phone_conditions = [];
          if (is_array($info['value'])) {
            $phones_added = [];
            foreach ($info['value'] as $phone) {
              if (is_string($phone) && FormValidator::validatePhone($phone)) {
                $phone = FormValidator::filterPhone($phone, 'parentheses');
                if (!in_array($phone, $phones_added)) {
                  foreach (['home', 'business', 'mobile', 'fax'] as $phone_type) {
                    $phone_conditions[] = "(dp.{$phone_type}_phone LIKE '{$phone}') OR (dpaddress.{$phone_type} LIKE '{$phone}')";
                  }
                  $phones_added[] = $phone;
                }
              }
            }
          }
          elseif (is_string($info['value']) && FormValidator::validatePhone($info['value'])) {
            $phone = FormValidator::filterPhone($info['value'], 'parentheses');
            foreach (['home', 'mobile', 'business', 'fax'] as $phone_type) {
              $phone_conditions[] = "(dp.{$phone_type}_phone LIKE '{$phone}') OR (dpaddress.{$phone_type} LIKE '{$phone}')";
            }
          }
          if (!empty($phone_conditions)) {
            $condition = '(' . implode(' OR ', $phone_conditions) . ')';
            $query->addCondition($condition);
            $query->addField('dpaddress.donor_id');
          }
        }
        elseif (($address_fields = ['address', 'city', 'state', 'zip']) &&
                in_array($info['field'], $address_fields) && !empty($info['value'])) {
          $address_conditions = [];
          if (is_array($info['value'])) {
            foreach ($info['value'] as $value) {
              if (is_scalar($value) && FormValidator::validateAlphaDash($value)) {
                $address_conditions[] = "(dp.{$info['field']} LIKE '{$value}') OR (dpaddress.{$info['field']} LIKE '{$value}%')";
              }
            }
          }
          elseif (is_scalar($info['value']) && FormValidator::validateAlphaDash($info['value'])) {
            $address_conditions[] = "(dp.{$info['field']} LIKE '{$info['value']}') OR (dpaddress.{$info['field']} LIKE '{$info['value']}%')";
          }
          if (!empty($address_conditions)) {
            $condition = '(' . implode(' OR ', $address_conditions) . ')';
            $query->addCondition($condition);
            $query->addField('dpaddress.donor_id');
          }
        }
        elseif (!empty($info['table']) && !empty($info['field']) &&
               ($nested_array_search = [$info['table'], $info['field']]) &&
               ($field_info = NestedArray::getValue($dptables, $nested_array_search))) {
          $wildcard = '';
          switch ($field_info['mysql_data_type']) {
            case 'varchar':
            case 'nvarchar':
            case 'char':
            case 'text':
              $operator = ' LIKE ';
              if (!empty($info['wildcard'])) {
                $wildcard = $info['wildcard'];
              }
              break;

            default:
              $operator = '=';
              break;

          }
          if (isset($info['operator'])) {
            $operator = $info['operator'];
          }
          if (!empty($info['value']) && is_array($info['value'])) {
            $temp_conditions = [];
            foreach ($info['value'] as $temp_value) {
              if (is_scalar($temp_value) && FormValidator::validateAlphaDash((string) $temp_value)) {
                $temp_value = static::addWildcard((string) $temp_value, $wildcard);
                $value = $field_info['quote'] ? "'" . str_replace("'", "''", $temp_value) . "'" : $temp_value;
                $temp_conditions[] = $info['table'] . '.' . $info['field'] . $operator . $value;
              }
            }
            if (!empty($temp_conditions)) {
              $condition = '(' . implode(' OR ', $temp_conditions) . ')';
              $query->addCondition($condition);
            }
            else {
              continue;
            }
          }
          elseif (empty($info['value']) || is_scalar($info['value'])) {
            $value = '';
            if (!empty($info['value'])) {
              if (!FormValidator::validateAlphaDash((string) $info['value'])) {
                continue;
              }
              $info['value'] = static::addWildcard((string) $info['value'], $wildcard);
              $value = $field_info['quote'] ? "'" . str_replace("'", "''", $info['value']) . "'" : $info['value'];
            }
            $condition = $info['table'] . '.' . $info['field'] . $operator . $value;
            $query->addCondition($condition);
          }
          // Make sure the table is included in the query.
          if ($info['table'] !== 'dp') {
            $matches = [];
            if (preg_match('/^dp(.*)udf$/', $info['table'], $matches)) {
              $query->addField($info['table'] . '.' . $matches[1] . '_id');
            }
            else {
              $query->addField($info['table'] . '.donor_id');
            }
          }
        }
      }
      if ($query->hasConditions()) {
        // Set ORDER BY.
        $query->addOrder('dp.last_name');
        $query->addOrder('dp.first_name');
        // Execute the query.
        $query->execute();
        // Reformat results into an array of donor_ids.
        if (!empty($query->result) && is_array($query->result)) {
          foreach ($query->result as $result) {
            if (is_array($result) && !empty($result['donor_id'])) {
              $results[$result['donor_id']] = (int) $result['donor_id'];
            }
          }
        }
      }
    }
    return $results;
  }

  /**
   * Add wildcard to search string in specified location(s).
   *
   * @param string $input
   *   The string to add wildcard(s) to.
   * @param string $location
   *   The location(s) to add wildcard(s).
   *
   * @return string
   *   The string with the wildcard(s) added.
   */
  public function addWildcard(string $input, string $location) {
    $output = $input;
    switch ($location) {
      case 'before':
        $output = '%' . $input;
        break;

      case 'after':
        $output = $input . '%';
        break;

      case 'both':
        $output = '%' . $input . '%';
        break;

    }
    return $output;
  }

  /**
   * Convert first_name string into an array of first_names.
   *
   * @param string $first_name
   *   String containing first_name(s).
   *
   * @return array
   *   Array of first_names.
   */
  public function explodeFirstNames(string $first_name) {
    $first_names = [];
    // Explode on the word 'and'.
    $temps = explode(' and ', $first_name);
    // Explode on commas.
    foreach ($temps as $temp) {
      $first_names = array_merge($first_names, explode(',', $temp));
    }
    // Trim all $first_names.
    foreach ($first_names as $key => $value) {
      $value = trim($value);
      if (!empty($value)) {
        $first_names[$key] = $value;
      }
      else {
        unset($first_names[$key]);
      }
    }
    return array_unique($first_names);
  }

  /**
   * Load additional addresses for the provided donor entities.
   *
   * @param array $donors
   *   Array of donor entities.
   */
  public function loadAddresses(array &$donors) {
    $donor_ids = $address_records = $addresses = $emails = $phones = [];
    foreach ($donors as $donor) {
      if ($donor instanceof Donor) {
        $donor_id = $donor->id();
        if (!in_array($donor_id, $donor_ids)) {
          $donor_ids[] = $donor_id;
        }
      }
    }
    if (!empty($donor_ids)) {
      $query = $this->dpQuery->create('select', 'dpaddress.address_id');
      $query->addField('*')
        ->addCondition('dpaddress.donor_id IN (' . implode(',', $donor_ids) . ')')
        ->execute();
      foreach ($query->result as $result) {
        if (is_array($result) && !empty($result['donor_id'])) {
          $address_records[$result['donor_id']][] = $result;
          if (!isset($addresses[$result['donor_id']])) {
            $addresses[$result['donor_id']] = [];
          }
          $address = $this->dpUtility->getAddressString($result);
          if (!empty($address) && !in_array($address, $addresses[$result['donor_id']])) {
            $addresses[$result['donor_id']][] = $address;
          }
          if (!isset($emails[$result['donor_id']])) {
            $emails[$result['donor_id']] = [];
          }
          if (!empty($result['email']) && !in_array($result['email'], $emails[$result['donor_id']])) {
            $emails[$result['donor_id']][] = $result['email'];
          }
          if (!isset($phones[$result['donor_id']])) {
            $phones[$result['donor_id']] = [];
          }
          foreach (['home', 'mobile', 'business', 'fax'] as $phone_type) {
            if (!empty($result["{$phone_type}_phone"]) && !in_array($result["{$phone_type}_phone"], $phones[$result['donor_id']])) {
              $phones[$result['donor_id']][] = $result["{$phone_type}_phone"];
            }
          }
        }
      }
    }
    foreach ($donors as $key => $donor) {
      if ($donor instanceof Donor) {
        $donor_id = $donor->id();
        if (!empty($address_records[$donor_id])) {
          $donors[$key]->setData('address_records', $address_records[$donor_id]);
        }
        if (!empty($addresses[$donor_id])) {
          $donors[$key]->setData('addresses', $addresses[$donor_id]);
        }
        if (!empty($emails[$donor_id])) {
          $donors[$key]->setData('emails', $emails[$donor_id]);
        }
        if (!empty($phones[$donor_id])) {
          $donors[$key]->setData('phones', $phones[$donor_id]);
        }
      }
    }
  }

}
