<?php

namespace Drupal\donorperfect_donor\Entity;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view builder for the entity.
 *
 * @ingroup donorperfect_donor
 */
class DonorViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function build(array $build) {
    $build = parent::build($build);
    // $entity = $build['#' . $build['#entity_type']];
    $build['content'] = [];
    return $build;
  }

}
