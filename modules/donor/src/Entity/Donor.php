<?php

namespace Drupal\donorperfect_donor\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\donorperfect\Entity\EntityBase;
use Drupal\donorperfect_donor\DonorNameAlphaFieldItemList;
use Drupal\donorperfect_donor\DonorNameFullFieldItemList;

/**
 * Defines the entity.
 *
 * @ingroup donorperfect_donor
 *
 * @ContentEntityType(
 *   id = "donorperfect_donor",
 *   label = @Translation("DonorPerfect Donor"),
 *   label_collection = @Translation("DonorPerfect Donors"),
 *   label_singular = @Translation("donor"),
 *   label_plural = @Translation("donors"),
 *   label_count = @PluralTranslation(
 *     singular = "@count donor",
 *     plural = "@count donors",
 *   ),
 *   base_table = "dp",
 *   udf_table = "dpudf",
 *   entity_keys = {
 *     "id" = "donor_id",
 *     "label" = "name_full",
 *   },
 *   handlers = {
 *     "storage" = "\Drupal\donorperfect\Entity\Storage",
 *     "storage_schema" = "\Drupal\donorperfect\Entity\StorageSchema",
 *     "access" = "\Drupal\donorperfect\Entity\AccessControlHandler",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "\Drupal\donorperfect_donor\Entity\DonorViewBuilder",
 *     "views_data" = "\Drupal\donorperfect\Entity\ViewsData",
 *     "form" = {
 *       "add" = "\Drupal\donorperfect_donor\Form\DonorForm",
 *       "edit" = "\Drupal\donorperfect_donor\Form\DonorForm",
 *     },
 *   },
 *   admin_permission = "donorperfect admin",
 *   links = {
 *     "canonical" = "/admin/donorperfect/donor/{donorperfect_donor}",
 *     "add-form" = "/admin/donorperfect/donor/add",
 *     "edit-form" = "/admin/donorperfect/donor/{donorperfect_donor}/edit",
 *     "collection" = "/admin/donorperfect/donor/list"
 *   },
 * )
 */
class Donor extends EntityBase implements DonorInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['name_alpha'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Alphabetical Name'))
      ->setDescription(t('The full name of the donor, formatted for alphabetical lists.'))
      ->setSetting('max_length', 512)
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setClass(DonorNameAlphaFieldItemList::class);
    $fields['name_full'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Full Name'))
      ->setDescription(t('The full name of the donor.'))
      ->setSetting('max_length', 512)
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setClass(DonorNameFullFieldItemList::class);
    return $fields;
  }

}
