<?php

namespace Drupal\donorperfect_donor\Entity;

/**
 * Provides an interface to be implemented by donor entities.
 */
interface DonorInterface {}
