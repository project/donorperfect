<?php

namespace Drupal\donorperfect_gift\Entity;

use Drupal\donorperfect\Entity\ControllerBase;

/**
 * Controller class for DonorPerfect Gift entities.
 */
class GiftController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  const ENTITY_TYPE_ID = 'donorperfect_gift';

  /**
   * {@inheritdoc}
   */
  public function getSettingsFormClass() {
    return '\Drupal\donorperfect_gift\Form\SettingsForm';
  }

}
