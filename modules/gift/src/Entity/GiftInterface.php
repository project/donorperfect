<?php

namespace Drupal\donorperfect_gift\Entity;

/**
 * Provides an interface to be implemented by gift entities.
 */
interface GiftInterface {}
