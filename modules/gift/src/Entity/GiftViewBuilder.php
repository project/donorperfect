<?php

namespace Drupal\donorperfect_gift\Entity;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view builder for the entity.
 *
 * @ingroup donorperfect_gift
 */
class GiftViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function build(array $build) {
    $build = parent::build($build);
    // $entity = $build['#' . $build['#entity_type']];
    $build['content'] = [];
    return $build;
  }

}
