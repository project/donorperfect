<?php

namespace Drupal\donorperfect_gift\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\donorperfect\Entity\EntityBase;

/**
 * Defines the entity.
 *
 * @ingroup donorperfect_gift
 *
 * @ContentEntityType(
 *   id = "donorperfect_gift",
 *   label = @Translation("DonorPerfect Gift"),
 *   label_collection = @Translation("DonorPerfect Gifts"),
 *   label_singular = @Translation("gift"),
 *   label_plural = @Translation("gifts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count gift",
 *     plural = "@count gifts",
 *   ),
 *   base_table = "dpgift",
 *   udf_table = "dpgiftudf",
 *   entity_keys = {
 *     "id" = "gift_id",
 *     "label" = "gift_id",
 *   },
 *   handlers = {
 *     "storage" = "\Drupal\donorperfect\Entity\Storage",
 *     "storage_schema" = "\Drupal\donorperfect\Entity\StorageSchema",
 *     "access" = "\Drupal\donorperfect\Entity\AccessControlHandler",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "\Drupal\donorperfect_gift\Entity\GiftViewBuilder",
 *     "views_data" = "\Drupal\donorperfect\Entity\ViewsData",
 *     "form" = {
 *       "add" = "\Drupal\donorperfect_gift\Form\GiftForm",
 *       "edit" = "\Drupal\donorperfect_gift\Form\GiftForm",
 *     },
 *   },
 *   admin_permission = "donorperfect admin",
 *   links = {
 *     "canonical" = "/admin/donorperfect/gift/{donorperfect_gift}",
 *     "add-form" = "/admin/donorperfect/gift/add",
 *     "edit-form" = "/admin/donorperfect/gift/{donorperfect_gift}/edit",
 *     "collection" = "/admin/donorperfect/gift/list"
 *   },
 * )
 */
class Gift extends EntityBase implements GiftInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    if (\Drupal::moduleHandler()->moduleExists('donorperfect_donor')) {
      $fields['donor_id'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Donor'))
        ->setDescription(t('The donor this gift belongs to.'))
        ->setSetting('target_type', 'donorperfect_donor');
    }

    return $fields;
  }

}
