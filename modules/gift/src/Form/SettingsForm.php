<?php

namespace Drupal\donorperfect_gift\Form;

use Drupal\donorperfect\Form\EntitySettingsFormBase;

/**
 * Form controller for DonorPerfect Gift settings.
 *
 * @ingroup donorperfect_gift
 */
class SettingsForm extends EntitySettingsFormBase {

  /**
   * {@inheritdoc}
   */
  const ENTITY_TYPE_ID = 'donorperfect_gift';

}
