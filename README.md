# DonorPerfect

This module provides integration between Drupal and
[DonorPerfect](https://donorperfect.com), which is a CRM for nonprofit
organizations.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/donorperfect).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/donorperfect).



## Table of contents


- Requirements
- Installation
- Configuration
- Maintainers



## Requirements


This module requires the following modules:


- [Entity API](https://www.drupal.org/project/entity)
- [Address](https://www.drupal.org/project/address)



## Installation


Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).



## Configuration


1. Go to the settings page (/admin/donorperfect/settings) and enter your API
   credentials (either a username and password, or preferably, an API key).
   Click the 'Save configuration' button at the bottom of the form to save
   your API credentials.
3. After you have saved your API credentials, click the button in the
   'DonorPerfect Cache' dropdown to populate the local metadata cache as
   described in the Features section above.
4. Also on the settings page, use the 'Drupal Entity Settings' fieldset to
   select which DonorPerfect fields should be included on the Drupal entities.
   The fields selected here will be the fields available when creating Views
   or when loading entities in custom modules.
5. Configure permissions. In order to use the search functionality on the Name
   form element as described in the Features section above, the Drupal user
   must have the 'Use DonorPerfect' permission.



## Maintainers


- Brandon Post - [brandonpost](https://www.drupal.org/u/brandonpost)

