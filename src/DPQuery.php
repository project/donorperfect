<?php

namespace Drupal\donorperfect;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use GuzzleHttp\Client;

/**
 * The DPQuery class.
 *
 * Provides capability to execute DonorPerfect predefined procedures
 * or execute custom SQL queries on the DonorPerfect database.
 */
class DPQuery {

  /**
   * The DonorPerfect XML API endpoint URL.
   */
  const DONORPERFECT_API_URL = 'https://www.donorperfect.net/prod/xmlrequest.asp';

  /**
   * The DPUtility service.
   *
   * @var \Drupal\donorperfect\DPUtility
   */
  protected $dpUtility;

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * List the valid types of queries that can be run by DPQuery.
   *
   * @var array
   */
  protected $validTypes = [
    'select',
    'insert',
    'update',
    'pass',
    'retrieveAllTableData',
  ];

  /**
   * List the DonorPerfect predefined procedures that can be run by DPQuery.
   *
   * @var array
   */
  protected $predefinedProcs = [
    'donorsearch',
    'gifts',
    'savedonor',
    'savegift',
    'saveotherinfo',
    'savecontact',
    'delflags',
    'saveflag',
    'saveudf',
  ];

  /**
   * Whether or not a predefined procedure is being used.
   *
   * @var array
   */
  protected $isProc = FALSE;

  /**
   * Array of fields required for the predefined procedure being used.
   *
   * @var array
   */
  protected $procFields = [];

  /**
   * The availTables array.
   *
   * List the tables that are accessible by the XML API in DonorPerfect
   * for queries.
   *
   * @var array
   */
  protected $availTables = [
    'dp',
    'dpudf',
    'dpgift',
    'dpgiftudf',
    'dpaddress',
    'dpaddressmailings',
    'dplink',
    'dpotherinfo',
    'dpotherinfoudf',
    'dppaymentmethod',
    'dpcontact',
    'dpcontactudf',
    'dpusermultivalues',
    'dpcodes',
    'dpflags',
    'dpbatch',
  ];

  /**
   * Array of fields accessible by the XML API query, keyed by table name.
   *
   * @var array
   */
  protected $availFields = [];

  /**
   * Authentication string to be used for calls to the XML API.
   *
   * @var string
   */
  protected $authString;

  /**
   * Type of query.
   *
   * Set the type of query: select, insert, update, or
   * pass (for a pass-thru sql statement).
   *
   * @var string
   */
  protected $type = '';

  /**
   * DonorPerfect table to be used as the base table for the query.
   *
   * @var string
   */
  protected $baseTable = '';

  /**
   * The baseField of the query.
   *
   * DonorPerfect field (from $baseTable) to be used as the base field for
   * the query.
   *
   * @var string
   */
  protected $baseField = '';

  /**
   * Array of fields to be retrieved by a select query.
   *
   * This should be a 2-dimensional array of tables => fields.
   *
   * @var array
   */
  protected $tablesFields = [];

  /**
   * Array of SQL conditions for the query.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * Array of ORDER BY clauses for the query.
   *
   * @var array
   */
  protected $orderBy = [];

  /**
   * ORDER BY clauses imploded to a string.
   *
   * @var string
   */
  protected $orderByString = '';

  /**
   * Array of fields and values to be updated by an update query.
   *
   * @var array
   */
  protected $updateFields = [];

  /**
   * Complied SQL statement.
   *
   * @var string
   */
  protected $sql = '';

  /**
   * Value of the 'action' parameter in the API call.
   *
   * This is based on which predefined procedure/query is being run.
   *
   * @var string
   */
  protected $dpAction = '';

  /**
   * Array of parameters passed to the query object.
   *
   * @var array
   */
  protected $params = [];

  /**
   * Query object parameters compiled to a string.
   *
   * @var string
   */
  protected $paramsString = '';

  /**
   * Final compiled API call string.
   *
   * @var string
   */
  protected $apiCall = '';

  /**
   * Type of SQL join to be used in the query.
   *
   * @var string
   */
  protected $joinType = ' INNER JOIN ';

  /**
   * Maximum number of records to be returned by a select query.
   *
   * @var int|null
   */
  protected $limit = NULL;

  /**
   * Offset for paged query results.
   *
   * @var int|null
   */
  protected $offset = NULL;

  /**
   * Array of options to pass to the http client.
   *
   * @var array
   */
  protected $httpRequestOptions = ['timeout' => 30];

  /**
   * The getCount property.
   *
   * Boolean indicating whether or not the query should return a count
   * rather than specific fields.
   *
   * @var bool
   */
  public $getCount = FALSE;

  /**
   * HTTP result code for the API call (200 for success).
   *
   * @var string|int
   */
  public $resultCode = '';

  /**
   * Array of query results.
   *
   * @var array
   */
  public $result = [];

  /**
   * Name of the currently logged-in user.
   *
   * @var string
   */
  public $userName = '';

  /**
   * Constructs a new \Drupal\donorperfect\DPQuery object.
   *
   * @param \Drupal\donorperfect\DPUtility $dputility
   *   The DPUtility service.
   * @param \GuzzleHttp\Client $http_client
   *   The Guzzle http client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config.factory service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(DPUtility $dputility, Client $http_client, ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user) {
    $this->dpUtility = $dputility;
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;

    $this->validTypes = array_merge($this->validTypes, $this->predefinedProcs);

    $this->availFields = $this->configFactory->get('donorperfect.cache')->get('dptables') ?? [];

    // Prepare authentication string.
    $this->authString = '';
    $credentials = $this->dpUtility->apiCredentialsLoad('dpquery');
    if (!empty($credentials['key'])) {
      $this->authString = "apikey={$credentials['key']}";
    }
    elseif (!empty($credentials['user']) && !empty($credentials['pw'])) {
      $this->authString = "login={$credentials['user']}&pass={$credentials['pw']}";
    }

    // Set userName.
    $this->userName = $this->currentUser->getDisplayName();
  }

  /**
   * Creates a DPQuery object.
   *
   * @param string $type
   *   The type of query object to create. Can be any of $validTypes or
   *   $predefinedProcs defined above.
   * @param mixed $optional
   *   Optional additional information for the query object, depending on
   *   the $type of query object being created.
   * @param array $args
   *   An array of query placeholder arguments.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Returns a new DPQuery object.
   */
  public function create(string $type = '', mixed $optional = NULL, array $args = []) {
    $query = new static($this->dpUtility, $this->httpClient, $this->configFactory, $this->currentUser);

    if (in_array($type, $query->validTypes)) {
      $query->type = $type;
      $query->isProc = in_array($type, $query->predefinedProcs);
      if ($query->isProc) {
        $query->procFields = $query->getProcFields();
      }
    }
    else {
      $query->type = '';
    }

    // For SELECT, INSERT, and UPDATE queries, $optional can be either
    // the 'base_table' or 'base_table.base_field'.
    if ((in_array($query->type, ['select', 'insert', 'update'])) && !empty($optional) && is_string($optional)) {
      $queryDot = strpos($optional, '.');
      if ($queryDot !== FALSE) {
        $queryTable = trim(substr($optional, 0, $queryDot));
        $queryField = trim(substr($optional, $queryDot + 1));
        if (array_key_exists($queryTable, $query->availFields) && array_key_exists($queryField, $query->availFields[$queryTable])) {
          $query->baseTable = $queryTable;
          $query->baseField = $queryField;
        }
      }
      else {
        if (in_array($optional, $query->availTables)) {
          $query->baseTable = $optional;
          $query->baseField = $query->getBaseField($optional);
        }
      }
      if (($query->type === 'select') && !empty($query->baseTable) && !empty($query->baseField)) {
        $query->addField("{$query->baseTable}.{$query->baseField}");
      }
    }

    // For passthrough queries, $optional is the SQL statement to be executed.
    if (($query->type === 'pass') && !empty($optional) && is_string($optional)) {
      $query->sql = $query->expandArguments($optional, $args);
    }

    if (($query->type === 'retrieveAllTableData') && $optional) {
      // We use this procedure to retrieve all the data from a specified table.
      $query->baseTable = $optional;
    }

    return $query;
  }

  /**
   * The addField method.
   *
   * For SELECT queries, this function adds a field to the list of fields to
   * retrieve. For UPDATE queries, adds a field and a value to be updated by
   * the query.
   *
   * @param string $field
   *   The name of the field to add to the query.
   * @param mixed $value
   *   For stored procedures and update queries, the value the field should
   *   be set to.
   * @param bool $add_quotes
   *   Boolean indicating whether or not the field value should be wrapped
   *   in quotes.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function addField(string $field, mixed $value = NULL, bool $add_quotes = FALSE) {
    if (!empty($field) && is_string($field)) {
      $field = strtolower($field);
    }
    if ($this->isProc) {
      // If $field is one of the procedure parameters defined by DonorPerfect,
      // set $add_quotes based on DP's definition.
      if (array_key_exists($field, $this->procFields)) {
        // If the proc_field is a date or character, then add quotes.
        if (in_array($this->procFields[$field], ['d', 'c'])) {
          $add_quotes = TRUE;
        }
        else {
          $add_quotes = FALSE;
        }
      }
      // Add field to $this->params.
      $this->params[$field] = $add_quotes ? $this->addSingleQuotes($value) : $value;
    }
    elseif ($this->type == 'select') {
      if (!empty($field)) {
        $thisDot = strpos($field, '.');
        if ($thisDot !== FALSE) {
          $thisTable = trim(substr($field, 0, $thisDot));
          $thisField = trim(substr($field, $thisDot + 1));
        }
        else {
          $thisTable = $this->baseTable;
          $thisField = trim($field);
        }
        if (array_key_exists($thisTable, $this->availFields)) {
          if (($thisField === '*') || array_key_exists($thisField, $this->availFields[$thisTable])) {
            // Check if this is a multi-value field.
            $nested_array_search = [$thisTable, $thisField, 'multi'];
            if (empty(NestedArray::getValue($this->availFields, $nested_array_search))) {
              if ((array_key_exists($thisTable, $this->tablesFields) && is_array($this->tablesFields[$thisTable]) && !in_array($thisField, $this->tablesFields[$thisTable]) && !in_array('*', $this->tablesFields[$thisTable])) ||
                  !array_key_exists($thisTable, $this->tablesFields)) {
                if ($thisField === '*') {
                  // If requesting * for this table, remove all
                  // individually-listed fields.
                  $this->tablesFields[$thisTable] = ['*'];
                }
                else {
                  $this->tablesFields[$thisTable][] = $thisField;
                }
              }
            }
            else {
              // This is a multi-value field.
              if ($thisField === 'flag') {
                $thisTable = 'dpflags';
              }
              else {
                $thisTable = 'dpusermultivalues';
              }
              if ((array_key_exists($thisTable, $this->tablesFields) && is_array($this->tablesFields[$thisTable]) && !in_array($thisField, $this->tablesFields[$thisTable])) ||
                  !array_key_exists($thisTable, $this->tablesFields)) {
                $this->tablesFields[$thisTable][] = $thisField;
              }
            }
          }
          else {
            // Check if this is a field on the related UDF table.
            $udfTable = "{$thisTable}udf";
            if (array_key_exists($udfTable, $this->availFields)) {
              if (array_key_exists($thisField, $this->availFields[$udfTable])) {
                // Check if this is a multi-value field.
                $nested_array_search = [$udfTable, $thisField, 'multi'];
                if (empty(NestedArray::getValue($this->availFields, $nested_array_search))) {
                  if ((array_key_exists($udfTable, $this->tablesFields) && is_array($this->tablesFields[$udfTable]) && !in_array($thisField, $this->tablesFields[$udfTable])) ||
                      !array_key_exists($udfTable, $this->tablesFields)) {
                    $this->tablesFields[$udfTable][] = $thisField;
                  }
                }
                else {
                  // This is a multi-value field.
                  $udfTable = 'dpusermultivalues';
                  if ((array_key_exists($udfTable, $this->tablesFields) && is_array($this->tablesFields[$udfTable]) && !in_array($thisField, $this->tablesFields[$udfTable])) ||
                      !array_key_exists($udfTable, $this->tablesFields)) {
                    $this->tablesFields[$udfTable][] = $thisField;
                  }
                }
              }
            }
          }
        }
      }
    }
    elseif (in_array($this->type, ['insert', 'update'])) {
      if ($field && !is_null($value)) {
        if (array_key_exists($this->baseTable, $this->availFields)) {
          if (array_key_exists($field, $this->availFields[$this->baseTable])) {
            $this->updateFields[$field] = $add_quotes ? $this->addSingleQuotes($value) : $value;
          }
        }
      }
    }
    return $this;
  }

  /**
   * The hasField method.
   *
   * Returns boolean indicating whether or not the specified field has been
   * added to the query.
   *
   * @param string $field
   *   The name of the field to check for.
   *
   * @return bool
   *   Boolean indicating whether or not the field is included in the query.
   */
  public function hasField(string $field) {
    $output = FALSE;
    if (!empty($field) && is_string($field)) {
      $field = strtolower($field);
      if ($this->isProc) {
        $output = array_key_exists($field, $this->params);
      }
      else {
        if (in_array($this->type, ['insert', 'update'])) {
          $output = array_key_exists($field, $this->updateFields);
        }
        elseif ($this->type === 'select') {
          $thisDot = strpos($field, '.');
          if ($thisDot !== FALSE) {
            $thisTable = trim(substr($field, 0, $thisDot));
            $thisField = trim(substr($field, $thisDot + 1));
          }
          else {
            $thisTable = $this->baseTable;
            $thisField = trim($field);
          }
          $exists = FALSE;
          NestedArray::getValue($this->tablesFields, [$thisTable, $thisField], $exists);
          $output = $exists;
        }
      }
    }
    return $output;
  }

  /**
   * Removes a field from the query.
   *
   * @param string $field
   *   Name of the field to remove from the query.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function removeField(string $field) {
    if (!empty($field) && is_string($field)) {
      $field = strtolower($field);
      if ($this->isProc) {
        if (array_key_exists($field, $this->params)) {
          unset($this->params[$field]);
        }
      }
      elseif ($this->type === 'select') {
        $thisDot = strpos($field, '.');
        if ($thisDot !== FALSE) {
          $thisTable = trim(substr($field, 0, $thisDot));
          $thisField = trim(substr($field, $thisDot + 1));
        }
        else {
          $thisTable = $this->baseTable;
          $thisField = trim($field);
        }
        if (!empty($this->tablesFields[$thisTable])) {
          $key = array_search($thisField, $this->tablesFields[$thisTable]);
          if ($key !== FALSE) {
            unset($this->tablesFields[$thisTable][$key]);
          }
        }
      }
      elseif (in_array($this->type, ['insert', 'update'])) {
        if (array_key_exists($field, $this->updateFields)) {
          unset($this->updateFields[$field]);
        }
      }
    }
    return $this;
  }

  /**
   * The addCondition method.
   *
   * For SELECT and UPDATE queries, this function adds a condition to the
   * list of conditions to narrow results.
   *
   * @param string $condition
   *   Condition to add to the query.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function addCondition(string $condition) {
    if (in_array($this->type, ['select', 'update'])) {
      if (!empty($condition) && is_string($condition)) {
        $this->conditions[] = $condition;
      }
    }
    return $this;
  }

  /**
   * Return boolean indicating if query has any conditions.
   *
   * @return bool
   *   Boolean indicating if query has conditions.
   */
  public function hasConditions() {
    return !empty($this->conditions);
  }

  /**
   * The addOrder method.
   *
   * For SELECT queries, this function adds a field to order the results by.
   * Returns the object to enable daisy-chaining.
   *
   * @param string $order
   *   Order string to add to the query.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function addOrder(string $order) {
    if ($this->type == 'select') {
      if (!empty($order) && is_string($order)) {
        $this->orderBy[] = $order;
      }
    }
    return $this;
  }

  /**
   * The addParam method.
   *
   * For predefined procedures, this function adds a parameter to the
   * query string.
   *
   * This is now just an alias for addField, and remains here to support
   * legacy code.
   *
   * @param string $field
   *   Name of the field to add to the query.
   * @param mixed $value
   *   For stored procedures and update queries, the value to set the field to.
   * @param bool $add_quotes
   *   Boolean indicating whether or not the value should be wrapped in quotes.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function addParam(string $field, mixed $value, bool $add_quotes = FALSE) {
    return $this->addField($field, $value, $add_quotes);
  }

  /**
   * Set the type of join between the tables in the query.
   *
   * @param string $type
   *   The type of join to be used in the query.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function setJoinType(string $type = 'inner') {
    switch ($type) {
      case 'left':
        $this->joinType = ' LEFT JOIN ';
        break;

      case 'inner':
      default:
        $this->joinType = ' INNER JOIN ';
        break;

    }
    return $this;
  }

  /**
   * Set $this->sql for passthrough queries.
   *
   * @param string $sql
   *   The complete SQL statement that should be executed.
   * @param array $args
   *   Array of query placeholder arguments.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function setSql(string $sql, array $args = []) {
    if ($this->type === 'pass') {
      $this->sql = $this->expandArguments($sql, $args);
    }
    return $this;
  }

  /**
   * Set the max time (in number of seconds) allowed to execute the api call.
   *
   * @param float $seconds
   *   The number of seconds to allow before timeout.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function setTimeout(float $seconds) {
    if (is_numeric($seconds)) {
      $this->httpRequestOptions['timeout'] = floatval($seconds);
    }
    return $this;
  }

  /**
   * The setHttpOption method.
   *
   * Set an option to be used by the HTTP client when making the call to the
   * DonorPerfect API. See \GuzzleHttp\RequestOptions for available options.
   *
   * @param string $key
   *   The name of the option to set.
   * @param mixed $value
   *   The value to be set for the option.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function setHttpOption(string $key, mixed $value) {
    $this->httpRequestOptions[$key] = $value;
    return $this;
  }

  /**
   * Set the max number of records to be returned by the query.
   *
   * @param int $new_limit
   *   The max number of records that should be returned.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function setLimit(int $new_limit) {
    $this->limit = $new_limit;
    return $this;
  }

  /**
   * Return the configured max number of records to be returned by the query.
   *
   * @return int|null
   *   Return the limit that is currently set.
   */
  public function getLimit() {
    return $this->limit;
  }

  /**
   * Set the offset for paginated query results.
   *
   * @param int $new_offset
   *   The new offset for paginated results.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function setOffset(int $new_offset) {
    $this->offset = $new_offset;
    return $this;
  }

  /**
   * Return the configured offset for paginated query results.
   *
   * @return int
   *   The currently set offset.
   */
  public function getOffset() {
    return $this->offset;
  }

  /**
   * Shortcut function to set both offset and limit in a single call.
   *
   * @param int $offset
   *   The offset that should be set on the query.
   * @param int $limit
   *   The max number of records that should be returned.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function queryRange($offset, $limit) {
    $this->offset = $offset;
    $this->limit = $limit;
    return $this;
  }

  /**
   * Return array of all fields available to be queried.
   *
   * The returned array is keyed by table name.
   *
   * @return array
   *   The array of available fields, keyed by table name.
   */
  public function listFields() {
    return $this->availFields;
  }

  /**
   * Return the compiled ORDER BY clause.
   *
   * @return string
   *   The ORDER BY string for the query.
   */
  public function getOrder() {
    return $this->orderByString;
  }

  /**
   * Compile query tables and fields into strings.
   *
   * @return array
   *   An array containing two elements: varFrom and varFields.
   */
  public function compileTablesFields() {
    $varFrom = $varFields = '';
    if (count($this->tablesFields) > 0) {
      $x = 0;
      $joinedTables = [];
      $baseTable = $baseTableMain = '';
      foreach ($this->tablesFields as $table => $fields) {
        // Reset $baseTable in case it had to be changed in a previous
        // iteration.
        $baseTable = $baseTableMain;
        if (in_array($table, $this->availTables)) {
          // Prepare FROM portion of select statement.
          if ($x == 0) {
            $varFrom = $table;
            $baseTable = $baseTableMain = $table;
            $joinedTables[] = $table;
          }
          else {
            $joinHandled = FALSE;
            switch ($table) {
              case 'dpaddressmailings':
                if (!in_array('dpaddressmailings', $joinedTables)) {
                  $temp = "{$this->joinType}dpaddressmailings ON dpaddress.address_id=dpaddressmailings.address_id";
                  if (in_array('dpaddress', $joinedTables)) {
                    if ($x > 1) {
                      $varFrom = "({$varFrom})";
                    }
                    $varFrom = "({$varFrom}){$temp}";
                    $joinedTables[] = 'dpaddressmailings';
                  }
                  else {
                    if ($x > 1) {
                      $varFrom = "({$varFrom})";
                    }
                    $varFrom = "({$varFrom}{$this->joinType}dpaddress ON {$baseTable}.donor_id=dpaddress.donor_id){$temp}";
                    $joinedTables[] = 'dpaddress';
                    $joinedTables[] = 'dpaddressmailings';
                  }
                }
                $joinHandled = TRUE;
                break;

              case 'dpusermultivalues':
                if (!in_array('dpusermultivalues', $joinedTables)) {
                  if (count($fields) === 1) {
                    // @todo Make it possible to handle more than one
                    // multi-value field in a single query.
                    $joinField = '';
                    switch ($baseTable) {
                      case 'dpgift':
                        $joinField = 'dpgift.gift_id';
                        break;

                      case 'dpcontact':
                        $joinField = 'dpcontact.contact_id';
                        break;

                      case 'dpotherinfo':
                        $joinField = 'dpotherinfo.other_id';
                        break;

                      case 'dp':
                        $joinField = 'dp.donor_id';
                        break;

                    }
                    if (!empty($joinField)) {
                      $field_name = strtoupper((string) reset($fields));
                      $temp = "{$this->joinType}dpusermultivalues ON {$joinField}=dpusermultivalues.matching_id AND dpusermultivalues.field_name='{$field_name}'";
                      if ($x > 1) {
                        $varFrom = "({$varFrom})";
                      }
                      $varFrom = "{$varFrom}$temp";
                      $joinedTables[] = 'dpusermultivalues';
                    }
                    else {
                      // Don't add any fields from this table.
                      $fields = [];
                    }
                  }
                  else {
                    // Don't add any fields from this table.
                    $fields = [];
                  }
                }
                $joinHandled = TRUE;
                break;

              case 'dpflags':
                if (!in_array('dpflags', $joinedTables)) {
                  $joinField = '';
                  switch ($baseTable) {
                    case 'dp':
                    case 'dpgift':
                    case 'dpcontact':
                    case 'dpotherinfo':
                      $joinField = "{$baseTable}.donor_id";
                      break;

                  }
                  if (!empty($joinField)) {
                    $temp = "{$this->joinType}dpflags ON {$joinField}=dpflags.donor_id";
                    if ($x > 1) {
                      $varFrom = "({$varFrom})";
                    }
                    $varFrom = "{$varFrom}$temp";
                    $joinedTables[] = 'dpflags';
                  }
                  else {
                    // Don't add any fields from this table.
                    $fields = [];
                  }
                }
                $joinHandled = TRUE;
                break;

            }
            if (!$joinHandled && !in_array($table, $joinedTables)) {
              // If this is a UDF table, make sure the parent table is
              // joined first.
              $matches = [];
              if (preg_match('/^(.+)udf$/', $table, $matches)) {
                if (!in_array($matches[1], $joinedTables)) {
                  if ($x > 1) {
                    $varFrom = "({$varFrom})";
                  }
                  $varFrom = "{$varFrom}{$this->joinType}{$matches[1]} ON {$baseTable}.donor_id={$matches[1]}.donor_id";
                  // Record that the parent table has been joined.
                  $joinedTables[] = $matches[1];
                  // Increment $x because another table was just added.
                  $x++;
                }
                // Temporarily set $baseTable to the parent table in order to
                // join to the parent table rather than $baseTableMain.
                $baseTable = $matches[1];
              }
              if ($x > 1) {
                $varFrom = "({$varFrom})";
              }
              $joinField = 'donor_id';
              switch ($baseTable) {
                case 'dpgift':
                  if ($table === 'dpgiftudf') {
                    $joinField = 'gift_id';
                  }
                  break;

                case 'dpcontact':
                  if ($table === 'dpcontactudf') {
                    $joinField = 'contact_id';
                  }
                  break;

                case 'dpotherinfo':
                  if ($table === 'dpotherinfoudf') {
                    $joinField = 'other_id';
                  }
                  break;

              }
              $varFrom = "{$varFrom}{$this->joinType}{$table} ON {$baseTable}.{$joinField}={$table}.{$joinField}";
              $joinedTables[] = $table;
              $joinHandled = TRUE;
            }
          }
          $x++;
          // Prepare FIELDS portion of select statement.
          foreach ($fields as $field) {
            if ($table == 'dpaddress') {
              // For the dpaddress table, add field aliases so that the values
              // don't overwrite values from the dp table with same field
              // names.
              $varFields .= "{$table}.{$field} sub_{$field},";
            }
            elseif ($table == 'dpusermultivalues') {
              if (strpos($varFields, $table) === FALSE) {
                $varFields .= "{$table}.matching_id, {$table}.code,";
              }
            }
            elseif ($table == 'dpflags') {
              if (strpos($varFields, $table) === FALSE) {
                $varFields .= "{$table}.donor_id flag_donor_id, {$table}.flag,";
              }
            }
            else {
              $varFields .= "{$table}.{$field},";
            }
          }
        }
      }
      // Remove the final comma from varFields.
      if (!empty($varFields)) {
        $varFields = substr($varFields, 0, -1);
      }
    }
    return [$varFrom, $varFields];
  }

  /**
   * Compile the WHERE clause for the SQL statement.
   *
   * @return string
   *   The compiled WHERE clause for the query.
   */
  public function compileWhere() {
    $varWhere = '';
    $x = 0;
    foreach ($this->conditions as $condition) {
      if ($x === 0) {
        $varWhere = "({$condition})";
      }
      else {
        $varWhere .= " AND ({$condition})";
      }
      $x++;
    }
    return $varWhere;
  }

  /**
   * Route query execution to the appropriate function.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   Return $this to enable chaining.
   */
  public function execute() {
    $thisFunc = "execute{$this->type}";
    if (method_exists($this, $thisFunc)) {
      $this->$thisFunc();
    }
    return $this;
  }

  /**
   * Executes a SQL SELECT statement.
   */
  protected function executeSelect() {
    // Compile tables and fields.
    [$varFrom, $varFields] = $this->compileTablesFields();
    if (!empty($varFrom) && !empty($varFields)) {
      // If this is a count query, replace the FIELDS portion of the
      // select statement.
      if ($this->getCount) {
        if (!empty($this->baseTable) && !empty($this->baseField)) {
          $varFields = "COUNT({$this->baseTable}.{$this->baseField}) AS count";
        }
        else {
          foreach ($this->tablesFields as $table => $fields) {
            $varFields = "COUNT({$table}.{$fields[0]}) AS count";
            break;
          }
        }
      }
      // Prepare WHERE portion of select statement.
      $varWhere = $this->compileWhere();
      // Prepare ORDER BY portion of select statement.
      $varOrder = '';
      if (!empty($this->orderBy) && is_array($this->orderBy)) {
        $varOrder = implode(',', $this->orderBy);
      }
      $this->orderByString = $varOrder;
      // Prepare final select statement.
      if ($this->getCount) {
        $this->sql = "SELECT {$varFields} FROM {$varFrom}";
        if (!empty($varWhere)) {
          $this->sql .= " WHERE {$varWhere}";
        }
        // Execute API call.
        $this->executeSelectApiCall();
      }
      elseif (is_null($this->offset) || is_null($this->limit)) {
        // Get results in groups of 500 because of DP API's limit of returning
        // a max of 500 records.
        if (empty($varOrder) && !empty($this->baseTable) && !empty($this->baseField)) {
          $varOrder = "{$this->baseTable}.{$this->baseField} ASC";
        }
        $x = -499;
        $y = 0;
        $result = [];
        $loop = TRUE;
        while ($loop) {
          $x += 500;
          $y += 500;
          $this->sql = "WITH mytable AS (SELECT {$varFields},ROW_NUMBER() OVER (ORDER BY {$varOrder}) AS RowNumber FROM {$varFrom} WHERE {$varWhere}) SELECT * FROM myTable WHERE (RowNumber BETWEEN {$x} AND {$y})";
          $this->executeSelectApiCall();
          $result = array_merge($result, $this->result);
          if (count($this->result) < 500) {
            $this->result = $result;
            $loop = FALSE;
          }
        }
      }
      else {
        // Use offset and limit to get specific records.
        $x = $this->offset + 1;
        $y = $this->offset + $this->limit;
        if (empty($varOrder) && $this->baseTable && $this->baseField) {
          $varOrder = $this->baseTable . '.' . $this->baseField . ' ASC';
        }
        $varWhere = !empty($varWhere) ? ' WHERE ' . $varWhere : '';
        $this->sql = "WITH mytable AS (SELECT {$varFields},ROW_NUMBER() OVER (ORDER BY {$varOrder}) AS RowNumber FROM {$varFrom}{$varWhere}) SELECT * FROM myTable WHERE (RowNumber BETWEEN {$x} AND {$y})";
        // Execute API call.
        $this->executeSelectApiCall();
      }
    }
  }

  /**
   * The executeSelectApiCall method.
   *
   * This intermediary function for executeApiCall caches the results of each
   * SELECT query and, if the same SELECT query is passed again, returns the
   * previous results without pinging the DP API again.
   */
  protected function executeSelectApiCall() {
    $previous_select_calls = &drupal_static('dpquery_select_calls');
    if (!is_array($previous_select_calls)) {
      $previous_select_calls = [];
    }
    if (array_key_exists($this->sql, $previous_select_calls)) {
      $this->result = $previous_select_calls[$this->sql]['result'];
      $this->resultCode = $previous_select_calls[$this->sql]['resultCode'];
    }
    else {
      $this->executeApiCall();
      $previous_select_calls[$this->sql] = [
        'result' => $this->result,
        'resultCode' => $this->resultCode,
      ];
    }
  }

  /**
   * Executes a SQL INSERT statement.
   */
  protected function executeInsert() {
    if (($this->baseTable) && (count($this->updateFields) > 0)) {
      // If not already included, add the currently logged-in website user
      // and date as modified_by and modified_date.
      if (!array_key_exists('created_by', $this->updateFields) || !array_key_exists('created_date', $this->updateFields)) {
        $thisUser = 'web:' . substr($this->userName, 0, 16);
        $thisDate = date('n/j/Y');
        $this->addField('created_by', $thisUser, TRUE);
        $this->addField('created_date', $thisDate, TRUE);
      }
      // Prepare the SET portion of the update statement.
      $varFields = $varValues = '';
      foreach ($this->updateFields as $field => $value) {
        $varFields .= $field . ',';
        $varValues .= $value . ',';
      }
      // Remove the final comma from $varFields and $varValues.
      $varFields = substr($varFields, 0, -1);
      $varValues = substr($varValues, 0, -1);
      // Prepare the final insert statement.
      $this->sql = "INSERT INTO {$this->baseTable} ({$varFields}) VALUES ({$varValues})";
      // Execute API call.
      $this->executeApiCall();
    }
  }

  /**
   * Executes a SQL UPDATE statement.
   */
  protected function executeUpdate() {
    if (($this->baseTable) && (count($this->updateFields) > 0) && (count($this->conditions) > 0)) {
      // If not already included, add the currently logged-in website user
      // and date as modified_by and modified_date.
      if (!array_key_exists('modified_by', $this->updateFields) || !array_key_exists('modified_date', $this->updateFields)) {
        $thisUser = 'web:' . substr($this->userName, 0, 16);
        $thisDate = date('n/j/Y');
        $this->addField('modified_by', $thisUser, TRUE);
        $this->addField('modified_date', $thisDate, TRUE);
      }
      // Prepare the SET portion of the update statement.
      $varSet = '';
      foreach ($this->updateFields as $field => $value) {
        $varSet .= "{$field}={$value},";
      }
      // Remove the final comma from $varSet.
      $varSet = substr($varSet, 0, -1);
      // Prepare WHERE portion of update statement.
      $varWhere = $this->compileWhere();
      // Prepare the final update statement.
      if (!empty($varSet) && !empty($varWhere)) {
        $this->sql = "UPDATE {$this->baseTable} SET {$varSet} WHERE {$varWhere}";
        // Execute API call.
        $this->executeApiCall();
      }
    }
  }

  /**
   * Executes the SQL statement that was passed into the query object.
   */
  protected function executePass() {
    $this->executeApiCall();
  }

  /**
   * Performs the actual call to the DonorPerfect API.
   */
  protected function executeApiCall() {
    // Make sure we have an $authString.
    if (empty($this->authString)) {
      $this->resultCode = 403;
      $this->result = [];
      return;
    }
    // Prepare API call.
    $http_method = 'GET';
    if ($this->isProc) {
      $this->paramsString = $this->filterString($this->paramsString);
      $this->apiCall = self::DONORPERFECT_API_URL . "?action={$this->dpAction}&params={$this->paramsString}&{$this->authString}";
    }
    elseif (!empty($this->sql)) {
      $this->sql = $this->filterString($this->sql);
      if (strlen($this->sql) <= 255) {
        $this->apiCall = self::DONORPERFECT_API_URL . "?action={$this->sql}&{$this->authString}";
      }
      else {
        // Use POST rather than GET request, as SQL statements can become
        // too long to fit in the url.
        $http_method = 'POST';
        $this->apiCall = self::DONORPERFECT_API_URL . '?' . $this->authString;
        $this->setHttpOption('form_params', ['action' => $this->sql]);
      }
    }
    else {
      $this->apiCall = '';
    }

    // Replace spaces in $this->apiCall with %20.
    $this->apiCall = str_replace(' ', '%20', $this->apiCall);

    // Execute API call.
    if (!empty($this->apiCall)) {
      $dpoReturn = $this->httpClient->request($http_method, $this->apiCall, $this->httpRequestOptions);
      $this->resultCode = (int) $dpoReturn->getStatusCode();
    }

    // Clear out the result array.
    $this->result = [];

    // Parse results.
    if ($this->resultCode === 200) {
      $result = $dpoReturn->getBody()->getContents();
      $thisResult = new \SimpleXMLElement($result);

      foreach ($thisResult->record as $rec) {
        $thisRow = [];
        foreach ($rec->field as $thisField) {
          $fieldName = (string) $thisField['name'];
          $thisRow[$fieldName] = (string) $thisField['value'];
        }
        $this->result[] = $thisRow;
      }

      // Reconfigure the result array if we used one of the DonorPerfect
      // "save" predefined procedures.
      if ($this->dpAction === 'dp_savedonor') {
        $temp = $this->result;
        $this->result = [];
        $this->result['donor_id'] = $temp[0][''];
      }
      elseif ($this->dpAction === 'dp_savegift') {
        $temp = $this->result;
        $this->result = [];
        $this->result['gift_id'] = $temp[0][''];
      }
      elseif ($this->dpAction === 'dp_savecontact') {
        $temp = $this->result;
        $this->result = [];
        $this->result['contact_id'] = $temp[0][''];
      }
      elseif ($this->dpAction === 'dp_saveotherinfo') {
        $temp = $this->result;
        $this->result = [];
        $this->result['other_id'] = $temp[0][''];
      }
    }
    else {
      // Problem getting data from DonorPerfect API.
      // Throw an exception?
    }
  }

  /**
   * The executePredefinedProc method.
   *
   * Intermediary function for predefined procedures to ensure that all
   * parameters are properly populated before being sent to the
   * DonorPerfect API.
   */
  protected function executePredefinedProc() {
    $thisParams = '';
    // Add the currently logged-in website user as the user_id so that it
    // will be recorded in DP.
    $thisUser = 'web:' . substr($this->userName, 0, 16);
    $this->addField('user_id', $thisUser, TRUE);

    foreach ($this->procFields as $thisField => $thisType) {
      if (array_key_exists($thisField, $this->params)) {
        $thisParams .= $this->params[$thisField] . ',';
      }
      else {
        // Set up default values for fields that don't allow null.
        if (($this->type === 'savedonor') && ($thisField === 'donor_id')) {
          $thisParams .= '0,';
        }
        elseif (($this->type === 'savedonor') && ($thisField === 'nomail')) {
          $thisParams .= "'N',";
        }
        elseif (($this->type === 'savegift') && ($thisField === 'gift_id')) {
          $thisParams .= '0,';
        }
        elseif (($this->type === 'savegift') && ($thisField === 'amount')) {
          $thisParams .= '0,';
        }
        elseif (($this->type === 'savegift') && ($thisField === 'batch_no')) {
          $thisParams .= '0,';
        }
        elseif (($this->type === 'savegift') && ($thisField === 'record_type')) {
          $thisParams .= "'G',";
        }
        elseif (($this->type === 'savegift') && ($thisField === 'split_gift')) {
          $thisParams .= "'N',";
        }
        elseif (($this->type === 'savegift') && ($thisField === 'pledge_payment')) {
          $thisParams .= "'N',";
        }
        elseif (($this->type === 'savegift') && ($thisField === 'nocalc')) {
          $thisParams .= "'N',";
        }
        elseif (($this->type === 'savegift') && ($thisField === 'receipt')) {
          $thisParams .= "'N',";
        }
        elseif (($this->type === 'saveotherinfo') && ($thisField === 'other_id')) {
          $thisParams .= '0,';
        }
        elseif (($this->type === 'savecontact') && ($thisField === 'contact_id')) {
          $thisParams .= '0,';
        }
        else {
          $thisParams .= 'null,';
        }
      }
    }

    // Remove the final comma from $thisParams.
    $thisParams = substr($thisParams, 0, -1);

    $this->paramsString = $thisParams;
    $this->executeApiCall();
  }

  /**
   * Executes the DonorSearch predefined procedure.
   */
  protected function executeDonorSearch() {
    $this->dpAction = 'dp_donorsearch';
    $this->executePredefinedProc();
  }

  /**
   * Executes the SaveDonor predefined procedure.
   */
  protected function executeSaveDonor() {
    // First of all, determine whether we're creating a new donor or updating
    // an existing one.
    // If creating a new donor, then we'll use DP's predefined procedure.
    // If updating an existing donor, then we'll create custom update sql
    // statements (because the predefined proc stinks for updating).
    if (!array_key_exists('donor_id', $this->params) || ($this->params['donor_id'] == 0)) {
      // Ok, we're creating a new donor, so let's use the predefined procedure.
      $this->dpAction = 'dp_savedonor';
      $this->executePredefinedProc();

      // Now we have created the new record and have the new donor_id in
      // $this->result.
      // But maybe additional parameters were passed to us that were not saved
      // to the new donor's record by the predefined proc.
      // Therefore, we're going to strip $this->params of all fields saved by
      // the predefined proc and then add the new donor_id.
      // Then we will let the next part of this function update the new record
      // with any additional fields that might be in $this->params.
      foreach ($this->procFields as $field => $type) {
        if (array_key_exists($field, $this->params)) {
          unset($this->params[$field]);
        }
      }
      $this->params['donor_id'] = $this->result['donor_id'];
    }

    // Now we are going to update an existing record. We need to have a
    // donor_id and at least one more parameter.
    if (array_key_exists('donor_id', $this->params) && ($this->params['donor_id'] > 0) && (count($this->params) > 1)) {
      // Since we are working with a donor record, the two DonorPerfect
      // tables we are concerned about are dp and dpudf.
      // Create a DPQuery update object for each of these two tables.
      $thisDp = $this->create('update', 'dp');
      $thisDpudf = $this->create('update', 'dpudf');

      // Add the currently logged-in website user and date as modified_by
      // and modified_date.
      $thisUser = 'web:' . substr($this->userName, 0, 16);
      $thisDate = date('n/j/Y');
      $thisDp->addField('modified_by', $thisUser, TRUE);
      $thisDp->addField('modified_date', $thisDate, TRUE);

      // Now pass all the params to both objects.
      // Let the object figure out whether or not each parameter is actually
      // a field in its corresponding table.
      foreach ($this->params as $field => $value) {
        if ($field === 'donor_id') {
          // Donor_id is actually the condition rather than a field
          // to be updated.
          $thisDp->addCondition('donor_id=' . $value);
          $thisDpudf->addCondition('donor_id=' . $value);
        }
        else {
          $thisDp->addField($field, $value);
          $thisDpudf->addField($field, $value);
        }
      }

      // Execute the updates on both objects.
      $thisDp->execute();
      $thisDpudf->execute();
    }
  }

  /**
   * Executes the Gifts predefined procedure.
   */
  protected function executeGifts() {
    $this->dpAction = 'dp_gifts';
    $this->executePredefinedProc();
  }

  /**
   * Executes the SaveGift predefined procedure.
   */
  protected function executeSaveGift() {
    // First of all, determine whether we're creating a new gift or updating
    // an existing one.
    // If creating a new gift, then we'll use DP's predefined procedure.
    // If updating an existing gift, then we'll create custom update sql
    // statements (because the predefined proc stinks for updating).
    if (!array_key_exists('gift_id', $this->params) || ((int) $this->params['gift_id'] === 0)) {
      // Ok, we're creating a new gift, so let's use the predefined procedure.
      $this->dpAction = 'dp_savegift';
      $this->executePredefinedProc();

      // Now we have created the new record and have the new gift_id in
      // $this->result.
      // But maybe additional parameters were passed to us that were not
      // saved to the new gift's record by the predefined proc.
      // Therefore, we're going to strip $this->params of all fields saved
      // by the predefined proc and then add the new gift_id.
      // Then we will let the next part of this function update the new record
      // with any additional fields that might be in $this->params.
      foreach ($this->procFields as $field => $type) {
        if (array_key_exists($field, $this->params)) {
          unset($this->params[$field]);
        }
      }
      $this->params['gift_id'] = $this->result['gift_id'];
    }

    // Now we are going to update an existing record. We need to have a gift_id
    // and at least one more parameter.
    if (array_key_exists('gift_id', $this->params) && ($this->params['gift_id'] > 0) && (count($this->params) > 1)) {
      // Since we are working with a gift record, the two DonorPerfect tables
      // we are concerned about are dpgift and dpgiftudf.
      // Create a DPQuery update object for each of these two tables.
      $thisDpgift = $this->create('update', 'dpgift');
      $thisDpgiftudf = $this->create('update', 'dpgiftudf');

      // Add the currently logged-in website user and date as modified_by and
      // modified_date.
      $thisUser = 'web:' . substr($this->userName, 0, 16);
      $thisDate = date('n/j/Y');
      $thisDpgift->addField('modified_by', $thisUser, TRUE);
      $thisDpgift->addField('modified_date', $thisDate, TRUE);

      // Now pass all the params to both objects.
      // Let the object figure out whether or not each parameter is actually
      // a field in its corresponding table.
      foreach ($this->params as $field => $value) {
        if ($field === 'gift_id') {
          // Gift_id is actually the condition rather than a field
          // to be updated.
          $thisDpgift->addCondition('gift_id=' . $value);
          $thisDpgiftudf->addCondition('gift_id=' . $value);
        }
        else {
          $thisDpgift->addField($field, $value);
          $thisDpgiftudf->addField($field, $value);
        }
      }
      // Execute the updates on both objects.
      $thisDpgift->execute();
      $thisDpgiftudf->execute();
    }
  }

  /**
   * Executes the SaveOtherInfo predefined procedure.
   */
  protected function executeSaveOtherInfo() {
    // First of all, determine whether we're creating a new record or
    // updating an existing one.
    // If creating a new record, then we'll use DP's predefined procedure.
    // If updating an existing record, then we'll create custom update sql
    // statements (because the predefined proc stinks for updating).
    if (!array_key_exists('other_id', $this->params) || ($this->params['other_id'] == 0)) {
      // Ok, we're creating a new record, so let's use the predefined
      // procedure.
      $this->dpAction = 'dp_saveotherinfo';
      $this->executePredefinedProc();

      // Now we have created the new record and have the new other_id in
      // $this->result.
      // But maybe additional parameters were passed to us that were not
      // saved to the new record by the predefined proc.
      // Therefore, we're going to strip $this->params of all fields saved
      // by the predefined proc and then add the new other_id.
      // Then we will let the next part of this function update the new record
      // with any additional fields that might be in $this->params.
      foreach ($this->procFields as $field => $type) {
        if (array_key_exists($field, $this->params)) {
          unset($this->params[$field]);
        }
      }
      $this->params['other_id'] = $this->result['other_id'];
    }

    // Now we are going to update an existing record. We need to have a
    // other_id and at least one more parameter.
    if (array_key_exists('other_id', $this->params) && ($this->params['other_id'] > 0) && (count($this->params) > 1)) {
      // Since we are working with an otherinfo record, the two DonorPerfect
      // tables we are concerned about are dpotherinfo and dpotherinfoudf.
      // Create a DPQuery update object for each of these two tables.
      $thisDp = $this->create('update', 'dpotherinfo');
      $thisDpudf = $this->create('update', 'dpotherinfoudf');

      // Add the currently logged-in website user and date as modified_by and
      // modified_date.
      $thisUser = 'web:' . substr($this->userName, 0, 16);
      $thisDate = date('n/j/Y');
      $thisDp->addField('modified_by', $thisUser, TRUE);
      $thisDp->addField('modified_date', $thisDate, TRUE);

      // Now pass all the params to both objects.
      // Let the object figure out whether or not each parameter is actually a
      // field in its corresponding table.
      foreach ($this->params as $field => $value) {
        if ($field === 'other_id') {
          // Other_id is actually the condition rather than a field to be
          // updated.
          $thisDp->addCondition('other_id=' . $value);
          $thisDpudf->addCondition('other_id=' . $value);
        }
        else {
          $thisDp->addField($field, $value);
          $thisDpudf->addField($field, $value);
        }
      }

      // Execute the updates on both objects.
      $thisDp->execute();
      $thisDpudf->execute();
    }
  }

  /**
   * Executes the SaveContact predefined procedure.
   */
  protected function executeSaveContact() {
    // First of all, determine whether we're creating a new record or updating
    // an existing one.
    // If creating a new record, then we'll use DP's predefined procedure.
    // If updating an existing record, then we'll create custom update sql
    // statements (because the predefined proc stinks for updating).
    if (!array_key_exists('contact_id', $this->params) || ($this->params['contact_id'] == 0)) {
      // Ok, we're creating a new record, so let's use the predefined
      // procedure.
      $this->dpAction = 'dp_savecontact';
      $this->executePredefinedProc();

      // Now we have created the new record and have the new contact_id in
      // $this->result.
      // But maybe additional parameters were passed to us that were not saved
      // to the new record by the predefined proc.
      // Therefore, we're going to strip $this->params of all fields saved by
      // the predefined proc and then add the new contact_id.
      // Then we will let the next part of this function update the new record
      // with any additional fields that might be in $this->params.
      foreach ($this->procFields as $field => $type) {
        if (array_key_exists($field, $this->params)) {
          unset($this->params[$field]);
        }
      }
      $this->params['contact_id'] = $this->result['contact_id'];
    }

    // Now we are going to update an existing record. We need to have a
    // contact_id and at least one more parameter.
    if (array_key_exists('contact_id', $this->params) && ($this->params['contact_id'] > 0) && (count($this->params) > 1)) {
      // Since we are working with a conact record, the two DonorPerfect
      // tables we are concerned about are dpcontact and dpcontactudf.
      // Create a DPQuery update object for each of these two tables.
      $thisDp = $this->create('update', 'dpcontact');
      $thisDpudf = $this->create('update', 'dpcontactudf');

      // Add the currently logged-in website user and date as modified_by
      // and modified_date.
      $thisUser = 'web:' . substr($this->userName, 0, 16);
      $thisDate = date('n/j/Y');
      $thisDp->addField('modified_by', $thisUser, TRUE);
      $thisDp->addField('modified_date', $thisDate, TRUE);

      // Now pass all the params to both objects.
      // Let the object figure out whether or not each parameter is actually
      // a field in its corresponding table.
      foreach ($this->params as $field => $value) {
        if ($field === 'contact_id') {
          // Contact_id is actually the condition rather than a field
          // to be updated.
          $thisDp->addCondition('contact_id=' . $value);
          $thisDpudf->addCondition('contact_id=' . $value);
        }
        else {
          $thisDp->addField($field, $value);
          $thisDpudf->addField($field, $value);
        }
      }

      // Execute the updates on both objects.
      $thisDp->execute();
      $thisDpudf->execute();
    }
  }

  /**
   * Executes the DelFlags predefined procedure.
   */
  protected function executeDelFlags() {
    $this->dpAction = 'dp_delflags_xml';
    $this->executePredefinedProc();
  }

  /**
   * Executes the SaveFlag predefined procedure.
   */
  protected function executeSaveFlag() {
    $this->dpAction = 'dp_saveflag_xml';
    $this->executePredefinedProc();
  }

  /**
   * Executes the SaveUdf predefined procedure.
   */
  protected function executeSaveUdf() {
    $this->dpAction = 'dp_save_udf_xml';
    $this->executePredefinedProc();
  }

  /**
   * Get the fields for a specific predefined procedure.
   *
   * The fields for each predefined procedure are defined in
   * the DonorPerfect XML API documentation.
   *
   * @return array
   *   Key is the field name, and value is the data type.
   *   (n = numeric, d=date, and c=character)
   */
  protected function getProcFields() {
    $proc_fields = [];
    switch ($this->type) {
      case 'delflags':
        $proc_fields = [
          'donor_id' => 'n',
          'user_id' => 'c',
        ];
        break;

      case 'donorsearch':
        $proc_fields = [
          'donor_id' => 'n',
          'last_name' => 'c',
          'first_name' => 'c',
          'opt_line' => 'c',
          'address' => 'c',
          'city' => 'c',
          'state' => 'c',
          'zip' => 'c',
          'country' => 'c',
          'filter_id' => 'n',
          'user_id' => 'c',
        ];
        break;

      case 'gifts':
        $proc_fields = [
          'donor_id' => 'n',
        ];
        break;

      case 'savecontact':
        $proc_fields = [
          // Enter 0 to create a new record, or an existing contact_id to
          // update a record.
          'contact_id' => 'n',
          'donor_id' => 'n',
          'activity_code' => 'c',
          'mailing_code' => 'c',
          'by_whom' => 'c',
          'contact_date' => 'd',
          'due_date' => 'd',
          'due_time' => 'c',
          'completed_date' => 'd',
          'comment' => 'c',
          'document_path' => 'c',
          'user_id' => 'c',
        ];
        break;

      case 'savedonor':
        $proc_fields = [
          // Must be 0 to create a new record, or an existing donor_id
          // to update a record.
          'donor_id' => 'n',
          'first_name' => 'c',
          'last_name' => 'c',
          'middle_name' => 'c',
          'suffix' => 'c',
          'title' => 'c',
          'salutation' => 'c',
          'prof_title' => 'c',
          'opt_line' => 'c',
          'address' => 'c',
          'address2' => 'c',
          'city' => 'c',
          'state' => 'c',
          'zip' => 'c',
          'country' => 'c',
          'address_type' => 'c',
          'home_phone' => 'c',
          'business_phone' => 'c',
          'fax_phone' => 'c',
          'mobile_phone' => 'c',
          'email' => 'c',
          'org_rec' => 'c',
          'donor_type' => 'c',
          'nomail' => 'c',
          'nomail_reason' => 'c',
          'narrative' => 'c',
          'user_id' => 'c',
        ];
        break;

      case 'saveflag':
        $proc_fields = [
          'donor_id' => 'n',
          'flag' => 'c',
          'user_id' => 'c',
        ];
        break;

      case 'savegift':
        $proc_fields = [
          // Enter 0 to create new gift record, or an existing gift_id
          // to update a record.
          'gift_id' => 'n',
          'donor_id' => 'n',
          'record_type' => 'c',
          'gift_date' => 'd',
          'amount' => 'n',
          'gl_code' => 'c',
          'solicit_code' => 'c',
          'sub_solicit_code' => 'c',
          'gift_type' => 'c',
          'split_gift' => 'c',
          'pledge_payment' => 'c',
          'reference' => 'c',
          'memory_honor' => 'c',
          'gfname' => 'c',
          'glname' => 'c',
          'fmv' => 'n',
          'batch_no' => 'n',
          'gift_narrative' => 'c',
          'ty_letter_no' => 'c',
          'glink' => 'n',
          'plink' => 'n',
          'nocalc' => 'c',
          'receipt' => 'c',
          'old_amount' => 'n',
          'user_id' => 'c',
          'campaign' => 'c',
          'membership_type' => 'c',
          'membership_level' => 'c',
          'membership_enr_date' => 'd',
          'membership_exp_date' => 'd',
          'membership_link_id' => 'n',
          'address_id' => 'n',
          'rcpt_status' => 'c',
          'rcpt_type' => 'c',
          'membership_code' => 'c',
          'contact_id' => 'n',
          'receipt_delivery_g' => 'c',
        ];
        break;

      case 'saveotherinfo':
        $proc_fields = [
          // Enter 0 to create a new record, or an existing other_id
          // to update a record.
          'other_id' => 'n',
          'donor_id' => 'n',
          'other_date' => 'd',
          'comments' => 'c',
          'user_id' => 'c',
        ];
        break;

      case 'saveudf':
        $proc_fields = [
          'matching_id' => 'n',
          'field_name' => 'c',
          'data_type' => 'c',
          'char_value' => 'c',
          'date_value' => 'd',
          'number_value' => 'n',
          'user_id' => 'c',
        ];
        break;

    }
    return $proc_fields;
  }

  /**
   * The getBaseField method.
   *
   * Given a table name, return the field that should be base_field
   * for a query.
   *
   * @param string $table
   *   The table for which the base field should be returned.
   *
   * @return string
   *   The base field of the specified table.
   */
  protected function getBaseField(string $table) {
    $output = '';
    switch ($table) {
      case 'dp':
      case 'dpudf':
      case 'dpflags':
        $output = 'donor_id';
        break;

      case 'dpgift':
      case 'dpgiftudf':
        $output = 'gift_id';
        break;

      case 'dpaddress':
      case 'dpaddressmailings':
        $output = 'address_id';
        break;

      case 'dplink':
        $output = 'link_id';
        break;

      case 'dpotherinfo':
      case 'dpotherinfoudf':
        $output = 'other_id';
        break;

      case 'dpcontact':
      case 'dpcontactudf':
        $output = 'contact_id';
        break;

      case 'dpusermultivalues':
      case 'dpbatch':
        $output = 'id';
        break;

      case 'dpcodes':
        $output = 'code';
        break;

    }
    return $output;
  }

  /**
   * Retrieve all data from the query's baseTable.
   *
   * Executes a series of queries to retrieve all data from a specified
   * table in DonorPerfect.
   */
  protected function executeRetrieveAllTableData() {
    if ($this->baseTable) {
      $thisResult = [];
      // Get the count of the number of records in the table.
      $tempSql = "SELECT count(*) AS count FROM :table";
      $args = [':table' => $this->baseTable];
      $query = $this->create('pass', $tempSql, $args);
      $query->execute();
      $thisCount = $query->result[0]['count'];
      if (!($thisCount > 0)) {
        $this->resultCode = 404;
        $this->result = [];
        return;
      }

      // Get a column name to order by.
      $tempSql = "SELECT TOP 1 * FROM :table";
      $args = [':table' => $this->baseTable];
      $query = $this->create('pass', $tempSql, $args);
      $query->execute();
      if (count($query->result) === 1) {
        foreach ($query->result as $value) {
          $tempKeys = array_keys($value);
          $thisOrderBy = $tempKeys[0];
          break;
        }
      }
      else {
        $this->resultCode = 404;
        $this->result = [];
        return;
      }

      // Retrieve the records in groups of 400 because DonorPerfect
      // limits the number of records that we can retrieve at one time.
      $groupCount = ($thisCount % 400 == 0) ? $thisCount / 400 : intval($thisCount / 400) + 1;

      for ($x = 0; $x <= $groupCount - 1; $x++) {
        $y = ($x * 400) + 1;
        $z = 400 + ($x * 400);

        $tempSql = "WITH mytable AS (SELECT *,ROW_NUMBER() OVER (order by :order_by) AS RowNumber FROM :table) SELECT * FROM myTable WHERE (RowNumber BETWEEN :y AND :z)";
        $args = [
          ':order_by' => $thisOrderBy,
          ':table' => $this->basetable,
          ':y' => $y,
          ':z' => $z,
        ];
        $query = $this->create('pass', $tempSql, $args);
        $query->execute();

        $thisResult = array_merge($thisResult, $query->result);
      }

      $this->resultCode = 200;
      $this->result = $thisResult;
      return;
    }
    else {
      $this->resultCode = 404;
      $this->result = [];
      return;
    }
  }

  /**
   * Returns a single value from the result array.
   *
   * @return mixed
   *   The first result of the result set.
   */
  public function fetchField() {
    if (is_array($this->result) && isset($this->result[0])) {
      if (is_array($this->result[0])) {
        return current($this->result[0]);
      }
      else {
        return $this->result[0];
      }
    }
    else {
      return NULL;
    }
  }

  /**
   * Return $this->result as an array of objects as required by Drupal Views.
   *
   * @return array
   *   Array of result objects.
   */
  public function resultObjects() {
    $return = [];
    foreach ($this->result as $key => $value) {
      if (is_array($value)) {
        $temp = new stdClass();
        foreach ($value as $tempKey => $tempValue) {
          $temp->{$tempKey} = $tempValue;
        }
      }
      else {
        $temp = $value;
      }
      $return[$key] = $temp;
    }
    return $return;
  }

  /**
   * Add single quotes around a value.
   *
   * @param string $value
   *   The value around which to add single quotes.
   *
   * @return string
   *   The value wrapped in single quotes.
   */
  protected function addSingleQuotes(string $value) {
    // Escape any single quotes already in $value.
    $value = str_replace("'", "''", $value);
    return "'{$value}'";
  }

  /**
   * Insert arguments into query string.
   *
   * @param string $sql
   *   The SQL statement string containing placeholders.
   * @param array $args
   *   Array of arguments, keyed by placeholder.
   *
   * @return string
   *   The SQL statement with the query arguments inserted.
   */
  protected function expandArguments(string $sql, array $args = []) {
    if (!empty($args)) {
      foreach ($args as $key => $value) {
        $sql = preg_replace('#' . $key . '\b#', $value, $sql);
      }
    }
    return $sql;
  }

  /**
   * Remove characters that cause problems for the API.
   *
   * @param string $value
   *   The value in which to replace troublesome characters.
   *
   * @return string
   *   The value with troublesome characters removed.
   */
  protected function filterString(string $value) {
    $value = str_replace('#', 'Number ', $value);
    $value = str_replace('&', 'and', $value);
    return $value;
  }

  /**
   * The refreshCache method.
   *
   * This module stores a local cache containing information about
   * different aspects of DonorPerfect (field definitions, code definitions,
   * etc). This method refreshes that cache.
   */
  public function refreshCache() {
    $this->loadDpCodes();
    $this->loadDpMultivalues();
    $this->loadDpTables();
  }

  /**
   * The loadDpTables method.
   *
   * Part of the process to refreshCache(). Loads table and field definitions
   * from DonorPerfect to store in the local cache.
   */
  protected function loadDpTables() {
    $theseFields = $this->config('donorperfect.cache')->get('dptables') ?? [];
    $theseCodes = $this->config('donorperfect.cache')->get('dpcodes') ?? [];
    // Get a list of calculated fields so that we can determine which fields
    // are not editable.
    $theseCalculated = [];
    $query2 = $this->create('pass', 'SELECT field_to_update FROM dp_calculated_fields');
    $query2->execute();
    foreach ($query2->result as $thisRow) {
      $temp = strtolower($thisRow['field_to_update']);
      $theseCalculated[] = $temp;
    }
    // Get all field data from sd_field.
    $query2 = $this->create('retrieveAllTableData', 'sd_field');
    $query2->execute();
    $tempFields = $query2->result;
    // Get all table_names from sd_section.
    $query2 = $this->create('pass', 'SELECT section_id,table_name FROM sd_section');
    $query2->execute();
    $tempSections = [];
    foreach ($query2->result as $section) {
      $tempSections['s' . $section['section_id']] = $section['table_name'];
    }
    // Re-key field info by table_name and field_name.
    $theseFieldInfo = [];
    foreach ($tempFields as $tempField) {
      if (!empty($tempField['field_name']) && !empty($tempField['section_id']) && ($tempTable = $tempSections['s' . $tempField['section_id']])) {
        $tempTable = strtolower($tempTable);
        if ($tempTable === 'bio') {
          $tempTable = 'donor';
        }
        $theseFieldInfo[$tempTable][strtolower($tempField['field_name'])] = $tempField;
      }
    }
    // Get info for all fields in tables included in $this->availTables.
    foreach ($this->availTables as $thisTable) {
      // Determine the table_name from sd_section that corresponds
      // to this table.
      $sdTable = '';
      switch ($thisTable) {
        case 'dp':
        case 'dpudf':
          $sdTable = 'donor';
          break;

        case 'dpgift':
        case 'dpgiftudf':
          $sdTable = 'gift';
          break;

        case 'dpaddress':
          $sdTable = 'address';
          break;

        case 'dpaddressmailings':
          $sdTable = 'dpaddressmailings';
          break;

        case 'dplink':
          $sdTable = 'dplink';
          break;

        case 'dpotherinfo':
        case 'dpotherinfoudf':
          $sdTable = 'other';
          break;

        case 'dpcontact':
        case 'dpcontactudf':
          $sdTable = 'contact';
          break;

        case 'dpcodes':
          $sdTable = 'dpcodes';
          break;

        case 'dpflags':
          $sdTable = 'dpflags';
          break;

      }
      if (empty($sdTable)) {
        continue;
      }
      // Get field names for $thisTable.
      $args = [':table' => $thisTable];
      $query = this->create('pass', 'SELECT TOP 1 * FROM :table', $args);
      $query->execute();
      $tempFields = [];
      if ((count($query->result) === 1) && is_array($query->result[0])) {
        foreach ($query->result[0] as $key => $value) {
          $tempFields[] = strtolower($key);
        }
      }
      // Get info for each of the fields in $thisTable and add to final
      // $theseFields array.
      foreach ($tempFields as $thisField) {
        $nested_array_search = [$sdTable, $thisField];
        if ($thisFieldInfo = NestedArray::getValue($theseFieldInfo, $nested_array_search)) {
          $thisDataType = strtoupper($thisFieldInfo['data_type']);
          $thisSqlDataType = '';
          $thisFormType = $thisNumericScale = $thisMulti = FALSE;
          switch ($thisDataType) {
            case 'C':
              $thisQuote = TRUE;
              $thisSqlDataType = 'varchar';
              switch ($thisFieldInfo['display_type']) {
                case 'L':
                  // DonorPerfect code for textarea.
                  $thisEntityDataType = 'string_long';
                  $thisFormType = 'textarea';
                  break;

                case 'C':
                  // DonorPerect code for boolean checkbox.
                  $thisEntityDataType = 'boolean';
                  $thisFormType = 'checkbox';
                  break;

                default:
                  $thisEntityDataType = 'string';
                  $thisFormType = 'textfield';
                  break;

              }
              break;

            case 'D':
              $thisQuote = TRUE;
              $thisSqlDataType = 'datetime';
              $thisEntityDataType = 'datetime';
              break;

            case 'M':
              $thisQuote = FALSE;
              $thisSqlDataType = 'money';
              $thisEntityDataType = 'decimal';
              $thisNumericScale = 2;
              break;

            case 'N':
              $thisQuote = FALSE;
              $thisSqlDataType = 'int';
              $thisEntityDataType = 'integer';
              break;

            default:
              $thisQuote = TRUE;
              $thisSqlDataType = 'varchar';
              $thisEntityDataType = 'string';
              break;

          }
          // Check if this is a coded field.
          $thisCode = FALSE;
          $theseOptions = [];
          if (array_key_exists($thisField, $theseCodes)) {
            $thisCode = TRUE;
            $theseOptions = $theseCodes[$thisField];
            $thisEntityDataType = 'list_string';
          }
          // Check if this is a calculated field.
          $thisCalculated = FALSE;
          if (in_array($thisField, $theseCalculated)) {
            $thisCalculated = TRUE;
          }
          $theseFields[$thisTable][$thisField] = [
            'dp_table' => $thisTable,
            'dp_field' => $thisField,
            'prompt' => $thisFieldInfo['field_prompt'],
            'mysql_data_type' => $thisSqlDataType,
            'max_length' => $thisFieldInfo['field_length'],
            'entity_data_type' => $thisEntityDataType,
            'form_type' => $thisFormType,
            'quote' => $thisQuote,
            'code' => $thisCode,
            'options' => $theseOptions,
            'multi' => $thisMulti,
            'calculated' => $thisCalculated,
            'editable' => !$thisCalculated,
            'decimals' => $thisNumericScale,
          ];
        }
      }
    }
    // Add in the multivalues so that they appear as regular fields.
    $theseMulti = $this->config('donorperfect.cache')->get('dpmultivalues') ?? [];
    foreach ($theseMulti as $thisMulti) {
      $thisTable = $sdTable = '';
      switch ($thisMulti['screen']) {
        case 'main':
        case 'bio':
          $thisTable = 'dpudf';
          $sdTable = 'donor';
          break;

        case 'gift':
        case 'pledge':
          $thisTable = 'dpgiftudf';
          $sdTable = 'gift';
          break;

        case 'address':
          $thisTable = $sdTable = 'dpaddressmailings';
          break;

        case 'contact':
          $thisTable = 'dpcontactudf';
          $sdTable = 'contact';
          break;

        case 'other':
          $thisTable = 'dpotherudf';
          $sdTable = 'other';
          break;

      }
      $nested_array_search = [$sdTable, $thisMulti['field_name']];
      if (!empty($sdTable) &&
         ($thisFieldInfo = NestedArray::getValue($theseFieldInfo, $nested_array_search))) {
        $theseFields[$thisTable][$thisMulti['field_name']] = [
          'dp_table' => $thisTable,
          'dp_field' => $thisMulti['field_name'],
          'prompt' => $thisFieldInfo['field_prompt'],
          'mysql_data_type' => 'varchar',
          'max_length' => $thisFieldInfo['field_length'],
          'entity_data_type' => 'text',
          'form_type' => 'textfield',
          'quote' => TRUE,
          'code' => TRUE,
          'options' => !empty($theseCodes[$thisMulti['field_name']]) ? $theseCodes[$thisMulti['field_name']] : [],
          'multi' => TRUE,
          'calculated' => FALSE,
          'editable' => TRUE,
          'decimals' => FALSE,
        ];
      }
    }
    // Add the special-case flags field to the dp table.
    $theseFields['dp']['flag'] = [
      'dp_table' => 'dp',
      'dp_field' => 'flag',
      'prompt' => 'Flag',
      'mysql_data_type' => 'varchar',
      'max_length' => 30,
      'entity_data_type' => 'list_string',
      'form_type' => 'textfield',
      'quote' => TRUE,
      'code' => TRUE,
      'options' => !empty($theseCodes['flag']) ? $theseCodes['flag'] : [],
      'multi' => TRUE,
      'calculated' => FALSE,
      'editable' => TRUE,
      'decimals' => FALSE,
    ];
    $theseFields['dpflags']['donor_id'] = [
      'dp_table' => 'dpflags',
      'dp_field' => 'donor_id',
      'prompt' => 'Donor ID',
      'mysql_data_type' => 'int',
      'max_length' => 100,
      'entity_data_type' => 'integer',
      'form_type' => FALSE,
      'quote' => FALSE,
      'code' => FALSE,
      'options' => [],
      'multi' => FALSE,
      'calculated' => FALSE,
      'editable' => TRUE,
      'decimals' => FALSE,
    ];
    // Sort the table and field arrays alphabetically.
    ksort($theseFields);
    foreach ($theseFields as &$thisTable) {
      ksort($thisTable);
    }
    $this->configFactory
      ->getEditable('donorperfect.cache')
      ->set('dptables', $theseFields)
      ->save();
  }

  /**
   * The loadDpCodes method.
   *
   * Part of the refreshCache() process. Loads all codes as configured
   * in the 'Code Maintenance' page in DonorPerfect.
   */
  protected function loadDpCodes() {
    $theseCodes = [];
    $query = $this->create();

    $tempSql = "SELECT count(code) AS count FROM dpcodes WHERE NOT field_name='w4w_walker'";
    $query = $this->create('pass', $tempSql);
    $query->execute();
    $codeCount = $query->result[0]['count'];

    // Retrieve the codes in groups of 400 because DonorPerfect limits
    // the number of records that we can retrieve at one time.
    $groupCount = ($codeCount % 400 == 0) ? $codeCount / 400 : intval($codeCount / 400) + 1;

    for ($x = 0; $x <= $groupCount - 1; $x++) {
      $y = ($x * 400) + 1;
      $z = 400 + ($x * 400);

      $tempSql = "WITH mytable AS (SELECT *,ROW_NUMBER() OVER (order by field_name,description) AS RowNumber FROM dpcodes WHERE NOT field_name='w4w_walker') SELECT code,description,field_name,inactive FROM myTable WHERE (RowNumber BETWEEN {$y} AND {$z})";
      $query = $this->create('pass', $tempSql);
      $query->execute();

      foreach ($query->result as $thisRow) {
        $theseCodes[strtolower($thisRow['field_name'])][$thisRow['code']] = [
          'dp_field' => strtolower($thisRow['field_name']),
          'code' => $thisRow['code'],
          'description' => $thisRow['description'],
          'active' => ($thisRow['inactive'] == 'N') ? TRUE : FALSE,
        ];
      }
    }

    $this->configFactory
      ->getEditable('donorperfect.cache')
      ->set('dpcodes', $theseCodes)
      ->save();
  }

  /**
   * The loadDpMultivalues method.
   *
   * Part of the refreshCache() process. Loads information about multi-value
   * fields in DonorPerfect to store in the local cache.
   */
  protected function loadDpMultivalues() {
    $query = $this->create();
    $theseMulti = [];

    $tempSql = "SELECT table_name,field_name FROM dpuserfields WHERE display_type='M'";
    $query = $this->create('pass', $tempSql);
    $query->execute();

    foreach ($query->result as $thisRow) {
      $theseMulti[strtolower($thisRow['field_name'])] = [
        'screen' => strtolower($thisRow['table_name']),
        'field_name' => strtolower($thisRow['field_name']),
      ];
    }

    $this->configFactory
      ->getEditable('donorperfect.cache')
      ->set('dpmultivalues', $theseMulti)
      ->save();
  }

}
