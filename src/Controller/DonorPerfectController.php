<?php

namespace Drupal\donorperfect\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\donorperfect\DPQuery;
use Drupal\donorperfect\DPUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides page callbacks for the DonorPerfect module.
 */
class DonorPerfectController implements ContainerInjectionInterface {

  use MessengerTrait;
  use RedirectDestinationTrait;
  use StringTranslationTrait;

  /**
   * The DPQuery service.
   *
   * @var \Drupal\donorperfect\DPQuery
   */
  protected $dpQuery;

  /**
   * The DPUtility service.
   *
   * @var \Drupal\donorperfect\DPUtility
   */
  protected $dpUtility;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\donorperfect\Controller\DonorPerfectController object.
   *
   * @param \Drupal\donorperfect\DPQuery $dpquery
   *   The DPQuery service.
   * @param \Drupal\donorperfect\DPUtility $dputility
   *   The DPUtility service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    DPQuery $dpquery,
    DPUtility $dputility,
    RequestStack $request_stack,
    ModuleHandlerInterface $module_handler,
  ) {
    $this->dpQuery = $dpquery;
    $this->dpUtility = $dputility;
    $this->requestStack = $request_stack;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('donorperfect.dpquery'),
      $container->get('donorperfect.dputility'),
      $container->get('request_stack'),
      $container->get('module_handler'),
    );
  }

  /**
   * Page callback to refresh the DonorPerfect cache.
   *
   * @param mixed $ajax
   *   Indicates whether this was called by ajax.
   */
  public function refreshCache(mixed $ajax = FALSE) {
    $query = $this->dpQuery->create();
    $query->refreshCache();
    $this->messenger()->addStatus($this->t('DonorPerfect cache refreshed successfully.'));
    $redirect_url = Url::fromUserInput($this->getRedirectDestination()->get())->setAbsolute()->toString();
    if ($ajax === 'ajax') {
      $response = new AjaxResponse();
      $response->addCommand(new RedirectCommand($redirect_url));
      return $response;
    }
    else {
      return new RedirectResponse($redirect_url);
    }
  }

  /**
   * Page callback for the name form_element to search donors by name.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function searchDonors() {
    $response = new AjaxResponse();
    $request_values = ['element_token' => ''];
    $donors = [];
    $result_content = $this->t('<strong>No matches found.</strong>');
    if ($this->moduleHandler->moduleExists('donorperfect_donor')) {
      $request = $this->requestStack->getCurrentRequest();
      foreach (['element_token', 'donor_type', 'last_name', 'first_name'] as $request_key) {
        $request_values[$request_key] = $request->get($request_key);
      }
      if (!empty($request_values['last_name'])) {
        $search_conditions = [];
        $search_conditions[] = [
          'table' => 'dp',
          'field' => 'last_name',
          'value' => $request_values['last_name'],
          'wildcard' => 'after',
        ];
        if (!empty($request_values['donor_type']) && ($request_values['donor_type'] === 'IN')) {
          $search_conditions[] = [
            'table' => 'dp',
            'field' => 'donor_type',
            'value' => $request_values['donor_type'],
          ];
          if (!empty($request_values['first_name'])) {
            $search_conditions[] = [
              'table' => 'dp',
              'field' => 'first_name',
              'value' => $request_values['first_name'],
              'wildcard' => 'both',
            ];
          }
        }
        // Using \Drupal::service() here because this service is only being
        // loaded if the donorperfect_donor module exists. This service
        // cannot be added through the create() method like the other
        // dependencies for this class because doing so would cause an error
        // if the donorperfect_donor module is not enabled.
        $donor_controller = \Drupal::service('donorperfect_donor.entity_controller');
        $search_results = $donor_controller->search($search_conditions);
        if (!empty($search_results)) {
          $donors = $this->dpUtility->entityLoadMultiple('donorperfect_donor', $search_results);
        }
      }
    }
    if (!empty($donors)) {
      $result_content = [];
      $result_content[] = ['#markup' => $this->t('<h3>Possible Matches in DonorPerfect</h3>')];
      $caption = $this->formatPlural(count($donors), '1 possible match', '@count possible matches');
      $header = [
        ['data' => $this->t('Match'), 'class' => ['donorperfect-name-match']],
        ['data' => $this->t('Name')],
        ['data' => $this->t('Address')],
        ['data' => $this->t('City')],
        ['data' => $this->t('State')],
        ['data' => $this->t('Zip')],
        ['data' => $this->t('Email')],
      ];
      $rows = [];
      foreach ($donors as $donor) {
        $donor_data = [];
        $field_keys = [
          'donor_id',
          'donor_type',
          'first_name',
          'last_name',
          'address',
          'city',
          'state',
          'zip',
          'email',
          'mobile_phone',
          'home_phone',
          'business_phone',
          'fax_phone',
        ];
        foreach ($field_keys as $field_key) {
          $donor_data[$field_key] = $donor->getFieldValue($field_key);
        }
        $row = [];
        $url_options = [
          'attributes' => [
            'result-data' => Json::encode($donor_data),
          ],
        ];
        $link = Link::fromTextAndUrl($this->t('Match'), Url::fromRoute('<current>', [], $url_options));
        $row[] = ['data' => $link, 'class' => ['donorperfect-name-match']];
        $row[] = ['data' => trim($donor_data['first_name'] . ' ' . $donor_data['last_name'])];
        $row[] = ['data' => $donor_data['address']];
        $row[] = ['data' => $donor_data['city']];
        $row[] = ['data' => $donor_data['state']];
        $row[] = ['data' => $donor_data['zip']];
        $row[] = ['data' => $donor_data['email']];
        $rows[] = $row;
      }
      $attributes = ['class' => ['donorperfect-name-search-results-table']];
      $result_content[] = [
        '#theme' => 'table',
        '#caption' => $caption,
        '#attributes' => $attributes,
        '#header' => $header,
        '#rows' => $rows,
      ];
    }
    $response->addCommand(new HtmlCommand('div#donorperfect-name-search-wrapper-' . $request_values['element_token'] . ' > div.donorperfect-name-search-results', $result_content));
    $invoke_args = [
      'show' => 'results',
      'element_token' => $request_values['element_token'],
    ];
    $response->addCommand(new InvokeCommand(NULL, 'DonorPerfectFormElementNameSearchSwitchLoadingResults', [Json::encode($invoke_args)]));
    return $response;
  }

}
