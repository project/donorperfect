<?php

namespace Drupal\donorperfect;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides utility functions for the DonorPerfect module.
 */
class DPUtility {

  use StringTranslationTrait;

  /**
   * Max length of specific fields as set by DonorPerfect.
   */
  const FIRST_NAME_MAXLENGTH = 50;
  const LAST_NAME_MAXLENGTH = 75;
  const ADDRESS_MAXLENGTH = 100;
  const CITY_MAXLENGTH = 50;
  const ZIP_MAXLENGTH = 20;
  const EMAIL_MAXLENGTH = 75;
  const PHONE_MAXLENGTH = 40;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new \Drupal\donorperfect\DPUtility object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ModuleExtensionList $module_extension_list, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user) {
    $this->moduleHandler = $module_handler;
    $this->moduleExtensionList = $module_extension_list;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Load the API credentials needed to access the DonorPerfect XML API.
   *
   * @param string $context
   *   The context for which the credentials are being loaded.
   *   Can be either 'settings' or 'dpquery'.
   *
   * @return array
   *   Array containing the API credentials,
   *   with possible keys 'user', 'pw', and/or 'key'.
   */
  public function apiCredentialsLoad(string $context = 'settings') {
    $credentials = &drupal_static('donorperfect_api_credentials_load');
    if (!isset($credentials) || !isset($credentials[$context])) {
      $credentials[$context] = [];
      // Allow other modules to handle loading the api credentials.
      $this->moduleHandler->alter('donorperfect_api_credentials_load', $credentials[$context], $context);
      // If no other module has handled it, load credentials from
      // default storage.
      if (empty($credentials[$context])) {
        $config = $this->configFactory->get('donorperfect.settings');
        foreach (['key', 'user', 'pw'] as $credential_key) {
          $credentials[$context][$credential_key] = $config->get("api.{$credential_key}");
        }
      }
    }
    return $credentials[$context];
  }

  /**
   * Validate the API credentials that were entered on the Settings form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state from the settings form.
   */
  public function apiCredentialsValidate(FormStateInterface $form_state) {
    // Allow other modules to handle the validation.
    $validated = FALSE;
    $this->moduleHandler->alter('donorperfect_api_credentials_validate', $form_state, $validated);
    // If no other module has handled it, perform default validation.
    if (!$validated) {
      $values = $form_state->getValues();
      if (!empty($values['api']) && is_array($values['api'])) {
        $has_credentials = FALSE;
        // If key is provided, clear user and pw.
        if (isset($values['api']['key']) && !empty(trim($values['api']['key']))) {
          $has_credentials = TRUE;
          $values['api']['key'] = trim($values['api']['key']);
          unset($values['api']['user']);
          unset($values['api']['pw']);
        }
        elseif (isset($values['api']['user']) && !empty(trim($values['api']['user']))) {
          $has_credentials = TRUE;
          unset($values['api']['key']);
          $values['api']['user'] = trim($values['api']['user']);
          if (isset($values['api']['pw']) && !empty(trim($values['api']['pw']))) {
            $values['api']['pw'] = trim($values['api']['pw']);
          }
          else {
            $form_state->setErrorByName('api][pw', $this->t('Please enter the password that corresponds to the DonorPerfect username.'));
          }
        }
        if (!$has_credentials) {
          $form_state->setErrorByName('api][key', $this->t('Please enter credentials to connect to the DonorPerfect XML API.'));
        }
      }
      $form_state->setValues($values);
    }
  }

  /**
   * Save the API credentials that were entered on the Settings form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state from the settings form.
   */
  public function apiCredentialsSave(FormStateInterface $form_state) {
    // Allow other modules to handle saving the credentials.
    $saved = FALSE;
    $this->moduleHandler->alter('donorperfect_api_credentials_save', $form_state, $saved);
    // If no other module has handled it, save the credentials
    // in the default location.
    if (!$saved) {
      $config = $this->configFactory->getEditable('donorperfect.settings');
      $config->set('api', $form_state->getValue('api'));
      $config->save();
    }
  }

  /**
   * Load a single entity.
   *
   * @param string $entity_type_id
   *   The type of the entity to be loaded.
   * @param int $entity_id
   *   The id of the entity to be loaded.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The loaded entity.
   */
  public function entityLoad(string $entity_type_id, int $entity_id) {
    try {
      return $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->load($entity_id);
    }
    catch (\Throwable $e) {
      return NULL;
    }
  }

  /**
   * Load multiple entities.
   *
   * @param string $entity_type_id
   *   The type of the entities to be loaded.
   * @param array $entity_ids
   *   An array of entity IDs, or NULL to load all entities.
   * @param array $conditions
   *   An array of conditions the loaded entities should meet.
   * @param bool $access_check
   *   Whether or not access should be checked.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of loaded entities.
   */
  public function entityLoadMultiple(string $entity_type_id, array $entity_ids = NULL, array $conditions = [], bool $access_check = TRUE) {
    try {
      if (empty($entity_ids) && !empty($conditions) && is_array($conditions)) {
        $query = $this->entityQuery($entity_type_id)
          ->accessCheck($access_check);
        foreach ($conditions as $condition) {
          if (is_array($condition)) {
            if (count($condition) === 2) {
              [$field, $value] = $condition;
              $query->condition($field, $value);
            }
            elseif (count($condition) === 3) {
              [$field, $value, $operator] = $condition;
              $query->condition($field, $value, $operator);
            }
          }
        }
        $entity_ids = $query->execute();
      }
      return $this->entityTypeManager
        ->getStorage($entity_type_id)
        ->loadMultiple($entity_ids);
    }
    catch (\Throwable $e) {
      return [];
    }
  }

  /**
   * Returns the entity query object for this entity type.
   *
   * @param string $entity_type
   *   The entity type (for example, node) for which the query object should be
   *   returned.
   * @param string $conjunction
   *   (optional) Either 'AND' if all conditions in the query need to apply, or
   *   'OR' if any of them is sufficient. Defaults to 'AND'.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query object that can query the given entity type.
   */
  public function entityQuery($entity_type, $conjunction = 'AND') {
    return $this->entityTypeManager->getStorage($entity_type)->getQuery($conjunction);
  }

  /**
   * Get the specified key from the DonorPerfect cache.
   *
   * @param string $key
   *   The cache key to return.
   *
   * @return array
   *   Array of values loaded from cache.
   */
  public function getCache(string $key = '') {
    if (substr($key, 0, 2) !== 'dp') {
      $key = 'dp' . $key;
    }
    $cache = &drupal_static(__CLASS__ . '_getCache');
    if (!is_array($cache) || !isset($cache[$key])) {
      $config = $this->configFactory->get('donorperfect.cache')->get($key);
      if (!empty($config) && is_array($config)) {
        $cache[$key] = $config;
      }
      else {
        $cache[$key] = [];
      }
    }
    return $cache[$key];
  }

  /**
   * Return DonorPerfect codes as an options array.
   *
   * This is useful for #options of a select form element.
   *
   * @param string $field_name
   *   Name of the DonorPerfect field for which the codes should be returned.
   * @param bool $include_inactive
   *   Boolean indicating whether or not inactive codes should be included.
   * @param string $include_code
   *   A DonorPerfect code that should be included even if inactive and
   *   $include_inactive is FALSE.
   *
   * @return array
   *   Array with codes as keys and descriptions as values.
   */
  public function getCodesAsOptions(string $field_name, bool $include_inactive = FALSE, string $include_code = '') {
    $output = [];
    $dpcodes = $this->getCache('codes');
    if (array_key_exists($field_name, $dpcodes) && is_array($dpcodes[$field_name])) {
      foreach ($dpcodes[$field_name] as $code) {
        if (is_array($code) && !empty($code['code']) && !empty($code['description'])) {
          if ($include_inactive || !empty($code['active']) || (!empty($include_code) && ($include_code === $code['code']))) {
            $output[$code['code']] = $code['description'];
          }
        }
      }
      asort($output);
    }
    return $output;
  }

  /**
   * Return the description for the specified DonorPerfect code.
   *
   * @param string $field_name
   *   Name of the DonorPerfect field to which the code belongs.
   * @param string $code
   *   The code for which to retrieve the description.
   *
   * @return string
   *   The description that corresponds to the given code.
   */
  public function getCodeDescription(string $field_name, string $code) {
    $output = '';
    $codes = $this->getCodesAsOptions($field_name, TRUE);
    if (array_key_exists($code, $codes)) {
      $output = $codes[$code];
    }
    return $output;
  }

  /**
   * Return DonorPerfect fields as an options array.
   *
   * This is useful for #options of a select form element.
   *
   * @param string $table
   *   Name of the DonorPerfect table whose fields should be returned.
   * @param bool $prefix_table_name
   *   Boolean indicating whether or not the table name should be prefixed
   *   to the field name in the returned array keys.
   * @param bool $append_field_name
   *   Boolean indicating whether or not the field name should be appended
   *   in parentheses at the end of the returned array values.
   *
   * @return array
   *   Array of fields, with field name as key and labels as values.
   */
  public function getFieldsAsOptions(string $table, bool $prefix_table_name = FALSE, bool $append_field_name = TRUE) {
    $output = [];
    if (empty($table)) {
      return $output;
    }
    $dptables = $this->getCache('tables');
    if (array_key_exists($table, $dptables) && is_array($dptables[$table])) {
      foreach ($dptables[$table] as $field_name => $field_info) {
        $key = $prefix_table_name ? "{$table}.{$field_name}" : $field_name;
        if (is_array($field_info) && !empty($field_info['prompt'])) {
          $prompt = $field_info['prompt'];
          if ($append_field_name) {
            $prompt .= " ({$field_name})";
          }
        }
        else {
          $prompt = $field_name;
        }
        $output[$key] = $prompt;
      }
      asort($output);
    }
    return $output;
  }

  /**
   * Given an array containing address info, return address as a string.
   *
   * @param array $info
   *   Array of address info.
   * @param string $prefix
   *   Field prefix (if any).
   *
   * @return string
   *   The address string.
   */
  public function getAddressString(array $info, string $prefix = '') {
    $address = '';
    for ($x = 1; $x <= 4; $x++) {
      $address_key = $prefix . 'address';
      if ($x > 1) {
        $address_key .= $x;
      }
      if (!empty($info[$address_key])) {
        if (!empty($address)) {
          $address .= ', ';
        }
        $address .= $info[$address_key];
      }
    }
    $csz = $this->getCsz($info, $prefix);
    if (!empty($csz)) {
      if (!empty($address)) {
        $address .= ', ';
      }
      $address .= $csz;
    }
    return $address;
  }

  /**
   * Given an array containing address info, return a formatted csz string.
   *
   * @param array $info
   *   Array of address info.
   * @param string $prefix
   *   Field prefix (if any).
   *
   * @return string
   *   The city, state, zip string.
   */
  public function getCsz(array $info, string $prefix = '') {
    $csz = '';
    $city = $prefix . 'city';
    $state = $prefix . 'state';
    $zip = $prefix . 'zip';
    if (!empty($info[$city]) && !empty($info[$state]) && !empty($info[$zip])) {
      $csz = $info[$city] . ', ' . $info[$state] . ' ' . $info[$zip];
    }
    elseif (!empty($info[$city]) && !empty($info[$state])) {
      $csz = $info[$city] . ', ' . $info[$state];
    }
    elseif (!empty($info[$city]) && !empty($info[$zip])) {
      $csz = $info[$city] . ' ' . $info[$zip];
    }
    elseif (!empty($info[$state]) && !empty($info[$zip])) {
      $csz = $info[$state] . ' ' . $info[$zip];
    }
    elseif (!empty($info[$city])) {
      $csz = $info[$city];
    }
    elseif (!empty($info[$state])) {
      $csz = $info[$state];
    }
    elseif (!empty($info[$zip])) {
      $csz = $info[$zip];
    }
    return $csz;
  }

  /**
   * The getEntityFieldInfo method.
   *
   * Return array of DonorPerfect fields that have been selected
   * in the settings form to be included in the Drupal entities
   * of specified type.
   *
   * @param string $entity_type_id
   *   The entity type for which field info should be returned.
   * @param bool $prefix_table_name
   *   Boolean indicating whether or not the table name should be prefixed
   *   to the field name in the returned array keys.
   *
   * @return array
   *   Array of field info.
   */
  public function getEntityFieldInfo(string $entity_type_id, bool $prefix_table_name = FALSE) {
    $output = [];
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $base_field = $entity_type->getKey('id');
    $field_names = $this->configFactory->get('donorperfect.settings')->get("entity.{$entity_type_id}.fields");
    // Make sure at least the $base_field is included.
    if (empty($field_names) || !is_array($field_names) || !in_array($base_field, $field_names)) {
      if (!is_array($field_names)) {
        $field_names = [];
      }
      $field_names[$base_field] = $base_field;
    }
    $dptables = $this->getCache('tables');
    $base_table = $entity_type->getBaseTable();
    $base_fields = (array_key_exists($base_table, $dptables) && is_array($dptables[$base_table])) ? $dptables[$base_table] : [];
    $udf_table = $entity_type->get('udf_table');
    $udf_fields = (array_key_exists($udf_table, $dptables) && is_array($dptables[$udf_table])) ? $dptables[$udf_table] : [];
    foreach ($field_names as $field_name) {
      $key = '';
      if (array_key_exists($field_name, $base_fields) && is_array($base_fields[$field_name])) {
        $key = $prefix_table_name ? "{$base_table}.{$field_name}" : $field_name;
        $field_info = $base_fields[$field_name];
      }
      elseif (array_key_exists($field_name, $udf_fields) && is_array($udf_fields[$field_name])) {
        $key = $prefix_table_name ? "{$udf_table}.{$field_name}" : $field_name;
        $field_info = $udf_fields[$field_name];
      }
      elseif ($field_name === $base_field) {
        $key = $prefix_table_name ? "{$base_table}.{$field_name}" : $field_name;
        $field_info = [
          'dp_table' => $base_table,
          'dp_field' => $base_field,
          'mysql_data_type' => 'int',
          'entity_data_type' => 'integer',
          'quote' => FALSE,
          'code' => FALSE,
          'options' => [],
          'multi' => FALSE,
          'calculated' => FALSE,
          'editable' => TRUE,
          'decimals' => FALSE,
        ];
      }
      if (!empty($key)) {
        $output[$key] = $field_info;
      }
    }
    return $output;
  }

  /**
   * Get default values for DonorPerfect fields.
   *
   * @return array
   *   An array of default values, keyed by field name.
   */
  public function getFieldDefaults() {
    $defaults = &drupal_static(__METHOD__);
    if (!is_array($defaults)) {
      $defaults = [
        'donor_id' => 0,
        'donor_type' => 'IN',
        'org_rec' => 0,
        'first_name' => '',
        'last_name' => '',
        'opt_line' => '',
        'address' => '',
        'address2' => '',
        'address3' => '',
        'city' => '',
        'state' => '',
        'zip' => '',
        'country' => 'US',
        'email' => '',
        'salutation' => '',
        'nomail' => 0,
        'nomail_reason' => '',
        'no_email' => 0,
        'no_email_reason' => '',
        'narrative' => '',
        'receipt_delivery' => 'L',
        'mobile_phone' => '',
        'home_phone' => '',
        'business_phone' => '',
        'fax_phone' => '',
        'phone_type' => 'mobile',
      ];
      // Allow other modules to alter $defaults.
      $this->moduleHandler->alter('donorperfect_field_defaults', $defaults);
    }
    return $defaults;
  }

  /**
   * Get the default value for the specified DonorPerfect field.
   *
   * @param string $field
   *   The name of the field to return the default value for.
   *
   * @return mixed
   *   The default value for the specified field.
   */
  public function getFieldDefault(string $field) {
    $output = '';
    $defaults = $this->getFieldDefaults();
    if (array_key_exists($field, $defaults)) {
      $output = $defaults[$field];
    }
    return $output;
  }

  /**
   * Generate random string token of specified length.
   *
   * @param int $token_length_min
   *   Minimum length of token if max is also provided, otherwise the exact
   *   length of the token.
   * @param int $token_length_max
   *   Maximum length of token.
   *
   * @return string
   *   A randomly generated string.
   */
  public function generateToken(int $token_length_min = 0, int $token_length_max = 0) {
    $token = '';
    if (!empty($token_length_min)) {
      if (!empty($token_length_max)) {
        if ((int) $token_length_max > (int) $token_length_min) {
          $length = rand((int) $token_length_min, (int) $token_length_max);
        }
        else {
          $length = rand((int) $token_length_max, (int) $token_length_min);
        }
      }
      else {
        $length = (int) $token_length_min;
      }
    }
    else {
      $length = rand(5, 8);
    }
    for ($x = 1; $x <= $length; $x++) {
      $char_type = rand(1, 3);
      switch ($char_type) {
        case 1:
          $char = rand(48, 57);
          break;

        case 2:
          $char = rand(65, 90);
          break;

        case 3:
          $char = rand(97, 122);
          break;

      }
      $token = $token . chr($char);
    }
    return $token;
  }

  /**
   * Upper-case a single word.
   *
   * @param string $string
   *   The string that should be upper-cased.
   *
   * @return string
   *   The string with first letter upper-cased.
   */
  public function ucword(string $string) {
    $string = ucfirst(strtolower($string));
    if (substr($string, 0, 2) == 'Mc') {
      $string = substr($string, 0, 2) . ucfirst(substr($string, 2));
    }
    if (substr($string, 0, 2) == "O'") {
      $string = substr($string, 0, 2) . ucfirst(substr($string, 2));
    }
    // Abbreviations that should be capitalized.
    // SR = State Route, CR = County Road.
    $abbrevs = ['p.o.', 'po', 'nw', 'ne', 'sw', 'se', 'sr', 'cr', 'us'];
    if (in_array(strtolower($string), $abbrevs)) {
      $string = strtoupper($string);
    }
    // If word is just initials with dots between, capitalize all letters.
    if ((preg_match("/^[A-Za-z]\.$/", $string)) || (preg_match("/^[A-Za-z]\.[A-Za-z]\.$/", $string))) {
      $string = strtoupper($string);
    }
    // If word is a number followed by a single letter, capitalize that letter.
    if (preg_match("/^[0-9]{1,}[A-Za-z]$/", $string)) {
      $string = strtoupper($string);
    }
    return $string;
  }

  /**
   * Upper-case every word in a string.
   *
   * @param string $string
   *   The string whose words should be upper-cased.
   *
   * @return string
   *   The string with first letters upper-cased.
   */
  public function ucwords(string $string) {
    $return_spaces = TRUE;
    if (strpos($string, '_') !== FALSE) {
      $words = explode('_', $string);
      $return_spaces = FALSE;
    }
    else {
      $words = explode(' ', $string);
    }
    foreach ($words as $key => $word) {
      $words[$key] = $this->ucword(trim($word));
    }
    return $return_spaces ? implode(' ', $words) : implode('', $words);
  }

  /**
   * Returns the file path for the specified module.
   *
   * @param string $module_name
   *   The name of the module.
   *
   * @return string
   *   The file path of the specified module.
   */
  public function getModulePath(string $module_name) {
    if ($this->moduleHandler->moduleExists($module_name)) {
      return $this->moduleExtensionList->getPath($module_name);
    }
    else {
      return '';
    }
  }

  /**
   * Returns the current user.
   *
   * @return \Drupal\Core\Session\AccountProxyInterface
   *   The current user.
   */
  public function currentUser() {
    return $this->currentUser;
  }

}
