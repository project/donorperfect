<?php

namespace Drupal\donorperfect\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\donorperfect\FormElementBase;

/**
 * Provides a form element for DonorPerfect donor email.
 *
 * @FormElement("donorperfect_email")
 */
class Email extends FormElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#input' => TRUE,
      '#default_value' => NULL,
      '#process' => [
        [$class, 'processElement'],
      ],
      '#pre_render' => [],
      '#after_build' => [],
      '#element_validate' => [
        [$class, 'validateElement'],
      ],
      '#theme_wrappers' => [],
      // Max length of fields.
      '#email_maxlength' => static::dpUtility()::EMAIL_MAXLENGTH,
    ];
  }

  /**
   * Processes the form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    $default_value = !empty($element['#default_value']) && is_string($element['#default_value']) ? $element['#default_value'] : static::dpUtility()->getFieldDefault('email');
    $element_token = static::getElementToken($element, $form_state);
    $element['element_token'] = [
      '#type' => 'hidden',
      '#default_value' => $element_token,
      '#attributes' => ['class' => ['donorperfect-element-token']],
    ];
    $element['#prefix'] = '<div class="donorperfect-element-wrapper donorperfect-email-wrapper" id="donorperfect-element-wrapper-' . $element_token . '" element-token="' . $element_token . '">';
    $element['#suffix'] = '</div>';
    $element['email'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#title_display' => 'invisible',
      '#default_value' => $default_value,
      '#required' => $element['#required'] ?? FALSE,
      '#attributes' => [
        'class' => ['dpfield'],
        'dpfield' => 'email',
        'placeholder' => t('Email address'),
      ],
      '#maxlength' => $element['#email_maxlength'] ?? static::dpUtility()::EMAIL_MAXLENGTH,
      '#dp_validate' => ['email'],
    ];
    // Add libraries.
    $element['#attached']['library'][] = 'donorperfect/form_element';
    return $element;
  }

  /**
   * Validates the form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public static function validateElement(array $element, FormStateInterface $form_state, array $complete_form) {
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $output = (string) $element['#default_value'] ?? '';
    if (!empty($input) && is_string($input)) {
      $output = $input;
    }
    return $output;
  }

}
