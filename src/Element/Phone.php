<?php

namespace Drupal\donorperfect\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\donorperfect\FormElementBase;

/**
 * Provides a form element for DonorPerfect donor phone number.
 *
 * @FormElement("donorperfect_phone")
 */
class Phone extends FormElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#input' => TRUE,
      '#default_value' => NULL,
      '#process' => [
        [$class, 'processElement'],
      ],
      '#pre_render' => [],
      '#after_build' => [],
      '#element_validate' => [
        [$class, 'validateElement'],
      ],
      '#theme_wrappers' => [],
      // Possible values: mobile, home, business, or fax.
      '#phone_type_default' => static::dpUtility()->getFieldDefault('phone_type'),
      // Whether or not user can access the phone_type dropdown.
      '#phone_type_access' => TRUE,
      // Values that should be included in the phone_type dropdown.
      '#phone_type_options' => [],
      // Input mask for phone numbers.
      '#phone_mask' => '(999) 999-9999',
      // Max length of fields.
      '#phone_maxlength' => static::dpUtility()::PHONE_MAXLENGTH,
    ];
  }

  /**
   * Processes the form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    if (!empty($element['#default_value']) && is_array($element['#default_value'])) {
      $default_value = $element['#default_value'];
    }
    else {
      $default_value = [
        'mobile_phone' => static::dpUtility()->getFieldDefault('mobile_phone'),
        'home_phone' => static::dpUtility()->getFieldDefault('home_phone'),
        'business_phone' => static::dpUtility()->getFieldDefault('business_phone'),
        'fax_phone' => static::dpUtility()->getFieldDefault('fax_phone'),
      ];
    }
    if (empty($default_value['phone_type'])) {
      $default_value['phone_type'] = $element['#phone_type_default'] ?? static::dpUtility()->getFieldDefault('phone_type');
    }
    $element_token = static::getElementToken($element, $form_state);
    $element['element_token'] = [
      '#type' => 'hidden',
      '#default_value' => $element_token,
      '#attributes' => ['class' => ['donorperfect-element-token']],
    ];
    $element['#prefix'] = '<div class="donorperfect-element-wrapper donorperfect-phone-wrapper" id="donorperfect-element-wrapper-' . $element_token . '" element-token="' . $element_token . '">';
    $element['#suffix'] = '</div>';
    // Phone Type dropdown.
    $phone_type_access = $element['#phone_type_access'] ?? TRUE;
    $phone_type_options = [];
    $phone_type_options_all = [
      'mobile' => t('Mobile Phone'),
      'home' => t('Home Phone'),
      'business' => t('Business Phone'),
      'fax' => t('Fax'),
    ];
    if (!empty($element['#phone_type_options']) && is_array($element['#phone_type_options'])) {
      foreach ($element['#phone_type_options'] as $type) {
        if (array_key_exists($type, $phone_type_options_all)) {
          $phone_type_options[$type] = $phone_type_options_all[$type];
        }
      }
    }
    else {
      $phone_type_options = $phone_type_options_all;
    }
    if (!array_key_exists($default_value['phone_type'], $phone_type_options)) {
      if (array_key_exists($default_value['phone_type'], $phone_type_options_all)) {
        $phone_type_options[$default_value['phone_type']] = $phone_type_options_all[$default_value['phone_type']];
      }
      else {
        $default_value['phone_type'] = '';
      }
    }
    elseif ($default_value['phone_type'] === 'first_with_value') {
      foreach (array_keys($phone_type_options) as $phone_type) {
        if (!empty($default_value["{$phone_type}_phone"])) {
          $default_value['phone_type'] = $phone_type;
          break;
        }
      }
      if ($default_value['phone_type'] === 'first_with_value') {
        $default_value['phone_type'] = '';
      }
    }
    if ($phone_type_access) {
      $element['phone_type'] = [
        '#type' => 'select',
        '#title' => t('Phone Type'),
        '#title_display' => 'invisible',
        '#options' => $phone_type_options,
        '#default_value' => $default_value['phone_type'],
        '#required' => $element['#required'] ?? FALSE,
        '#attributes' => [
          'class' => ['phone-type-' . $element_token, 'dpfield'],
          'token' => 'phone-type-' . $element_token,
          'dpfield' => 'phone_type',
        ],
        '#wrapper_attributes' => ['class' => ['phone-type-wrapper']],
      ];
    }
    else {
      $element['phone_type'] = [
        '#type' => 'hidden',
        '#default_value' => $default_value['phone_type'],
        '#attributes' => [
          'class' => ['phone-type-' . $element_token, 'dpfield'],
          'token' => 'phone-type-' . $element_token,
          'dpfield' => 'phone_type',
        ],
      ];
    }
    // Phone textfields.
    foreach ($phone_type_options as $phone_type => $phone_title) {
      $element[$phone_type . '_phone'] = [
        '#type' => 'textfield',
        '#title' => t(':phone_title Number', [':phone_title' => $phone_title]),
        '#title_display' => 'invisible',
        '#default_value' => $default_value[$phone_type . '_phone'] ?? '',
        '#attributes' => [
          'class' => [$phone_type . '-phone-' . $element_token, 'dpfield'],
          'dpfield' => $phone_type . '_phone',
          'placeholder' => t(':phone_type phone number', [':phone_type' => static::dpUtility()->ucword($phone_type)]),
        ],
        '#wrapper_attributes' => ['class' => [$phone_type . '-phone-wrapper']],
        '#maxlength' => $element['#phone_maxlength'] ?? static::dpUtility()::PHONE_MAXLENGTH,
        '#dp_validate' => ['phone'],
        '#states' => [
          'visible' => [
            ':input[token="phone-type-' . $element_token . '"]' => ['value' => $phone_type],
          ],
        ],
      ];
      if (!empty($element['#required'])) {
        $element[$phone_type . '_phone']['#states']['required'] = [
          ':input[token="phone-type-' . $element_token . '"]' => ['value' => $phone_type],
        ];
      }
      if (!empty($element['#phone_mask']) && is_string($element['#phone_mask'])) {
        $element[$phone_type . '_phone']['#attributes']['data-mask-value'] = $element['#phone_mask'];
      }
    }
    // Add libraries.
    $element['#attached']['library'][] = 'donorperfect/form_element';
    return $element;
  }

  /**
   * Validates the form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public static function validateElement(array $element, FormStateInterface $form_state, array $complete_form) {
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $output = $element['#default_value'] = (array) $element['#default_value'];
    if (is_array($input)) {
      $output = $input;
    }
    return $output;
  }

}
