<?php

namespace Drupal\donorperfect\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\donorperfect\FormElementBase;

/**
 * Provides a form element for DonorPerfect donor name.
 *
 * @FormElement("donorperfect_name")
 */
class Name extends FormElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#input' => TRUE,
      '#default_value' => NULL,
      '#process' => [
        [$class, 'processElement'],
      ],
      '#pre_render' => [],
      '#after_build' => [],
      '#element_validate' => [
        [$class, 'validateElement'],
      ],
      '#theme_wrappers' => [],
      // Whether or not the search functionality should be included.
      '#search_access' => TRUE,
      // Any valid DonorPerfect donor_type code value.
      '#donor_type_default' => 'IN',
      // Whether or not user can access the donor_type dropdown.
      '#donor_type_access' => TRUE,
      // Values that should be made available in the donor_type dropdown.
      '#donor_type_options' => [],
      // Max length of fields.
      '#first_name_maxlength' => static::dpUtility()::FIRST_NAME_MAXLENGTH,
      '#last_name_maxlength' => static::dpUtility()::LAST_NAME_MAXLENGTH,
    ];
  }

  /**
   * Processes the form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    $default_value = !empty($element['#default_value']) && is_array($element['#default_value']) ? $element['#default_value'] : [];
    $search_permission = !empty($element['#search_access']) && static::dpUtility()->currentUser()->hasPermission('donorperfect user');
    $element_token = static::getElementToken($element, $form_state);
    $element['element_token'] = [
      '#type' => 'hidden',
      '#default_value' => $element_token,
      '#attributes' => ['class' => ['donorperfect-element-token']],
    ];
    $element['#prefix'] = '<div class="donorperfect-element-wrapper donorperfect-name-wrapper" id="donorperfect-element-wrapper-' . $element_token . '" element-token="' . $element_token . '">';
    $element['#suffix'] = '</div>';
    // Donor Type dropdown.
    $donor_type_access = isset($element['#donor_type_access']) ? (bool) $element['#donor_type_access'] : TRUE;
    $donor_type_default = $default_value['donor_type'] ?? ($element['#donor_type_default'] ?? 'IN');
    $donor_type_default_text = static::dpUtility()->getCodeDescription('donor_type', $donor_type_default);
    if (empty($donor_type_default_text)) {
      $donor_type_default = static::dpUtility()->getFieldDefault('donor_type');
      $donor_type_default_text = static::dpUtility()->getCodeDescription('donor_type', $donor_type_default);
    }
    $donor_type_options = [];
    if ($donor_type_access) {
      if (!empty($element['#donor_type_options']) && is_array($element['#donor_type_options'])) {
        foreach ($element['#donor_type_options'] as $donor_type_option) {
          $donor_type_text = static::dpUtility()->getCodeDescription('donor_type', $donor_type_option);
          if (!empty($donor_type_text)) {
            $donor_type_options[$donor_type_option] = $donor_type_text;
          }
        }
      }
      else {
        $donor_type_options = static::dpUtility()->getCodesAsOptions('donor_type');
      }
    }
    if (!array_key_exists($donor_type_default, $donor_type_options)) {
      $donor_type_options[$donor_type_default] = $donor_type_default_text;
    }
    if ($donor_type_access) {
      $element['donor_type'] = [
        '#type' => 'select',
        '#title' => t('Donor Type'),
        '#title_display' => 'invisible',
        '#options' => $donor_type_options,
        '#default_value' => $donor_type_default,
        '#required' => $element['#required'] ?? FALSE,
        '#attributes' => [
          'class' => ['donor-type-' . $element_token, 'dpfield'],
          'token' => 'donor-type-' . $element_token,
          'dpfield' => 'donor_type',
        ],
        '#wrapper_attributes' => ['class' => ['donor-type-wrapper']],
      ];
    }
    else {
      $element['donor_type'] = [
        '#type' => 'hidden',
        '#default_value' => $donor_type_default,
        '#attributes' => [
          'class' => ['donor-type-' . $element_token, 'dpfield'],
          'token' => 'donor-type-' . $element_token,
          'dpfield' => 'donor_type',
        ],
      ];
    }
    // Name textfields.
    $element['first_name'] = [
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#title_display' => 'invisible',
      '#default_value' => $default_value['first_name'] ?? static::dpUtility()->getFieldDefault('first_name'),
      '#attributes' => [
        'class' => ['first-name-' . $element_token, 'dpfield'],
        'dpfield' => 'first_name',
        'placeholder' => t('First Name'),
      ],
      '#wrapper_attributes' => ['class' => ['first-name-wrapper']],
      '#maxlength' => !empty($element['#first_name_maxlength']) ? (int) $element['#first_name_maxlength'] : static::dpUtility()::FIRST_NAME_MAXLENGTH,
      '#dp_validate' => ['alphaDash'],
      '#dp_filter' => ['properCase'],
      '#states' => [
        'visible' => [
          ':input[token="donor-type-' . $element_token . '"]' => ['value' => 'IN'],
        ],
      ],
    ];
    if (!empty($element['#required'])) {
      $element['first_name']['#states']['required'] = [':input[token="donor-type-' . $element_token . '"]' => ['value' => 'IN']];
    }
    $donor_type_value = static::getFieldValue('donor_type', $element, $form_state, $donor_type_default);
    $last_name_placeholder = ($donor_type_value === 'IN') ? t('Last Name') : $donor_type_options[$donor_type_value] . ' ' . t('Name');
    $element['last_name'] = [
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#title_display' => 'invisible',
      '#default_value' => $default_value['last_name'] ?? static::dpUtility()->getFieldDefault('last_name'),
      '#required' => $element['#required'] ?? FALSE,
      '#attributes' => [
        'class' => ['last-name-' . $element_token, 'dpfield'],
        'dpfield' => 'last_name',
        'placeholder' => $last_name_placeholder,
      ],
      '#wrapper_attributes' => ['class' => ['last-name-wrapper']],
      '#maxlength' => !empty($element['#last_name_maxlength']) ? (int) $element['#last_name_maxlength'] : static::dpUtility()::LAST_NAME_MAXLENGTH,
      '#dp_validate' => ['alphaDash'],
      '#dp_filter' => ['properCase'],
    ];
    if ($search_permission) {
      $element['dpsearch_button'] = [
        '#type' => 'markup',
        '#markup' => '<div class="donorperfect-name-search-button-wrapper"><img src="/' . static::dpUtility()->getModulePath('donorperfect') . '/assets/images/search-icon.png" title="' . t('Search DonorPerfect') . '"></div>',
      ];
      $element['donor_id'] = [
        '#type' => 'hidden',
        '#default_value' => $default_value['donor_id'] ?? 0,
        '#attributes' => [
          'class' => ['donor-id-' . $element_token, 'dpfield'],
          'dpfield' => 'donor_id',
        ],
      ];
      $element['#suffix'] .= '<div class="donorperfect-name-search-wrapper" id="donorperfect-name-search-wrapper-' . $element_token . '" element-token="' . $element_token . '"><div class="donorperfect-name-search-searching" style="display:none;"><img src="/core/misc/throbber-active.gif" />Searching DonorPerfect...</div><div class="donorperfect-name-search-results" style="display:none;"></div></div>';
    }
    // Add libraries.
    $element['#attached']['library'][] = 'donorperfect/form_element';
    $element['#attached']['library'][] = 'donorperfect/form_element.name';
    if ($search_permission) {
      $element['#attached']['library'][] = 'donorperfect/form_element.name.search';
    }
    return $element;
  }

  /**
   * Validates the form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public static function validateElement(array $element, FormStateInterface $form_state, array $complete_form) {
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $output = $element['#default_value'] = (array) $element['#default_value'];
    if (is_array($input)) {
      $output = $input;
      if (!empty($input['donor_type']) && ($input['donor_type'] !== 'IN')) {
        $output['org_name'] = $input['last_name'];
        $output['last_name'] = $output['first_name'] = '';
      }
    }
    return $output;
  }

}
