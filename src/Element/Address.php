<?php

namespace Drupal\donorperfect\Element;

use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Drupal\Core\Form\FormStateInterface;
use Drupal\donorperfect\FormElementBase;

/**
 * Provides a form element for DonorPerfect donor address.
 *
 * @FormElement("donorperfect_address")
 */
class Address extends FormElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#input' => TRUE,
      '#default_value' => NULL,
      '#process' => [
        [$class, 'processElement'],
      ],
      '#pre_render' => [],
      '#after_build' => [
        [$class, 'afterBuild'],
      ],
      '#element_validate' => [
        [$class, 'validateElement'],
      ],
      '#theme_wrappers' => [],
      // Whether or not the user should have access to the country dropdown.
      '#country_access' => FALSE,
      // Default country code.
      '#country_default' => static::dpUtility()->getFieldDefault('country'),
      // Array of country options that should be included in dropdown.
      '#country_options' => [],
      // Whether or not the user should have access to address line 2.
      '#address2_access' => FALSE,
      // Whether or not the user should have access to address line 3.
      '#address3_access' => FALSE,
      // Max length of fields.
      '#address_maxlength' => static::dpUtility()::ADDRESS_MAXLENGTH,
      '#city_maxlength' => static::dpUtility()::CITY_MAXLENGTH,
      '#zip_maxlength' => static::dpUtility()::ZIP_MAXLENGTH,
    ];
  }

  /**
   * Processes the form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    if (!empty($element['#default_value']) && is_array($element['#default_value'])) {
      $default_value = $element['#default_value'];
      // Rename $default_value array keys from DonorPerfect field names
      // to Drupal address field names.
      static::renameArrayKeys($default_value);
      if (empty($default_value['country_code'])) {
        $default_value['country_code'] = $element['#country_default'] ?? static::dpUtility()->getFieldDefault('country');
      }
    }
    else {
      $default_value = [
        'address_line1' => static::dpUtility()->getFieldDefault('address'),
        'address_line2' => static::dpUtility()->getFieldDefault('address2'),
        'address_line3' => static::dpUtility()->getFieldDefault('address3'),
        'locality' => static::dpUtility()->getFieldDefault('city'),
        'administrative_area' => static::dpUtility()->getFieldDefault('state'),
        'postal_code' => static::dpUtility()->getFieldDefault('zip'),
        'country_code' => $element['#country_default'] ?? static::dpUtility()->getFieldDefault('country'),
      ];
    }
    $element_token = static::getElementToken($element, $form_state);
    $element['element_token'] = [
      '#type' => 'hidden',
      '#default_value' => $element_token,
      '#attributes' => ['class' => ['donorperfect-element-token']],
    ];
    $element['#prefix'] = '<div class="donorperfect-element-wrapper donorperfect-address-wrapper" id="donorperfect-element-wrapper-' . $element_token . '" element-token="' . $element_token . '">';
    $element['#suffix'] = '</div>';
    $required = $element['#required'] ?? FALSE;
    $element['address'] = [
      '#type' => 'address',
      '#default_value' => $default_value,
      '#required' => $required,
      '#field_overrides' => [
        AddressField::ADDRESS_LINE1 => $required ? FieldOverride::REQUIRED : FieldOverride::OPTIONAL,
        AddressField::LOCALITY => $required ? FieldOverride::REQUIRED : FieldOverride::OPTIONAL,
        AddressField::ADMINISTRATIVE_AREA => $required ? FieldOverride::REQUIRED : FieldOverride::OPTIONAL,
        AddressField::POSTAL_CODE => $required ? FieldOverride::REQUIRED : FieldOverride::OPTIONAL,
        AddressField::ORGANIZATION => FieldOverride::HIDDEN,
        AddressField::GIVEN_NAME => FieldOverride::HIDDEN,
        AddressField::FAMILY_NAME => FieldOverride::HIDDEN,
        AddressField::ADDRESS_LINE2 => $element['#address2_access'] ? ($required ? FieldOverride::REQUIRED : FieldOverride::OPTIONAL) : FieldOverride::HIDDEN,
        AddressField::ADDRESS_LINE3 => $element['#address3_access'] ? ($required ? FieldOverride::REQUIRED : FieldOverride::OPTIONAL) : FieldOverride::HIDDEN,
      ],
    ];
    // Add libraries.
    $element['#attached']['library'][] = 'donorperfect/form_element';
    return $element;
  }

  /**
   * Modify element after it is built by the address module.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function afterBuild(array $element, FormStateInterface $form_state) {
    // Determine if country should be hidden.
    if (empty($element['#country_access'])) {
      $element['address']['country_code']['#access'] = FALSE;
    }
    $dpfields = [
      'address_line1' => 'address',
      'locality' => 'city',
      'administrative_area' => 'state',
      'postal_code' => 'zip',
    ];
    foreach ($dpfields as $field_key => $dpfield) {
      $element['address'][$field_key]['#attributes']['class'][] = 'dpfield';
      $element['address'][$field_key]['#attributes']['dpfield'] = $dpfield;
      $element['address'][$field_key]['#attributes']['placeholder'] = $element['address'][$field_key]['#title'];
      $element['address'][$field_key]['#title_display'] = 'invisible';
    }
    $element['address']['administrative_area']['#empty_option'] = t('- State -');
    $element['address']['address_line1']['#maxlength'] = $element['#address_maxlength'] ?? static::dpUtility()::ADDRESS_MAXLENGTH;
    $element['address']['address_line1'] += [
      '#dp_validate' => ['alphaDash'],
      '#dp_filter' => ['properCase'],
    ];
    if (!empty($element['address2_access'])) {
      $element['address']['address_line2']['#maxlength'] = $element['#address_maxlength'] ?? static::dpUtility()::ADDRESS_MAXLENGTH;
      $element['address']['address_line2'] += [
        '#dp_validate' => ['alphaDash'],
        '#dp_filter' => ['properCase'],
      ];
    }
    if (!empty($element['address3_access'])) {
      $element['address']['address_line3']['#maxlength'] = $element['#address_maxlength'] ?? static::dpUtility()::ADDRESS_MAXLENGTH;
      $element['address']['address_line3'] += [
        '#dp_validate' => ['alphaDash'],
        '#dp_filter' => ['properCase'],
      ];
    }
    $element['address']['locality']['#maxlength'] = $element['#city_maxlength'] ?? static::dpUtility()::CITY_MAXLENGTH;
    $element['address']['locality'] += [
      '#dp_validate' => ['alphaDash'],
      '#dp_filter' => ['properCase'],
    ];
    $element['address']['postal_code']['#maxlength'] = $element['#zip_maxlength'] ?? static::dpUtility()::ZIP_MAXLENGTH;
    $element['address']['postal_code'] += [
      '#dp_validate' => ['alphaDash'],
      '#dp_filter' => ['upperCase'],
    ];
    return $element;
  }

  /**
   * Validates the form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public static function validateElement(array $element, FormStateInterface $form_state, array $complete_form) {
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $output = $element['#default_value'] = (array) $element['#default_value'];
    if (is_array($input)) {
      $output = $input;
    }
    return $output;
  }

  /**
   * Rename array keys from DonorPerfect fields to Drupal address fields.
   *
   * @param array $input
   *   Array of address fields.
   */
  public static function renameArrayKeys(array &$input) {
    $array_keys = [
      'address' => 'address_line1',
      'address2' => 'address_line2',
      'address3' => 'address_line3',
      'city' => 'locality',
      'state' => 'administrative_area',
      'zip' => 'postal_code',
      'country' => 'country_code',
    ];
    foreach ($array_keys as $dp_field => $drupal_field) {
      if (isset($input[$dp_field])) {
        $input[$drupal_field] = $input[$dp_field];
      }
    }
  }

}
