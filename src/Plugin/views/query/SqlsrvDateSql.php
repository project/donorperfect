<?php

namespace Drupal\donorperfect\Plugin\views\query;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\views\Plugin\views\query\DateSqlInterface;

/**
 * MSSQL-specific date handling.
 */
class SqlsrvDateSql implements DateSqlInterface {

  use DependencySerializationTrait;

  /**
   * An array of PHP-to-MSSQL replacement patterns.
   *
   * @var array
   */
  protected static $replace = [
    'Y' => 'yyyy',
    'y' => 'yy',
    'M' => 'MMM',
    'm' => 'MM',
    'n' => 'M',
    'F' => 'MMMM',
    'D' => 'ddd',
    'd' => 'dd',
    'l' => 'ddd',
    'j' => 'd',
    // No week number format.
    'H' => 'HH',
    'h' => 'hh',
    'i' => 'mm',
    's' => 'ss',
    'A' => 'tt',
  ];

  /**
   * {@inheritdoc}
   */
  public function getDateField($field, $string_date) {
    if ($string_date) {
      return "CONVERT(datetime2, $field, 127)";
    }

    // Base date field storage is timestamp, so the date to be returned here is
    // epoch + stored value (seconds from epoch).
    return "DATEADD(second, $field, '19700101')";
  }

  /**
   * {@inheritdoc}
   */
  public function getDateFormat($field, $format) {
    $format = strtr($format, static::$replace);
    if ($format === 'W') {
      return "DATEPART(iso_week, $field)";
    }
    return "FORMAT($field, '$format')";
  }

  /**
   * {@inheritdoc}
   */
  public function setTimezoneOffset($offset) {
    // $this->database->query("SET @@session.time_zone = '$offset'");
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldTimezoneOffset(&$field, $offset) {
    if (!empty($offset)) {
      $field = "DATEADD(second, $offset, $field)";
    }
  }

}
