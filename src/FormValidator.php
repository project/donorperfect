<?php

namespace Drupal\donorperfect;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Provides validation for DonorPerfect form elements.
 */
class FormValidator {

  /**
   * Recursively add DonorPerfect form validation/filters to form elements.
   *
   * @param mixed $element
   *   The form element to check for validation/filter keys.
   */
  public static function addValidation(mixed &$element) {
    if (is_array($element)) {
      foreach (Element::children($element) as $key) {
        if (is_array($element[$key])) {
          if (!empty($element[$key]['#dp_validate']) || !empty($element[$key]['#dp_filter'])) {
            $element[$key] += ['#element_validate' => []];
            if (!in_array('\Drupal\donorperfect\FormValidator::validateElement', $element[$key]['#element_validate'])) {
              $element[$key]['#element_validate'][] = '\Drupal\donorperfect\FormValidator::validateElement';
            }
          }
          static::addValidation($element[$key]);
        }
      }
    }
  }

  /**
   * Validate a form element and apply filters.
   *
   * @param array $element
   *   The form element to be validated/filtered.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object for the form.
   * @param array $form
   *   The full form array.
   */
  public static function validateElement(array $element, FormStateInterface $form_state, array $form) {
    if (is_array($element) && !empty($element['#parents'])) {
      $value = $form_state->getValue($element['#parents']);
      if (isset($element['#dp_validate']) && !is_array($element['#dp_validate'])) {
        $element['#dp_validate'] = [$element['#dp_validate']];
      }
      if (!empty($element['#dp_validate'])) {
        foreach ($element['#dp_validate'] as $validate_type) {
          if (!is_string($validate_type)) {
            continue;
          }
          // Make sure first letter is capitalized.
          $validate_type = strtoupper(substr($validate_type, 0, 1)) . substr($validate_type, 1);
          $validate_key = 'validate' . $validate_type;
          if (method_exists(__CLASS__, $validate_key)) {
            $result = static::{$validate_key}($value, 'string');
            if ($result === TRUE) {
              // Check if there is a filter with the same name.
              $filter_key = 'filter' . $validate_type;
              if (method_exists(__CLASS__, $filter_key)) {
                $form_state->setValue($element['#parents'], static::{$filter_key}($value));
              }
            }
            else {
              $message = is_string($result) ? $result : '';
              $form_state->setErrorByName(implode('][', $element['#parents']), t($message, ['%field' => $element['#title'] ?? '']));
            }
          }
        }
      }
      if (isset($element['#dp_filter']) && !is_array($element['#dp_filter'])) {
        $element['#dp_filter'] = [$element['#dp_filter']];
      }
      if (!empty($element['#dp_filter'])) {
        foreach ($element['#dp_filter'] as $filter_type) {
          if (!is_string($filter_type)) {
            continue;
          }
          // Make sure first letter is capitalized.
          $filter_type = strtoupper(substr($filter_type, 0, 1)) . substr($filter_type, 1);
          $filter_key = 'filter' . $filter_type;
          if (method_exists(__CLASS__, $filter_key)) {
            $form_state->setValue($element['#parents'], static::{$filter_key}($value));
          }
        }
      }
    }
  }

  /**
   * Validate string to ensure it contains a valid email address.
   *
   * @param string $input
   *   The input string to be validated.
   * @param string $return_type
   *   The desired return type.
   *
   * @return bool|string
   *   Boolean or string indicating whether or not $input is valid.
   */
  public static function validateEmail(string $input, string $return_type = 'bool') {
    $input = trim($input);
    if (!empty($input)) {
      if (!\Drupal::service('email.validator')->isValid($input)) {
        return ($return_type === 'bool') ? FALSE : 'Please enter a valid email address in %field.';
      }
    }
    return TRUE;
  }

  /**
   * Filter input string to return an email address.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterEmail(string $input) {
    return strtolower(trim($input));
  }

  /**
   * Validate string to ensure it contains a valid URL.
   *
   * @param string $input
   *   The input string to be validated.
   * @param string $return_type
   *   The desired return type.
   *
   * @return bool|string
   *   Boolean or string indicating whether or not $input is valid.
   */
  public static function validateUrl(string $input, string $return_type = 'bool') {
    if (!empty($input)) {
      $parts = parse_url($input);
      if (empty($parts) || !is_array($parts) || empty($parts['scheme']) || empty($parts['host'])) {
        return ($return_type === 'bool') ? FALSE : 'Please enter a valid URL in %field.';
      }
    }
    return TRUE;
  }

  /**
   * Validate string to ensure it contains a valid phone number.
   *
   * @param string $input
   *   The input string to be validated.
   * @param string $return_type
   *   The desired return type.
   *
   * @return bool|string
   *   Boolean or string indicating whether or not $input is valid.
   */
  public static function validatePhone(string $input, string $return_type = 'bool') {
    $input = trim($input);
    if (!empty($input)) {
      $input = preg_replace("/[^0-9]/", "", $input);
      if (!preg_match('/^[0-9]{10}$/', $input)) {
        return ($return_type === 'bool') ? FALSE : 'Please enter a valid phone number in %field.';
      }
    }
    return TRUE;
  }

  /**
   * Filter input string to return a formatted phone number.
   *
   * @param string $input
   *   The input string to be formatted.
   * @param string $format
   *   The desired format of the phone number.
   *   Possible values: dashes or parentheses.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterPhone(string $input, string $format = 'parentheses') {
    $output = trim($input);
    $value = preg_replace("/[^0-9]/", "", $input);
    if (preg_match('/^[0-9]{10}$/', $value)) {
      switch ($format) {
        case 'dashes':
          $output = substr($value, 0, 3) . '-' . substr($value, 3, 3) . '-' . substr($value, 6, 4);
          break;

        case 'parentheses':
        default:
          $output = '(' . substr($value, 0, 3) . ') ' . substr($value, 3, 3) . '-' . substr($value, 6, 4);
          break;

      }
    }
    return $output;
  }

  /**
   * Validate string to ensure it contains only alpha-numerics and dashes.
   *
   * @param string $input
   *   The input string to be validated.
   * @param string $return_type
   *   The desired return type.
   *
   * @return bool|string
   *   Boolean or string indicating whether or not $input is valid.
   */
  public static function validateAlphaDash(string $input, string $return_type = 'bool') {
    $input = trim($input);
    if (!empty($input)) {
      if (!preg_match("/^[ 0-9a-zA-Z,'#@!_\-\.\/\?\(\)]+$/", $input)) {
        return ($return_type === 'bool') ? FALSE : 'Please use only letters, numbers, apostrophe, hyphen and underscore in %field.';
      }
    }
    return TRUE;
  }

  /**
   * Filter input string to return only alpha-numerics and dashes.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterAlphaDash(string $input) {
    // Use same regex as the alpha_dash rule.
    return preg_replace("/[ 0-9a-zA-Z,'#@!_\-\.\/\?\(\)]/", '', $input);
  }

  /**
   * Validate string to ensure it contains a decimal number.
   *
   * @param string $input
   *   The input string to be validated.
   * @param string $return_type
   *   The desired return type.
   *
   * @return bool|string
   *   Boolean or string indicating whether or not $input is valid.
   */
  public static function validateDecimal(string $input, string $return_type = 'bool') {
    $input = trim($input);
    if (!empty($input)) {
      $input = str_replace([' ', ','], '', $input);
      if (!preg_match('/^-?\d*\.?\d*$/', $input)) {
        return ($return_type === 'bool') ? FALSE : 'Please enter a valid number in %field.';
      }
    }
    return TRUE;
  }

  /**
   * Filter input string to return a decimal number.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterDecimal(string $input) {
    // This filter should be used in conjunction with the decimal rule.
    $input = str_replace([' ', ','], '', $input);
    return $input;
  }

  /**
   * Validate string to ensure it contains a money number.
   *
   * @param string $input
   *   The input string to be validated.
   * @param string $return_type
   *   The desired return type.
   *
   * @return bool|string
   *   Boolean or string indicating whether or not $input is valid.
   */
  public static function validateMoney(string $input, string $return_type = 'bool') {
    $input = trim($input);
    if (!empty($input)) {
      $input = str_replace([' ', ','], '', $input);
      if (!preg_match('/^-?[0-9]+(?:\.[0-9]{1,2})?$/', $input)) {
        return ($return_type === 'bool') ? FALSE : 'Please enter a valid number in %field.';
      }
    }
    return TRUE;
  }

  /**
   * Filter input string to return a decimal number.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterMoney(string $input) {
    $input = str_replace([' ', ','], '', $input);
    if (!empty($input)) {
      $matches = [];
      if (preg_match('/^(-?[0-9]+)(\.[0-9]{1,2})?$/', $input, $matches)) {
        if (!empty($matches[2])) {
          $input = $matches[1] . $matches[2];
          if (strlen($matches[2]) === 2) {
            $input = $input . '0';
          }
        }
        else {
          $input = $matches[1] . '.00';
        }
      }
    }
    return $input;
  }

  /**
   * Validate string to ensure it contains only numeric digits.
   *
   * @param string $input
   *   The input string to be validated.
   * @param string $return_type
   *   The desired return type.
   *
   * @return bool|string
   *   Boolean or string indicating whether or not $input is valid.
   */
  public static function validateDigit(string $input, string $return_type = 'bool') {
    $input = trim($input);
    if (!empty($input)) {
      $input = str_replace([' ', ','], '', $input);
      if (!preg_match('/^[0-9]+$/', $input)) {
        return ($return_type === 'bool') ? FALSE : 'Please enter a valid number in %field.';
      }
    }
    return TRUE;
  }

  /**
   * Filter input string to return only numeric digits.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterDigit(string $input) {
    return preg_replace('/[^0-9]/', '', $input);
  }

  /**
   * Filter input string with check_plain.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterCheckPlain(string $input) {
    if (!empty($input)) {
      return Html::escape($input);
    }
    return '';
  }

  /**
   * Filter input string with filter_xss.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterXss(string $input) {
    if (!empty($input)) {
      return Xss::filter($input);
    }
    return '';
  }

  /**
   * Filter input string to return proper cased string.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterProperCase(string $input) {
    $text = trip($input);
    $loop = TRUE;
    while ($loop) {
      $matches = [];
      preg_match("/([\w']+)\b/si", $text, $matches);
      if (!empty($matches)) {
        if (in_array(strtolower($matches[1]), ['and', 'or', 'of'])) {
          $input = preg_replace('/\b' . preg_quote($matches[1], '/') . '\b/', strtolower($matches[1]), $input, 1);
        }
        elseif (ctype_lower(substr($matches[1], 0, 1)) || ctype_upper($matches[1])) {
          $input = preg_replace('/\b' . preg_quote($matches[1], '/') . '\b/', static::dpUtility()->ucword($matches[1]), $input, 1);
        }
        $text = preg_replace('/' . preg_quote($matches[0], '/') . '/', '', $text, 1);
      }
      else {
        $loop = FALSE;
      }
    }
    return $input;
  }

  /**
   * Filter a string to lower case by reference.
   *
   * @param string $string
   *   The input string to be formatted.
   */
  public static function filterStrtolowerByRef(string &$string) {
    $string = strtolower($string);
  }

  /**
   * Filter a string to lower case.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterLowerCase(string $input) {
    return strtolower($input);
  }

  /**
   * Filter a string to upper case.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterUpperCase(string $input) {
    return strtoupper($input);
  }

  /**
   * Filter a string using trim.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterTrim(string $input) {
    return trim($input);
  }

  /**
   * Filter a string to remove characters not supported through the API.
   *
   * @param string $input
   *   The input string to be formatted.
   *
   * @return string
   *   The formatted string.
   */
  public static function filterUnsupportedCharacters(string $input) {
    $output = $input;
    $output = str_replace('&', ' and ', $output);
    $output = str_replace('# ', 'Unit ', $output);
    return $output;
  }

  /**
   * Returns the DPUtility service.
   *
   * @return \Drupal\donorperfect\DPUtility
   *   The DPUtility service.
   */
  public static function dpUtility() {
    return \Drupal::service('donorperfect.dputility');
  }

}
