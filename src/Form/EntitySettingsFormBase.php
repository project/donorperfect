<?php

namespace Drupal\donorperfect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\donorperfect\DPUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form controller for DonorPerfect entity settings.
 *
 * @ingroup donorperfect
 */
abstract class EntitySettingsFormBase extends ConfigFormBase {

  /**
   * The entity_type_id for which this is the settings form.
   */
  const ENTITY_TYPE_ID = '';

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type object.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The entity last installed schema repository service.
   *
   * @var Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface
   */
  protected $schemaRepository;

  /**
   * The DPUtility service.
   *
   * @var \Drupal\donorperfect\DPUtility
   */
  protected $dpUtility;

  /**
   * Constructs a new \Drupal\donorperfect\Form\EntitySettingsFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $schema_repository
   *   The entity last installed schema repository service.
   * @param \Drupal\donorperfect\DPUtility $dputility
   *   The DPUtility service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityLastInstalledSchemaRepositoryInterface $schema_repository,
    DPUtility $dputility,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->schemaRepository = $schema_repository;
    $this->dpUtility = $dputility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity.last_installed_schema.repository'),
      $container->get('donorperfect.dputility')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['donorperfect.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::ENTITY_TYPE_ID . '_settings_form';
  }

  /**
   * Gets the entity type object.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The entity type object.
   */
  protected function getEntityType() {
    if (empty($this->entityType)) {
      $this->entityType = $this->entityTypeManager->getDefinition(static::ENTITY_TYPE_ID);
    }
    return $this->entityType;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('donorperfect.settings');
    $entity_type_label = $this->getEntityType()->getLabel();
    $base_field = $this->getEntityType()->getKey('id');
    $tables = [
      'base' => $this->getEntityType()->getBaseTable(),
      'udf' => $this->getEntityType()->get('udf_table'),
    ];
    $field_options = [];
    foreach ($tables as $table) {
      $field_options += $this->dpUtility->getFieldsAsOptions($table);
    }
    if (!empty($field_options)) {
      // Alphabetize field list.
      asort($field_options);
      $form[static::ENTITY_TYPE_ID] = [
        '#type' => 'details',
        '#title' => $this->t(':label Entities', [':label' => $entity_type_label]),
        '#tree' => TRUE,
        '#open' => FALSE,
      ];
      $form[static::ENTITY_TYPE_ID]['fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Entity Fields'),
        '#description' => $this->t('Select the DonorPerfect fields that should be included in :label entities in Drupal.', [':label' => $entity_type_label]),
        '#description_display' => 'before',
        '#options' => $field_options,
        '#default_value' => $config->get('entity.' . static::ENTITY_TYPE_ID . '.fields') ?: [$base_field],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Remove unselected entity fields.
    $values_key = ['entity', static::ENTITY_TYPE_ID, 'fields'];
    $values = $form_state->getValue($values_key, []);
    foreach ($values as $key => $value) {
      if ($key !== $value) {
        unset($values[$key]);
      }
    }
    // Make sure the base field is selected.
    $base_field = $this->getEntityType()->getKey('id');
    $values[$base_field] = $base_field;
    // Set $values back on $form_state.
    $form_state->setValue($values_key, $values);
  }

  /**
   * Perform necessary procedures after form submission is complete.
   *
   * @param array $form
   *   The complete form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object for the form.
   */
  public function postSubmitForm(array &$form, FormStateInterface $form_state) {
    // Entity field storage definitions need to be rebuilt.
    $this->entityFieldManager->clearCachedFieldDefinitions();
    $field_definitions = $this->entityFieldManager->getBaseFieldDefinitions(static::ENTITY_TYPE_ID);
    $this->schemaRepository->setLastInstalledFieldStorageDefinitions(static::ENTITY_TYPE_ID, $field_definitions);
  }

}
