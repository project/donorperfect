<?php

namespace Drupal\donorperfect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\donorperfect\DPUtility;
use Drupal\donorperfect\Entity\ControllerServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for DonorPerfect module settings.
 *
 * @ingroup donorperfect
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The DonorPerfect entity controller service.
   *
   * @var \Drupal\donorperfect\Entity\ControllerServiceInterface
   */
  protected $dpControllerService;

  /**
   * The DPUtility service.
   *
   * @var \Drupal\donorperfect\DPUtility
   */
  protected $dpUtility;

  /**
   * The default destination for a redirect.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Constructs a new \Drupal\donorperfect\Form\SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\donorperfect\Entity\ControllerServiceInterface $controller_service
   *   The DonorPerfect entity controller service.
   * @param \Drupal\donorperfect\DPUtility $dputility
   *   The DPUtility service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ControllerServiceInterface $controller_service, DPUtility $dputility, RedirectDestinationInterface $redirect_destination) {
    parent::__construct($config_factory);
    $this->dpControllerService = $controller_service;
    $this->dpUtility = $dputility;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('donorperfect.entity.controller_service'),
      $container->get('donorperfect.dputility'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['donorperfect.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'donorperfect_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('donorperfect.settings');
    $form['cache'] = [
      '#type' => 'details',
      '#title' => $this->t('DonorPerfect Cache'),
      '#description' => $this->t('This module stores a cache of information about different aspects of DonorPerfect, such as custom field definitions and code definitions. Sometimes it is necessary to refresh this cache, for example when a new field or a new code is added in DonorPerfect. Click the link below to refresh the cache now.'),
      '#open' => FALSE,
    ];
    $form['cache']['refresh'] = Link::createFromRoute(
      $this->t('Refresh DonorPerfect cache'),
      'donorperfect.admin.refresh_cache',
      ['ajax' => 'nojs'],
      [
        'query' => $this->redirectDestination->getAsArray(),
        'attributes' => ['class' => ['button', 'use-ajax']],
      ]
    )->toRenderable();
    // Add settings for each entity type.
    $form['entity'] = [
      '#type' => 'details',
      '#title' => $this->t('Drupal Entity Settings'),
      '#description' => $this->t('This module queries the DonorPerfect database and presents the data as native Drupal entities. Use these settings to configure each of the entity types.'),
      '#tree' => TRUE,
      '#open' => FALSE,
    ];
    // Call the controller service to add entity settings.
    $form['entity'] = $this->dpControllerService->settingsFormBuild($form['entity'], $form_state);
    // Don't show the entity fieldset if empty.
    if (empty(Element::children($form['entity']))) {
      $form['entity']['#access'] = FALSE;
    }
    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('XML API Credentials'),
      '#tree' => TRUE,
      '#open' => FALSE,
    ];
    $credentials = $this->dpUtility->apiCredentialsLoad('settings');
    $form['api']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('XML API Key'),
      '#default_value' => $credentials['key'] ?? '',
      '#attributes' => ['autocorrect' => 'off', 'autocomplete' => 'off'],
    ];
    $form['api']['or'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h5>-- OR --</h5>'),
    ];
    $form['api']['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DonorPerfect Username'),
      '#default_value' => $credentials['user'] ?? '',
      '#attributes' => ['autocorrect' => 'off', 'autocomplete' => 'off'],
    ];
    $form['api']['pw'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DonorPerfect Password'),
      '#default_value' => $credentials['pw'] ?? '',
      '#attributes' => ['autocorrect' => 'off', 'autocomplete' => 'off'],
    ];
    $form['misc'] = [
      '#type' => 'details',
      '#title' => $this->t('Miscellaneous Settings'),
      '#tree' => FALSE,
      '#open' => FALSE,
    ];
    $variations = $config->get('first_name_variations') ?? [];
    $variations_default = '';
    foreach ($variations as $group) {
      if (is_array($group)) {
        $variations_default .= implode(', ', $group) . "\n";
      }
    }
    $form['misc']['first_name_variations'] = [
      '#type' => 'textarea',
      '#title' => $this->t('First Name Variations'),
      '#description' => $this->t('Enter one group per line, and put a comma between each variation. When this module searches DonorPerfect, if one variation of a first name is being searched for, then all the other variations in that group will also be included in the search. For example, if a search is being done for Bill Smith, then William, Billy, Will, etc. will also be searched for.'),
      '#default_value' => $variations_default,
      '#rows' => 3,
    ];
    // Specify form keys to save at submit.
    $form_state->set('save_keys', ['entity']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Validate the API credentials.
    $this->dpUtility->apiCredentialsValidate($form_state);
    // Call the controller service to validate the form.
    $this->dpControllerService->settingsFormValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Save the API credentials.
    $this->dpUtility->apiCredentialsSave($form_state);
    // Call the controller service to submit the form.
    $this->dpControllerService->settingsFormSubmit($form, $form_state);
    // Load config.
    $config = $this->config('donorperfect.settings');
    // Save first_name_variations to config.
    if ($variations_string = $form_state->getValue('first_name_variations')) {
      $variations = [];
      $groups = explode("\n", $variations_string);
      foreach ($groups as $group) {
        $temp = array_map('strtolower', array_map('trim', explode(',', $group)));
        if (count($temp) > 1) {
          $variations[] = $temp;
        }
      }
      $config->set('first_name_variations', $variations);
    }
    // Save values to config.
    $save_keys = $form_state->get('save_keys');
    if (!empty($save_keys) && is_array($save_keys)) {
      foreach ($save_keys as $save_key) {
        $config->set($save_key, $form_state->getValue($save_key));
      }
    }
    $config->save();
    // Call the controller service to perform post submission procedures.
    $this->dpControllerService->settingsFormPostSubmit($form, $form_state);
  }

}
