<?php

namespace Drupal\donorperfect\Entity;

/**
 * Provides an interface for DonorPerfect entity controllers.
 *
 * @ingroup donorperfect
 */
interface ControllerInterface {

  /**
   * The getSettingsFormClass method.
   *
   * Return the class name of the settings form for this
   * DonorPerfect entity type.
   *
   * @return string
   *   The class name of the settings form.
   */
  public function getSettingsFormClass();

}
