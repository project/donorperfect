<?php

namespace Drupal\donorperfect\Entity;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\donorperfect\DPUtility;
use Drupal\entity\EntityViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides overrides to core's views integration for DonorPerfect entities.
 */
class ViewsData extends EntityViewsData {

  /**
   * The DPUtility service.
   *
   * @var \Drupal\donorperfect\DPUtility
   */
  protected $dpUtility;

  /**
   * Constructs a new \Drupal\donorperfect\Entity\ViewsData object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type to provide views integration for.
   * @param \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $storage_controller
   *   The storage handler used for this entity type.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation_manager
   *   The translation manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\donorperfect\DPUtility $dputility
   *   The DPUtility service.
   */
  public function __construct(EntityTypeInterface $entity_type, SqlEntityStorageInterface $storage_controller, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, TranslationInterface $translation_manager, EntityFieldManagerInterface $entity_field_manager, DPUtility $dputility) {
    parent::__construct($entity_type, $storage_controller, $entity_type_manager, $module_handler, $translation_manager, $entity_field_manager);
    $this->dpUtility = $dputility;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('string_translation'),
      $container->get('entity_field.manager'),
      $container->get('donorperfect.dputility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $entity_type_id = $this->entityType->id();
    $base_table = $this->entityType->getBaseTable() ?: $entity_type_id;
    $entity_id_key = $this->entityType->getKey('id');

    $data[$base_table]['table']['base'] = array_merge($data[$base_table]['table']['base'], [
      'help' => 'Represents a ' . $this->entityType->getSingularLabel() . ' record in DonorPerfect.',
      'query_id' => 'donorperfect_views_query',
      'defaults' => ['field' => $entity_id_key],
    ]);

    $udf_table = $this->entityType->get('udf_table');
    if (!empty($udf_table)) {
      $data[$udf_table]['table'] = [
        'group' => $this->entityType->getLabel(),
        'provider' => $this->entityType->getProvider(),
        'entity type' => $entity_type_id,
        'join' => [
          'dp' => [
            'left_field' => $entity_id_key,
            'field' => $entity_id_key,
          ],
        ],
      ];

      $field_infos = $this->dpUtility->getEntityFieldInfo($entity_type_id);
      foreach ($field_infos as $field_name => $field_info) {
        if (is_array($field_info) && !empty($field_info['dp_table']) && ($field_info['dp_table'] === $udf_table)) {
          if (array_key_exists($field_name, $data[$base_table])) {
            $data[$udf_table][$field_name] = $data[$base_table][$field_name];
            unset($data[$base_table][$field_name]);
          }
        }
      }
    }

    // Add entity reference relationships.
    $base_fields = $this->getEntityFieldManager()->getBaseFieldDefinitions($entity_type_id);
    $entity_reference_fields = array_filter($base_fields, function (BaseFieldDefinition $field) {
      return $field->getType() == 'entity_reference';
    });
    $this->addRelationships($data, $entity_reference_fields);

    // Add computed fields.
    foreach ($base_fields as $field_name => $field_definition) {
      if ($field_definition->isComputed()) {
        $data[$base_table][$field_name] = [
          'title' => $field_definition->getLabel(),
          'field' => [
            'id' => 'field',
          ],
          'entity field' => $field_name,
        ];
      }
    }

    return $data;
  }

  /**
   * Adds relationships for the base entity reference fields.
   *
   * @param array $data
   *   The views data.
   * @param \Drupal\Core\Field\BaseFieldDefinition[] $fields
   *   The entity reference fields.
   */
  protected function addRelationships(array &$data, array $fields) {
    $entity_type_id = $this->entityType->id();
    $base_table = $this->getViewsTableForEntityType($this->entityType);
    assert($this->entityType instanceof ContentEntityType);

    foreach ($fields as $field) {
      $target_entity_type_id = $field->getSettings()['target_type'];
      $target_entity_type = $this->getEntityTypeManager()->getDefinition($target_entity_type_id);
      if (!($target_entity_type instanceof ContentEntityType)) {
        continue;
      }
      $target_table = $this->getViewsTableForEntityType($target_entity_type);
      $field_name = $field->getName();
      $field_storage = $field->getFieldStorageDefinition();

      $args = [
        '@label' => $this->entityType->getSingularLabel(),
        '@entity' => $target_entity_type->getLabel(),
        '@field_name' => $field_name,
      ];
      $pseudo_field_name = $entity_type_id . '__' . $field_name;
      $relationship_data = [
        'label' => $target_entity_type->getLabel(),
        'group' => $this->entityType->getLabel(),
        'entity_type' => $entity_type_id,
      ];
      $data[$base_table][$pseudo_field_name]['relationship'] = [
        'id' => 'standard',
        'title' => $this->t('@entity with @field_name', $args),
        'help' => $this->t('The @entity that this @label belongs to.', $args),
        'base' => $target_table,
        'base field' => $target_entity_type->getKey('id'),
        'relationship field' => $this->tableMapping->getFieldColumnName($field_storage, 'target_id'),
      ] + $relationship_data;
    }
  }

}
