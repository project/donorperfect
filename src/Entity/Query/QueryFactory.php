<?php

namespace Drupal\donorperfect\Entity\Query;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\Query\QueryFactoryInterface;
use Drupal\donorperfect\DPQuery;

/**
 * Factory class creating entity query objects for the DonorPerfect backend.
 *
 * Most of the code in this class is copied from
 * \Drupal\Core\Entity\Query\Sql\QueryFactory.
 */
class QueryFactory implements QueryFactoryInterface {

  /**
   * The database connection to use.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The namespace of this class, the parent class etc.
   *
   * @var array
   */
  protected $namespaces;

  /**
   * The DPQuery service.
   *
   * @var \Drupal\donorperfect\DPQuery
   */
  protected $dpQuery;

  /**
   * Constructs a new \Drupal\donorperfect\Entity\Query\QueryFactory object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection used by the entity query.
   * @param \Drupal\donorperfect\DPquery $dpquery
   *   The DPQuery service.
   */
  public function __construct(Connection $connection, DPQuery $dpquery) {
    $this->connection = $connection;
    $this->namespaces = QueryBase::getNamespaces($this);
    $this->dpQuery = $dpquery;
  }

  /**
   * {@inheritdoc}
   */
  public function get(EntityTypeInterface $entity_type, $conjunction) {
    $class = QueryBase::getClass($this->namespaces, 'Query');
    return new $class($entity_type, $conjunction, $this->connection, $this->namespaces, $this->dpQuery);
  }

  /**
   * {@inheritdoc}
   */
  public function getAggregate(EntityTypeInterface $entity_type, $conjunction) {
    $class = QueryBase::getClass($this->namespaces, 'QueryAggregate');
    return new $class($entity_type, $conjunction, $this->connection, $this->namespaces, $this->dpQuery);
  }

}
