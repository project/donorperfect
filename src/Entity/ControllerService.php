<?php

namespace Drupal\donorperfect\Entity;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * A service_collector class to connect all DonorPerfect entity controllers.
 *
 * @see donorperfect.services.yml
 */
class ControllerService implements ControllerServiceInterface {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Array of all DonorPerfect entity controllers.
   *
   * @var \Drupal\donorperfect\Entity\ControllerInterface[]
   */
  protected $controllers = [];

  /**
   * Array of class names for each entity type's settings form.
   *
   * @var string[]
   */
  protected $settingsFormClasses = [];

  /**
   * The settingsFormObjects property.
   *
   * Array of objects instantiated from the form classes specified
   * in settingsFormClasses.
   *
   * @var \Drupal\Core\Form\FormInterface[]
   */
  protected $settingsFormObjects = [];

  /**
   * Constructs a new \Drupal\donorperfect\Entity\ControllerService object.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver.
   */
  public function __construct(ClassResolverInterface $class_resolver) {
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function addController(ControllerInterface $controller) {
    $this->controllers[] = $controller;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsFormClasses() {
    if (empty($this->settingsFormClasses)) {
      foreach ($this->controllers as $controller) {
        if (!empty($controller::ENTITY_TYPE_ID)) {
          $class = $controller->getSettingsFormClass();
          if (!empty($class)) {
            $this->settingsFormClasses[$controller::ENTITY_TYPE_ID] = $class;
          }
        }
      }
    }
    return $this->settingsFormClasses;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsFormObjects() {
    if (empty($this->settingsFormObjects)) {
      $classes = $this->getSettingsFormClasses();
      foreach ($classes as $entity_type_id => $class) {
        // Using class_resolver rather than form_builder because the
        // form object is needed, and form_builder does not have a
        // method that returns the form object.
        $this->settingsFormObjects[$entity_type_id] = $this->classResolver->getInstanceFromDefinition($class);
      }
    }
    return $this->settingsFormObjects;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $form, FormStateInterface $form_state) {
    $form_objects = $this->getSettingsFormObjects();
    foreach ($form_objects as $form_object) {
      $form = $form_object->buildForm($form, $form_state);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormValidate(array &$form, FormStateInterface $form_state) {
    $form_objects = $this->getSettingsFormObjects();
    foreach ($form_objects as $form_object) {
      $form_object->validateForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormSubmit(array &$form, FormStateInterface $form_state) {
    $form_objects = $this->getSettingsFormObjects();
    foreach ($form_objects as $form_object) {
      $form_object->submitForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormPostSubmit(array &$form, FormStateInterface $form_state) {
    $form_objects = $this->getSettingsFormObjects();
    foreach ($form_objects as $form_object) {
      $form_object->postSubmitForm($form, $form_state);
    }
  }

}
