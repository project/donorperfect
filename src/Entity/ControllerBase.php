<?php

namespace Drupal\donorperfect\Entity;

/**
 * Base controller class for DonorPerfect entities.
 */
class ControllerBase implements ControllerInterface {

  /**
   * The entity_type_id that this controller is connected to.
   */
  const ENTITY_TYPE_ID = '';

  /**
   * {@inheritdoc}
   */
  public function getSettingsFormClass() {
    return '';
  }

}
