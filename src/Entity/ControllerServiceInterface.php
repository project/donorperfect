<?php

namespace Drupal\donorperfect\Entity;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an interface for the DonorPerfect entity controller service.
 *
 * @ingroup donorperfect
 */
interface ControllerServiceInterface {

  /**
   * Adds an entity controller.
   *
   * @param \Drupal\donorperfect\Entity\ControllerInterface $controller
   *   The entity controller.
   */
  public function addController(ControllerInterface $controller);

  /**
   * The getSettingsFormClasses method.
   *
   * Return array of class names for each entity type's settings form,
   * keyed by entity_type_id.
   *
   * @return array
   *   Array of class names.
   */
  public function getSettingsFormClasses();

  /**
   * The getSettingsFormObjects method.
   *
   * Return array of objects instantiated from the form classes specified
   * in settingsFormClasses, keyed by entity_type_id.
   *
   * @return \Drupal\Core\Form\FormInterface[]
   *   Array of form objects.
   */
  public function getSettingsFormObjects();

  /**
   * The settingsFormBuild method.
   *
   * Call the buildForm method for the settings form of all
   * DonorPerfect entity types.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function settingsFormBuild(array $form, FormStateInterface $form_state);

  /**
   * The settingsFormValidate method.
   *
   * Call the validateForm method for the settings form of all
   * DonorPerfect entity types.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function settingsFormValidate(array &$form, FormStateInterface $form_state);

  /**
   * The settingsFormSubmit method.
   *
   * Call the submitForm method for the settings form of all
   * DonorPerfect entity types.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function settingsFormSubmit(array &$form, FormStateInterface $form_state);

  /**
   * The settingsFormPostSubmit method.
   *
   * Call the postSubmitForm method for the settings form of all
   * DonorPerfect entity types.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function settingsFormPostSubmit(array &$form, FormStateInterface $form_state);

}
