<?php

namespace Drupal\donorperfect\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity\EntityAccessControlHandler;

/**
 * Controls access based on the entity permissions.
 */
class AccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $account = $this->prepareUser($account);
    if ($operation === 'delete') {
      // DonorPerfect entities cannot be deleted.
      $result = AccessResult::forbidden();
    }
    else {
      /** @var \Drupal\Core\Access\AccessResult $result */
      $result = parent::checkAccess($entity, $operation, $account);
    }
    return $result;
  }

}
