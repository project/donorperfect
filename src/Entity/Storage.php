<?php

namespace Drupal\donorperfect\Entity;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * The default storage for DonorPerfect content entities.
 */
class Storage extends ContentEntityStorageBase implements SqlEntityStorageInterface {

  /**
   * The DPQuery service.
   *
   * @var \Drupal\donorperfect\DPQuery
   */
  protected $dpQuery;

  /**
   * The DPUtility service.
   *
   * @var \Drupal\donorperfect\DPUtility
   */
  protected $dpUtility;

  /**
   * The entity type's field storage definitions.
   *
   * @var \Drupal\Core\Field\FieldStorageDefinitionInterface[]
   */
  protected $fieldStorageDefinitions;

  /**
   * The mapping of field columns to SQL tables.
   *
   * @var \Drupal\Core\Entity\Sql\TableMappingInterface
   */
  protected $tableMapping;

  /**
   * Whether this storage should use the temporary table mapping.
   *
   * @var bool
   */
  protected $temporary = FALSE;

  /**
   * Constructs a new \Drupal\donorperfect\Entity\Storage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to be used.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache backend.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityFieldManagerInterface $entity_field_manager, CacheBackendInterface $cache, MemoryCacheInterface $memory_cache, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    parent::__construct($entity_type, $entity_field_manager, $cache, $memory_cache, $entity_type_bundle_info);
    $this->fieldStorageDefinitions = $this->entityFieldManager->getActiveFieldStorageDefinitions($entity_type->id());
    $this->tableMapping = $this->getTableMapping($this->fieldStorageDefinitions);
  }

  /**
   * {@inheritdoc}
   */
  protected function getQueryServiceName() {
    return 'donorperfect.entity.query';
  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadMultiple(array $ids = NULL) {
    // Attempt to load entities from the persistent cache. This will remove IDs
    // that were loaded from $ids.
    $entities_from_cache = $this->getFromPersistentCache($ids);
    // Load any remaining entities from the database.
    if ($entities_from_storage = $this->getFromStorage($ids)) {
      $this->invokeStorageLoadHook($entities_from_storage);
      $this->setPersistentCache($entities_from_storage);
    }
    return $entities_from_cache + $entities_from_storage;
  }

  /**
   * Gets entities from the storage.
   *
   * @param array|null $ids
   *   If not empty, return entities that match these IDs.
   *   Return empty array when NULL.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   Array of entities from the storage.
   */
  protected function getFromStorage(array $ids = NULL) {
    $entities = [];
    if (!empty($ids)) {
      // Sanitize IDs.
      $ids = $this->cleanIds($ids);
      if (empty($ids)) {
        return $entities;
      }
      // Build and execute the query.
      $base_table = $this->entityType->getBaseTable();
      $base_field = $this->entityType->getKey('id');
      $query = $this->dpQuery()->create('select', "{$base_table}.{$base_field}");
      $query->addCondition("{$base_table}.{$base_field} IN (" . implode(',', $ids) . ")");
      $fields = $this->dpUtility()->getEntityFieldInfo($this->entityType->id(), TRUE);
      $multi_fields = [];
      foreach ($fields as $field_name => $field_info) {
        if (empty($field_info['multi'])) {
          $query->addField($field_name);
        }
        else {
          $multi_fields[$field_name] = $field_info;
        }
      }
      $records = $query->execute()->result;
      $entities = $this->mapFromStorageRecords($records);
      // Add multi-field values.
      $multi_values = [];
      foreach ($multi_fields as $field_name => $field_info) {
        $dot_position = strpos($field_name, '.');
        if ($dot_position !== FALSE) {
          $field_name = substr($field_name, $dot_position + 1);
        }
        if ($field_name === 'flag') {
          $sql = "SELECT donor_id AS matching_id, flag AS code from dpflags WHERE donor_id IN (:donor_ids)";
          $args = [
            ':donor_ids' => implode(',', $ids),
          ];
        }
        else {
          $sql = "SELECT matching_id, code FROM dpusermultivalues WHERE (matching_id IN (:matching_ids)) AND (field_name=':field_name')";
          $args = [
            ':matching_ids' => implode(',', $ids),
            ':field_name' => strtoupper($field_name),
          ];
        }
        $query = $this->dpQuery()->create('pass', $sql, $args);
        $results = $query->execute()->result;
        foreach ($results as $result) {
          if (!array_key_exists($result['matching_id'], $multi_values)) {
            $multi_values[$result['matching_id']] = [];
          }
          if (!array_key_exists($field_name, $multi_values[$result['matching_id']])) {
            $multi_values[$result['matching_id']][$field_name] = [];
          }
          if (!in_array($result['code'], $multi_values[$result['matching_id']][$field_name])) {
            $multi_values[$result['matching_id']][$field_name][] = $result['code'];
          }
        }
      }
      foreach ($multi_values as $id => $values) {
        if (array_key_exists($id, $entities)) {
          foreach ($values as $field_name => $field_values) {
            $entities[$id]->{$field_name}->setValue($field_values);
          }
        }
      }
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function mapFromStorageRecords(array $records) {
    $entities = [];
    $entity_class = $this->getEntityClass();
    $base_field = $this->entityType->getKey('id');
    foreach ($records as $record) {
      $entity_id = (int) $record[$base_field] ?? 0;
      $entity_values = [];
      foreach ($this->tableMapping->getTableNames() as $table) {
        foreach ($this->tableMapping->getFieldNames($table) as $field_name) {
          $field_columns = $this->tableMapping->getColumnNames($field_name);
          // Handle field types that store several properties.
          if (count($field_columns) > 1) {
            $definition_columns = $this->fieldStorageDefinitions[$field_name]->getColumns();
            foreach ($field_columns as $property_name => $column_name) {
              if (array_key_exists($column_name, $record)) {
                // Convert date if needed.
                if ($this->fieldStorageDefinitions[$field_name]->getType() === 'datetime') {
                  $matches = [];
                  if (preg_match('/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/', $record[$column_name], $matches)) {
                    $record[$column_name] = $matches[3] . '-' . str_pad($matches[1], 2, '0', STR_PAD_LEFT) . '-' . str_pad($matches[2], 2, '0', STR_PAD_LEFT);
                  }
                }
                $entity_values[$field_name][LanguageInterface::LANGCODE_DEFAULT][$property_name] = !empty($definition_columns[$property_name]['serialize']) ? unserialize($record[$column_name], ['allowed_classes' => FALSE]) : $record[$column_name];
                unset($record[$column_name]);
              }
            }
          }
          // Handle field types that store only one property.
          else {
            $column_name = reset($field_columns);
            if (array_key_exists($column_name, $record)) {
              // Convert date if needed.
              if ($this->fieldStorageDefinitions[$field_name]->getType() === 'datetime') {
                $matches = [];
                if (preg_match('/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})$/', $record[$column_name], $matches)) {
                  $record[$column_name] = $matches[3] . '-' . str_pad($matches[1], 2, '0', STR_PAD_LEFT) . '-' . str_pad($matches[2], 2, '0', STR_PAD_LEFT);
                }
              }
              $columns = $this->fieldStorageDefinitions[$field_name]->getColumns();
              $column = reset($columns);
              $entity_values[$field_name][LanguageInterface::LANGCODE_DEFAULT] = !empty($column['serialize']) ? unserialize($record[$column_name], ['allowed_classes' => FALSE]) : $record[$column_name];
              unset($record[$column_name]);
            }
          }
        }
      }
      // The code below for creating entity objects is copied from the parent
      // class \Drupal\Core\Entity\EntityStorageBase::mapFromStorageRecords().
      // The create() method cannot be called in this case because this is the
      // storage class, and the create() method calls on this class to create
      // the entity object.
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = new $entity_class($entity_values, $this->entityTypeId);
      $entities[$entity_id] = $entity;
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function doSaveFieldItems(ContentEntityInterface $entity, array $names = []) {}

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities) {
    // DonorPerfect does not allow deleting through the API.
  }

  /**
   * {@inheritdoc}
   */
  public function countFieldData($storage_definition, $as_bool = FALSE) {
    $count = 0;
    return $as_bool ? (bool) $count : (int) $count;
  }

  /**
   * {@inheritdoc}
   */
  protected function readFieldItemsToPurge(FieldDefinitionInterface $field_definition, $batch_size) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function purgeFieldItems(ContentEntityInterface $entity, FieldDefinitionInterface $field_definition) {}

  /**
   * {@inheritdoc}
   */
  protected function doLoadMultipleRevisionsFieldItems($revision_ids) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function doDeleteFieldItems($entities) {}

  /**
   * {@inheritdoc}
   */
  protected function doDeleteRevisionFieldItems(ContentEntityInterface $revision) {}

  /**
   * {@inheritdoc}
   */
  protected function has($id, EntityInterface $entity) {
    return !$entity->isNew();
  }

  /**
   * {@inheritdoc}
   */
  public function getTableMapping(array $storage_definitions = NULL) {
    // If a new set of field storage definitions is passed, for instance when
    // comparing old and new storage schema, we compute the table mapping
    // without caching.
    if (!empty($storage_definitions)) {
      return $this->getCustomTableMapping($this->entityType, $storage_definitions);
    }
    // If we are using our internal storage definitions, which is our main use
    // case, we can statically cache the computed table mapping.
    if (!isset($this->tableMapping)) {
      $this->tableMapping = $this->getCustomTableMapping($this->entityType, $this->fieldStorageDefinitions);
    }
    return $this->tableMapping;
  }

  /**
   * Gets a table mapping for the specified entity type and storage definitions.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type
   *   An entity type definition.
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface[] $storage_definitions
   *   An array of field storage definitions to be used to compute the table
   *   mapping.
   * @param string $prefix
   *   (optional) A prefix to be used by all the tables of this mapping.
   *   Defaults to an empty string.
   *
   * @return \Drupal\Core\Entity\Sql\TableMappingInterface
   *   A table mapping object for the entity's tables.
   *
   * @internal
   */
  public function getCustomTableMapping(ContentEntityTypeInterface $entity_type, array $storage_definitions, $prefix = '') {
    $prefix = $prefix ?: ($this->temporary ? 'tmp_' : '');
    return DefaultTableMapping::create($entity_type, $storage_definitions, $prefix);
  }

  /**
   * Gets the DPQuery service.
   *
   * @return \Drupal\donorperfect\DPQuery
   *   The DPQuery service.
   */
  protected function dpQuery() {
    if (!$this->dpQuery) {
      $this->dpQuery = \Drupal::service('donorperfect.dpquery');
    }
    return $this->dpQuery;
  }

  /**
   * Gets the DPUtility service.
   *
   * @return \Drupal\donorperfect\DPUtility
   *   The DPUtility service.
   */
  protected function dpUtility() {
    if (!$this->dpUtility) {
      $this->dpUtility = \Drupal::service('donorperfect.dputility');
    }
    return $this->dpUtility;
  }

}
