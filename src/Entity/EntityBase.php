<?php

namespace Drupal\donorperfect\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Extends ContentEntityBase for entities stored in DonorPerfect.
 *
 * @ingroup donorperfect
 */
class EntityBase extends ContentEntityBase {

  /**
   * The DPUtility service.
   *
   * @var \Drupal\donorperfect\DPUtility
   */
  protected $dpUtility;

  /**
   * Additional data storage for the entity.
   *
   * @var array
   */
  protected $data = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);
    $this->dpUtility();
  }

  /**
   * Get specified key from entity's $data array.
   *
   * @param string|array $key
   *   The key to retrieve from $data.
   * @param mixed $default
   *   The value to return if key doesn't exist on $data.
   *
   * @return mixed
   *   The value from $data or the $default passed in.
   */
  public function getData(string $key = '', mixed $default = NULL) {
    $output = $default;
    if (empty($key)) {
      $output = $this->data;
    }
    elseif (is_string($key)) {
      if (isset($this->data[$key])) {
        $output = $this->data[$key];
      }
    }
    elseif (is_array($key)) {
      $exists = [];
      $value = NestedArray::getValue($this->data, $key, $exists);
      if ($exists) {
        $output = $value;
      }
    }
    return $output;
  }

  /**
   * Set a value on entity's $data array.
   *
   * @param string|array $key
   *   The key to set on the $data array.
   * @param mixed $value
   *   The value to set on the $data array.
   *
   * @return \Drupal\donorperfect\Entity\EntityBase
   *   Return $this to enable chaining.
   */
  public function setData(string $key, mixed $value) {
    if (empty($key) && is_array($value)) {
      $this->data = $value;
    }
    elseif (is_string($key)) {
      $this->data[$key] = $value;
    }
    elseif (is_array($key)) {
      NestedArray::setValue($this->data, $key, $value, TRUE);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = [];
    $field_infos = static::dpUtility()->getEntityFieldInfo($entity_type->id());
    $id_key = $entity_type->getKey('id');
    foreach ($field_infos as $field_info) {
      if (!empty($field_info) && is_array($field_info) && !empty($field_info['dp_field']) && !empty($field_info['entity_data_type'])) {
        $field = BaseFieldDefinition::create($field_info['entity_data_type'])
          ->setLabel($field_info['prompt'] ?? $field_info['dp_field'])
          ->setReadOnly($field_info['calculated'] ?? FALSE)
          ->setCardinality($field_info['multi'] ? FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED : 1)
          ->setSetting('dp_table', $field_info['dp_table'])
          ->setSetting('dp_field', $field_info['dp_field'])
          ->setSetting('dp_quote_value', ($field_info['quote'] ?? TRUE));
        if ($field_info['dp_field'] === $id_key) {
          $field->setReadOnly(TRUE)
            ->setSetting('unsigned', TRUE);
        }
        switch ($field_info['entity_data_type']) {
          case 'string':
            if (!empty($field_info['max_length']) && ((int) $field_info['max_length'] > 0)) {
              $field->setSetting('max_length', (int) $field_info['max_length']);
            }
            break;

          case 'list_string':
            $field->setSetting('allowed_values', static::dpUtility()->getCodesAsOptions($field_info['dp_field'], TRUE));
            break;

          case 'datetime':
            $field->setSetting('datetime_type', 'date');
            break;

        }
        $fields[$field_info['dp_field']] = $field;
      }
    }
    return $fields;
  }

  /**
   * Return the value of specified entity field.
   *
   * @param string $field_name
   *   The name of the field whose value should be returned.
   * @param array $options
   *   Array of options.
   *
   * @return mixed
   *   The value of the requested field.
   */
  public function getFieldValue(string $field_name, array $options = []) {
    try {
      $value = $this->get($field_name);
    }
    catch (\InvalidArgumentException $e) {
      return NULL;
    }
    if (!empty($value)) {
      if ($date = $value->date) {
        return $date;
      }
      elseif (isset($value->value)) {
        return $value->value;
      }
      elseif (!empty($value->target_id)) {
        $entities = $value->referencedEntities();
        if (empty($entities)) {
          return NULL;
        }
        elseif (count($entities) === 1) {
          return reset($entities);
        }
        else {
          if (!empty($options['return_first'])) {
            return reset($entities);
          }
          else {
            return $entities;
          }
        }
      }
    }
    else {
      return NULL;
    }
  }

  /**
   * Gets the DPUtility service.
   *
   * @return \Drupal\donorperfect\DPUtility
   *   The DPUtility service.
   */
  protected static function dpUtility() {
    // Check whether or not function is being called statically.
    if (isset($this) && ($this instanceof EntityBase)) {
      if (!$this->dpUtility) {
        $this->dpUtility = \Drupal::service('donorperfect.dputility');
      }
      return $this->dpUtility;
    }
    else {
      return \Drupal::service('donorperfect.dputility');
    }
  }

}
