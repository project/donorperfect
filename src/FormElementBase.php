<?php

namespace Drupal\donorperfect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a base class for DonorPerfect form elements.
 */
abstract class FormElementBase extends FormElement {

  /**
   * Gets specified value from $form_state.
   *
   * @param string $key
   *   The desired key to get.
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param mixed $default
   *   The default value to return if $key does not exist.
   *
   * @return mixed
   *   The value of the specified entity field.
   */
  public static function getFieldValue(string $key, array $element, FormStateInterface $form_state, mixed $default = NULL) {
    if (!empty($element['#parents']) && is_array($element['#parents'])) {
      $key = array_merge($element['#parents'], [$key]);
    }
    return $form_state->getValue($key, $default);
  }

  /**
   * Gets a token string to identify this form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return string
   *   The token identifier for the given element.
   */
  public static function getElementToken(array $element, FormStateInterface $form_state) {
    $token = static::getFieldValue('element_token', $element, $form_state, NULL);
    if (empty($token)) {
      $token = static::dpUtility()->generateToken();
    }
    return $token;
  }

  /**
   * Returns the DPUtility service.
   *
   * @return \Drupal\donorperfect\DPUtility
   *   The DPUtility service.
   */
  public static function dpUtility() {
    return \Drupal::service('donorperfect.dputility');
  }

}
