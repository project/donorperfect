<?php

/**
 * @file
 * Hooks provided by the DonorPerfect module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Load the API credentials needed to connect to the DonorPerfect XML API.
 *
 * @param array $credentials
 *   The array to populate with the api credentials.
 *   Possible array keys are 'user', 'pw', and/or 'key'.
 * @param string $context
 *   The context for which the credentials are being loaded.
 *   Possible values are 'settings' (indicating for the settings form),
 *   or 'dpquery' (indicating for actual use to connect to the API).
 */
function hook_donorperfect_api_credentials_load_alter(array &$credentials, string $context) {
}

/**
 * Validate the API credentials that were entered on the settings form.
 *
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The $form_state from the settings form.
 * @param bool $validated
 *   Set this to TRUE if validation has been completed.
 */
function hook_donorperfect_api_credentials_validate_alter(\Drupal\Core\Form\FormStateInterface $form_state, bool &$validated) {
}

/**
 * Save the API credentials that were entered on the settings form.
 *
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The $form_state from the settings form.
 * @param bool $saved
 *   Set this to TRUE once the credentials have been saved.
 */
function hook_donorperfect_api_credentials_save_alter(\Drupal\Core\Form\FormStateInterface $form_state, bool &$saved) {
}

/**
 * @} End of "addtogroup hooks".
 */
